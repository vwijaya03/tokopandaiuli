'use strict';

import GoogleAuth from 'google-auth-library'
import _ from 'lodash'

import middleware from '~/api/controllers/auth'

import service_user from '~/services/user'
import service_distributor from '~/services/distributor'

import helper from '~/utils/helper'

import { Distributor } from '~/orm/index'
import { Logs } from '~/orm/index'
import { ACCESS_TOKEN_INTERNAL_DEV } from '~/properties'

import UnauthorizedRequestError from '~/response/error/UnauthorizedRequestError'
import ExistDataError from '~/response/error/ExistDataError'
import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

const acl_distributor = ['super_admin'];

async function insert_distributor(req,res){
  const log_action = "Create Distributor Code";
  const dt_code = req.swagger.params.dt_code.value;
  const name = req.swagger.params.name.value;
  const token = req.swagger.params.token.value;
  Logs.create({action: log_action, description: "param req DT CODE: "+dt_code+", name: "+name});

  try {
    if(token != ACCESS_TOKEN_INTERNAL_DEV) {
        throw new UnauthorizedRequestError();
    }
    let dist = await service_distributor.getOneByDtCode({dt_code: dt_code});
    if(dist != null){
      throw new ExistDataError('data sudah ada');
    }
    else{
       let args = {
       dt_code:dt_code,
       name:name};
       service_distributor.input(args);
       res.json(new CustomSuccess(200, ' Insert berhasil'));
     }
  } catch(err) {
      if(err.code === undefined) {
          err.code = 400;
          err.message = 'Insert Gagal :'+err;
      }
      err.message = (err.message === undefined) ? err : err.message;
      res.status(400).json({code: 400, message: err.message});
  }
}

async function delete_distributor(req, res) {
  const log_action = "Delete Distributor Code";
  const dt_code = req.swagger.params.dt_code.value;
  const token = req.swagger.params.token.value;
  Logs.create({action: log_action, description: "param req DT CODE: "+dt_code});

  try {
    if(token != ACCESS_TOKEN_INTERNAL_DEV) {
        throw new UnauthorizedRequestError();
    }
    let dist = await service_distributor.getOneByDtCode({dt_code: dt_code});

    if(dist == null){
      res.json(new CustomSuccess(400, 'Data Kosong'));
    }
  else{
    let args = {
         dt_code:dt_code
       };
       service_distributor.deleteDtCode(args);
       res.json(new CustomSuccess(200, 'berhasil'));
     }
  } catch(err) {
      if(err.code === undefined) {
          err.code = 400;
          err.message = 'Delete Gagal : Data Kosong ';
      }
      err.message = (err.message === undefined) ? err : err.message;
      res.status(400).json({code: 400, message: err.message});
  }
}

async function get_distributors(req, res) {
  const log_action = "Get Distributor";
  const token = req.swagger.params.token.value;
  Logs.create({action: log_action, description: "Get All Distributor");

  try {
    let auth_user = await middleware.verify({token: token, acl: acl_distributor});

     let data = await  service_distributor.getAll();
     res.json(data);

  } catch(err) {
      if(err.code === undefined) {
          err.code = 400;
          err.message = 'Error'+err;
      }
      err.message = (err.message === undefined) ? err : err.message;
      res.status(400).json({code: 400, message: err.message});
  }
}

async function get_one_distributor(req, res) {
  const log_action = "Get One Distributor Code";
  const dt_code = req.swagger.params.dt_code.value;
  const token = req.swagger.params.token.value;
  Logs.create({action: log_action, description: "param req DT CODE: "+dt_code});

  try {
    let auth_user = await middleware.verify({token: token, acl: acl_distributor});

    let dist = await service_distributor.getOneByDtCode({dt_code: dt_code});
    if(dist == null){
      throw new error('data tidak ada');
    }
    res.json(dist);

     }
   catch(err) {
      if(err.code === undefined) {
          err.code = 400;
          err.message = 'Gagal : Data Kosong ';
      }
      err.message = (err.message === undefined) ? err : err.message;
      res.status(400).json({code: 400, message: err.message});
  }
}


export { insert_distributor }
export { delete_distributor }
export { get_distributors }
export { get_one_distributor }
