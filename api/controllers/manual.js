'use strict';
import _ from 'lodash'
import moment from 'moment'
import middleware from '~/api/controllers/auth'

import helper from '~/utils/helper'

import service_invoice from '~/services/invoice'
import service_formatter from '~/services/formatter'
import service_transaction from '~/services/transaction'
import service_top from '~/services/top'
import service_distributor from '~/services/distributor'
import service_potongan from '~/services/potongan'

import { Invoice } from '~/orm/index'
import { Transaction } from '~/orm/index'
import { Logs } from '~/orm/index'
import { orm } from '~/services/mariadb'

import UnauthorizedRequestError from '~/response/error/UnauthorizedRequestError'
import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'


async function manual_transaction(req,res){
  const token = req.swagger.params.token.value;
  const from_user_id = req.swagger.params.from_user_id.value;
  const to_user_id = req.swagger.params.to_user_id.value;
  const dt_code = req.swagger.params.dt_code.value;
  const invoice_id = req.swagger.params.invoice_id.value;
  const invoice_code = req.swagger.params.invoice_code.value;
  const retailer_id = req.swagger.params.retailer_id.value;
  const distributor_id = req.swagger.params.distributor_id.value;
  const type = req.swagger.params.type.value;
  const amount = req.swagger.params.amount.value;
  const currency_code = req.swagger.params.currency_code.value;
  const transact_date = req.swagger.params.transact_date.value;
  const valdo_tx_id = req.swagger.params.valdo_tx_id.value;
  
 

  try {

  let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

    await Transaction.create({
         fromUserId: from_user_id,
         toUserId: to_user_id,
         dt_code: dt_code,
         invoice_id: invoice_id,
         invoice_code: dt_code+invoice_id,
         valdo_tx_id: valdo_tx_id,
         amount: amount,
         currency_code: currency_code,
         transact_date: transact_date,
         retailerId: retailer_id,
         distributorId: distributor_id,
         type:type,
     });
     
     
    
     res.status(200).json({code: 200, status:"Success"});
  
  } catch(err) {
    if(err.code === undefined) {
      err.code = 400;
      err.message = 'Gagal :'+err;
    }
    err.message = (err.message === undefined) ? err : err.message;
    res.status(400).json({code: 400, message: err.message});
    }
}

async function get_invoice(req,res){
    const token = req.swagger.params.token.value;
    const invoice_id = req.swagger.params.invoice_id.value;
    
    try {
        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let invoices = await orm.query('select * from invoice where invoice_id='+invoice_id, { type: orm.QueryTypes.SELECT});

        res.status(200).json({code: 200, status:invoices});
    } 
    catch(err) {
    if(err.code === undefined) {
      err.code = 400;
      err.message = 'Gagal :'+err;
    }
    err.message = (err.message === undefined) ? err : err.message;
    res.status(400).json({code: 400, message: err.message});
    }
}


async function get_transaction(req,res){
  const token = req.swagger.params.token.value;
  const param_dt_code = req.swagger.params.dt_code;
  const param_invoice_id = req.swagger.params.invoice_id;

  let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
  let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;

  try{
      await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

      let invoice = await service_top.get_one_transaction({invoice_id:invoice_id,dt_code:dt_code})
      console.log(invoice)
      console.log("length "+invoice.length)
      if(invoice.length === 0){
        invoice =0
      }
      res.status(200).json({code:200, transactions: invoice})
  }
  catch(err) {
     if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data transaksi: ,'+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
  }
}
async function get_one_distributor(req,res){
  const token = req.swagger.params.token.value;
  const param_dt_code = req.swagger.params.dt_code;

  let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;

  try{
      await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

      let invoice = await service_top.get_one_distributor({dt_code:dt_code})
      if(invoice.length === 0){
        invoice =0
      }
      res.status(200).json({code:200, transactions: invoice})
  }
  catch(err) {
     if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data transaksi: ,'+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
  }
}

async function get_one_distributor(req,res){
  const token = req.swagger.params.token.value;
  const param_dt_code = req.swagger.params.dt_code;

  let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;

  try{
      await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

      let invoice = await service_top.get_one_distributor({dt_code:dt_code})
      if(invoice.length === 0){
        invoice =0
      }
      res.status(200).json({code:200, transactions: invoice})
  }
  catch(err) {
     if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data transaksi: ,'+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
  }
}

async function get_top_user(req, res) {
    const token = req.swagger.params.token.value;
    const param_le_code = req.swagger.params.le_code;
   
   
    let le_code = _.isUndefined(param_le_code.value)?null:param_le_code.value;
  
    try {
        
        await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
        
        let user_data = await service_invoice.get_dtcode({le_code:le_code});
	if(user_data.length === 0){
             throw new CustomError(400,'Le Code tidak ditemukan!')
        }
        let dist_detail = await service_distributor.getOneByDtCode({dt_code:user_data[0]["dt_code"]})

        let out = user_data[0]
        out.distributor_name = dist_detail.dataValues.name
        

        res.status(200).json({code: 200, body:out});
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function create_retur(req,res){
  const token = req.swagger.params.token.value;
  const param_amount_potongan = req.swagger.params.amount_potongan;
  const param_dt_code = req.swagger.params.dt_code;
  const param_le_code = req.swagger.params.le_code;
  const param_invoice_id = req.swagger.params.invoice_id;
  const param_remark_potongan = req.swagger.params.remark_potongan;

  let amount_potongan = _.isUndefined(param_amount_potongan.value)? 0 : param_amount_potongan.value;
  let dt_code = _.isUndefined(param_dt_code.value)? null : param_dt_code.value;
  let le_code = _.isUndefined(param_le_code.value)? null : param_le_code.value;
  let invoice_id = _.isUndefined(param_invoice_id.value)? null : param_invoice_id.value;
  let remark_potongan = _.isUndefined(param_remark_potongan.value)? null : param_remark_potongan.value;

  try {
        
    let args = {
      le_code : le_code,
      dt_code : dt_code,
      amount_potongan  : amount_potongan,
      trans_invoice_id : invoice_id,
      date_potongan : moment(Date.now()).format('YYYY-MM-DD HH:mm:ss'),
      remark_potongan : remark_potongan,
      remark_app : 'retur_cash_uli'
    }

    let check_invoice = await service_potongan.invoice_check({dt_code:dt_code,le_code:le_code,trans_invoice_id:invoice_id})
    console.log(check_invoice)

    let get_invoice = await service_invoice.get_one_invoice({dt_code:dt_code,invoice_id:invoice_id})
    console.log(get_invoice)
    let balance_amount = parseInt(get_invoice[0]["cash_memo_balance_amount"])
    console.log(balance_amount)

    if(check_invoice === 0 && amount_potongan <= balance_amount){
      let create_retur = await service_potongan.create_retur(args)
      console.log(create_retur)
    }
    else if(check_invoice === 1 && amount_potongan <= balance_amount){
      let update_retur = await service_potongan.update_retur(args)
      console.log(update_retur)
    }else{
      throw new CustomError(400,'Gagal memasukkan data retur!')
    }

    
    
    res.status(200).json({code: 200, message: 'Success'});
  } catch(err) {
    if(err.code === undefined) {
        err.code = 400;
        err.err_code = '';
        err.message = ''+err;
    }

    res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
}
}

async function get_all_potongan(req,res){
  const token = req.swagger.params.token.value;
  const param_dt_code = req.swagger.params.dt_code;
  const param_role_type = req.swagger.params.role_type;
  const param_page = req.swagger.params.page;
  const param_items_per_page = req.swagger.params.items_per_page;
  
  let dt_code = _.isUndefined(param_dt_code.value)? null : param_dt_code.value;
  let role_type = _.isUndefined(param_role_type.value)? null : param_role_type.value;
  let startPage = _.isUndefined(param_page.value)?0:param_page.value;
  let itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;

  if(dt_code === ''){
    dt_code = null
  }
  if(role_type=== ''){
    role_type = null
  }
  if(startPage === ''){
    startPage = 0
  }
  if(itemsPerPage === ''){
    itemsPerPage = 15
  }
  
  startPage = (startPage < 1 ) ? 1 : startPage;

  try {
    
    let args = {
      role_type:role_type,
      dt_code:dt_code,
      remark_potongan: 'retur_invoice',
      startPage:startPage,
      itemsPerPage:itemsPerPage
    }
    
    console.log(args)
    let all = await service_potongan.get_all_potongan(args)
    console.log(all, 'manual get_all_potongan')

    let {list:data, total:total_list_potongan} = all;
		let total_pages = Math.ceil(total_list_potongan / itemsPerPage);
		let out = {};
		out.page = startPage;
		out.total_pages = total_pages;
		out.size = data.length;
		out.total_list_potongan = total_list_potongan;
		out.items_per_page = itemsPerPage;
		out.potongan = data;
    
    res.status(200).json(out);
  } catch(err) {
    if(err.code === undefined) {
        err.code = 400;
        err.err_code = '';
        err.message = ''+err;
    }

    res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
}
}

export { get_top_user }
export { manual_transaction }
export { get_transaction }
export { get_invoice }
export { get_one_distributor }
export { create_retur }
export { get_all_potongan }





