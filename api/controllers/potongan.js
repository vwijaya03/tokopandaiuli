'use strict';

import _ from 'lodash'
import csvToJson from 'convert-csv-to-json'

import middleware from '~/api/controllers/auth'

import helper from '~/utils/helper'

import service_distributor from '~/services/distributor'
import service_invoice from '~/services/invoice'
import service_potongan from '~/services/potongan'
import service_formatter from '~/services/formatter'

import { Invoice } from '~/orm/index'
import { Logs } from '~/orm/index'
import { Task } from '~/orm/index'
import { TaskPotongan } from '~/orm/index'
import { TaskBatalPotongan } from '~/orm/index'

import UnauthorizedRequestError from '~/response/error/UnauthorizedRequestError'
import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

const exec = require('child_process').execSync;

async function upload_data_potongan(req, res) {
    const token = req.swagger.params.token.value;
    const param_dt_code = req.swagger.params.dt_code;
    const param_filename = req.swagger.params.filename;
    const param_fmcg_name = req.swagger.params.fmcg_name;
    const param_user_id = req.swagger.params.user_id;

    let fmcg_name = _.isUndefined(param_fmcg_name.value)?null:param_fmcg_name.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let filename = _.isUndefined(param_filename.value)?null:param_filename.value;
    let user_id = _.isUndefined(param_user_id.value)?null:param_user_id.value;

    let d = new Date();
    let current_datetime = d.getFullYear()+"-"+("0" + (d.getMonth() + 1)).slice(-2)+"-"+("0" + d.getDate()).slice(-2)+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();

    let path_dir = '/opt/data-potongan/'+filename;
    let dest_dir = '/opt/data-potongan/rl_'+fmcg_name+'_'+current_datetime+'_'+filename;
    let originalname = 'rl_'+fmcg_name+'_'+current_datetime+'_'+filename;

    try {
        await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let distributor = await service_distributor.getOneByDtCode({dt_code: dt_code});

        if(distributor == null) {
            throw new CustomError(400, 'Data distributor tidak di temukan');
        }

        res.json(new CustomSuccess(200, 'Data potongan berhasil di upload'));
        
        let remove_2_lines_from_bottom = "head --lines=-0 '" + path_dir + "' > '" + dest_dir + "'";
        
        exec(remove_2_lines_from_bottom, (error, stdout, stderr) => {
        });
        
        exec("rm -rf '" + path_dir + "'", (error, stdout, stderr) => {
        });

        let json = csvToJson.fieldDelimiter(',').getJsonFromCsv(dest_dir);
        let result = await TaskPotongan.create({dt_code: json[0]["dt_code"], original_name: originalname, file_name: filename, status: 0, total_row: json.length, user_id: user_id});
        let task_id = result.id;

        await service_potongan.uploadPotongan({records: json, filename: filename, originalname: originalname, task_id: task_id});
        
        service_potongan.inputTaskPotongan({task_id: task_id});
    } catch(err) {
        console.log(err)
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function upload_data_pembatalan_potongan(req, res) {
    const token = req.swagger.params.token.value;
    const param_dt_code = req.swagger.params.dt_code;
    const param_filename = req.swagger.params.filename;
    const param_fmcg_name = req.swagger.params.fmcg_name;
    const param_user_id = req.swagger.params.user_id;

    let fmcg_name = _.isUndefined(param_fmcg_name.value)?null:param_fmcg_name.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let filename = _.isUndefined(param_filename.value)?null:param_filename.value;
    let user_id = _.isUndefined(param_user_id.value)?null:param_user_id.value;

    let d = new Date();
    let current_datetime = d.getFullYear()+"-"+("0" + (d.getMonth() + 1)).slice(-2)+"-"+("0" + d.getDate()).slice(-2)+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();

    let path_dir = '/opt/data-potongan/'+filename;
    let dest_dir = '/opt/data-potongan/rl_pembatalan'+fmcg_name+'_'+current_datetime+'_'+filename;
    let originalname = 'rl_pembatalan'+fmcg_name+'_'+current_datetime+'_'+filename;

    try {
        await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let distributor = await service_distributor.getOneByDtCode({dt_code: dt_code});

        if(distributor == null) {
            throw new CustomError(400, 'Data distributor tidak di temukan');
        }
        
        res.json(new CustomSuccess(200, 'Data pembatalan potongan berhasil di upload'));

        let remove_2_lines_from_bottom = "head --lines=-0 '" + path_dir + "' > '" + dest_dir + "'";
        
        exec(remove_2_lines_from_bottom, (error, stdout, stderr) => {
        });
        
        exec("rm -rf '" + path_dir + "'", (error, stdout, stderr) => {
        });
        
        let json = csvToJson.fieldDelimiter(',').getJsonFromCsv(dest_dir);
        let result = await TaskBatalPotongan.create({dt_code: json[0]["dt_code"], original_name: originalname, file_name: filename, status: 0, total_row: json.length, user_id: user_id});
        let task_id = result.id;

        await service_potongan.updatePembatalanPotongan({records: json, filename: filename, originalname: originalname, task_id: task_id}); 

        TaskBatalPotongan.update({status: 1}, {where: {id: task_id}});
    } catch(err) {
        console.log(err)
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function task_list_potongan(req, res) {
    const token = req.swagger.params.token.value;
    const param_dt_code = req.swagger.params.dt_code;
    const param_email = req.swagger.params.email;
    const param_page = req.swagger.params.page;
    const param_item_per_page = req.swagger.params.item_per_page;
    const param_sorting = req.swagger.params.sorting;
    const param_sortingBy = req.swagger.params.sortingBy;
    const param_usertype = req.swagger.params.usertype;

    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let email = _.isUndefined(param_email.value)?null:param_email.value;
    let page = _.isUndefined(param_page.value)?0:param_page.value;
    let item_per_page = _.isUndefined(param_item_per_page.value)?15:param_item_per_page.value;
    let sorting = _.isUndefined(param_sorting.value)?null:param_sorting.value;
    let sortingBy = _.isUndefined(param_sortingBy.value)?null:param_sortingBy.value;
    let usertype = _.isUndefined(param_usertype.value)?null:param_usertype.value;

    try {
        await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let args = {};
        args.dt_code = dt_code;
        args.email = email;
        args.page = page;
        args.item_per_page = item_per_page;
        args.sorting = sorting;
        args.sortingBy = sortingBy;
        args.usertype = usertype;
        
        let tasks = await service_potongan.getAllTaskPotongan(args);

        let {list:result_tasks, total:total_tasks} = tasks;

        let total_pages = Math.ceil(total_tasks / item_per_page);

        let out = {};
        out.page = page;
        out.total_pages = total_pages;
        out.size = result_tasks.length;
        out.total_tasks = total_tasks;
        out.items_per_page = item_per_page;
        out.tasks = result_tasks;

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function task_list_pembatalan_potongan(req, res) {
    const token = req.swagger.params.token.value;
    const param_dt_code = req.swagger.params.dt_code;
    const param_email = req.swagger.params.email;
    const param_page = req.swagger.params.page;
    const param_item_per_page = req.swagger.params.item_per_page;
    const param_sorting = req.swagger.params.sorting;
    const param_sortingBy = req.swagger.params.sortingBy;
    const param_usertype = req.swagger.params.usertype;

    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let email = _.isUndefined(param_email.value)?null:param_email.value;
    let page = _.isUndefined(param_page.value)?0:param_page.value;
    let item_per_page = _.isUndefined(param_item_per_page.value)?15:param_item_per_page.value;
    let sorting = _.isUndefined(param_sorting.value)?null:param_sorting.value;
    let sortingBy = _.isUndefined(param_sortingBy.value)?null:param_sortingBy.value;
    let usertype = _.isUndefined(param_usertype.value)?null:param_usertype.value;

    try {
        await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let args = {};
        args.dt_code = dt_code;
        args.email = email;
        args.page = page;
        args.item_per_page = item_per_page;
        args.sorting = sorting;
        args.sortingBy = sortingBy;
        args.usertype = usertype;
        
        let tasks = await service_potongan.getAllTaskPembatalanPotongan(args);

        let {list:result_tasks, total:total_tasks} = tasks;

        let total_pages = Math.ceil(total_tasks / item_per_page);

        let out = {};
        out.page = page;
        out.total_pages = total_pages;
        out.size = result_tasks.length;
        out.total_tasks = total_tasks;
        out.items_per_page = item_per_page;
        out.tasks = result_tasks;

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

export { upload_data_potongan }
export { upload_data_pembatalan_potongan }
export { task_list_potongan }
export { task_list_pembatalan_potongan }