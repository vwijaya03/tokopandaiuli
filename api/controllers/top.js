'use strict';

import _ from 'lodash'
import request from 'request'
import moment from 'moment'

import middleware from '~/api/controllers/auth'
import helper from '~/utils/helper'

import UnauthorizedRequestError from '~/response/error/UnauthorizedRequestError'
import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

import service_top from '~/services/top'
import service_invoice_top from '~/services/invoice_top'


const exec = require('child_process').execSync;

//let top_dev_api = "tokopandai-development.pinjammodal.id";
//let top_api_key = "0ff0170259be55375551f943d9ebf04eb097d0b55661a5e8f13dac3ef9062820";

let top_dev_api = "api.pinjammodal.id:8443/tokopandai";
let top_api_key = "904071eb7378b9f0cc7152b7ada253d7f9493e0e4a1ef9a0634ebf101b7e84ca"

async function check_url(req,res){
    const token = req.swagger.params.token.value;
    try{
         request({
            headers: {
            'Content-Type': 'application/json'
            },
             url: "https://"+top_dev_api,
             method: "GET",
             json: true
           }, function (error, response, body){
                console.log(response.statusCode)
                if(response.statusCode === 502 ){
                    res.status(400).json({code: response.statusCode, message: "PayLater Sedang Dalam Perbaikan"});
                }
            })
       
  } catch(err) {
    if(err.code === undefined) {
      err.code = 400;
      err.message = 'Gagal :'+err;
    }
    err.message = (err.message === undefined) ? err : err.message;
    res.status(400).json({code: 400, message: err.message});
    }
}

async function get_invoice(req,res){
  const token = req.swagger.params.token.value;
  const param_user_id = req.swagger.params.user_id;
  const param_lecode = req.swagger.params.le_code;

  const param_dtcode = req.swagger.params.dt_code;
  const param_invoice_payment_status = req.swagger.params.invoice_payment_status;
  const param_invoice_due_date = req.swagger.params.invoice_due_date;
  const param_overdue = req.swagger.params.overdue;
  const param_dist_dt_code = req.swagger.params.dist_dt_code;
  const param_role_type = req.swagger.params.role_type;
  const param_page = req.swagger.params.page;
  const param_items_per_page = req.swagger.params.items_per_page;
  const param_sorting = req.swagger.params.sorting;

  let user_id = _.isUndefined(param_user_id.value) ? null : param_user_id.value;
  let role_type = _.isUndefined(param_role_type.value) ? null : param_role_type.value;
  let le_code = _.isUndefined(param_lecode.value)?0:param_lecode.value;
  let startPage = _.isUndefined(param_page.value)?0:param_page.value;
  let itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;
  let dt_code = _.isUndefined(param_dtcode.value) ? null : param_dtcode.value;
  let invoice_payment_status = _.isUndefined(param_invoice_payment_status.value) ? null : param_invoice_payment_status.value;
  let invoice_due_date = _.isUndefined(param_invoice_due_date.value) ? null : param_invoice_due_date.value;
  let overdue = _.isUndefined(param_overdue.value)?null:param_overdue.value;
  let dist_dt_code = _.isUndefined(param_dist_dt_code.value) ? null : param_dist_dt_code.value;
  let sorting = _.isUndefined(param_sorting.value) ? null : param_sorting.value;

  
  if(dt_code === ''){
    dt_code = null
  }
  if(invoice_payment_status === ''){
    invoice_payment_status = null
  }
  if(invoice_due_date === ''){
    invoice_due_date = null
  }
  if(overdue === 0){
    overdue = null
  }
  if(startPage === ''){
    startPage = 0
  }
  if(itemsPerPage === ''){
    itemsPerPage = 15
  }
  if(sorting === ''){
    sorting = null
  }
  
  startPage = (startPage < 1 ) ? 1 : startPage;

  try {
  let leCode = le_code.split(',')
  console.log(leCode,leCode.length)
  console.log(invoice_payment_status, 'paid/unpaid')
  console.log(invoice_due_date,'due date')
  
  request({
            headers: {
            'Content-Type': 'application/json'
            },
             url: "https://"+top_dev_api,
             method: "GET",
             json: true
           }, function (error, response, body){
                console.log(response.statusCode)
                if(response.statusCode != 502 ){
                    for(let i=0;i < leCode.length;i++){
     
    request({
      headers: {
      'Content-Type': 'application/json',
      'api_key' : top_api_key
      },
       url: "https://"+top_dev_api+"/loan/detail",
       method: "POST",
       json: true,
       body: {
         le_code: leCode[i]
      }
     }, function (error, response, body){
        // console.log(body)
         if (body["status"] !=="not_found") {
            let arr_body = body.data.installment
            //console.log(arr_body)
            arr_body.forEach(async function(item) {
             //console.log(item)
                let data_checked = await service_invoice_top.find_invoice({dt_code:item.dt_code,le_code:item.le_code,cash_memo_doc_no:item.cash_memo_doc_no,invoice_toko: item.invoice_toko})
                //console.log(data_checked)
                if(data_checked === 0){
                  service_invoice_top.insert_invoice({
                    user_id:user_id,
                    dt_code: item.dt_code,
                    le_code:item.le_code,
                    outlet_code: item.outlet_code,
                    cash_memo_doc_no: item.cash_memo_doc_no,
                    cash_memo_doc_date: item.cash_memo_doc_date,
                    cash_memo_total_amount: item.cash_memo_total_amount,
                    cash_memo_balance_amount: item.cash_memo_balance_amount,
                    cashmemo_type: item.cashmemo_type,
                    invoice_id:item.invoice_id,
                    invoice_id_associated_with_retailer_id: item.invoice_id_associated_with_retailer_id,
                    invoice_sales_date: item.invoice_sales_date,
                    invoice_due_date: item.invoice_due_date,
                    invoice_payment_status: item.invoice_payment_status,
                    product: item.product,
                    product_name: item.product_name,
                    quantity_pc: item.quantity_pc,
                    product_price_cs: item.product_price_cs,
                    product_pride_dz: item.product_pride_dz,
                    product_price_pc: item.product_price_pc,
                    overdue: item.overdue,
                    distributor_principle_code: item.distributor_principle_code,
                    invoice_toko: item.invoice_toko,
                    principle: item.principle,
                    denda_amount: item.denda_amount,
                    total_yg_harus_dibayar: item.total_yg_harus_dibayar,
                    credit_limit: item.credit_limit,
                    status_toko: item.status_toko
                  })
                 // Logs.create({action: 'Insert New TOP Invoice', description:'token:'+token+'le_code:'+le_code+'dt_code:'+dt_code+'invoice_id:'+item.cash_memo_doc_no});
                }
                else if(data_checked === 1){
                  service_invoice_top.update_invoice({
                    le_code: item.le_code,
                    dt_code:item.dt_code,
                    cash_memo_doc_no: item.cash_memo_doc_no,
                    cash_memo_doc_date: item.cash_memo_doc_date,
                    cash_memo_total_amount: item.cash_memo_total_amount,
                    cash_memo_balance_amount: item.cash_memo_balance_amount,
                    cashmemo_type: item.cashmemo_type,
                    invoice_id:item.invoice_id,
                    invoice_id_associated_with_retailer_id: item.invoice_id_associated_with_retailer_id,
                    invoice_sales_date: item.invoice_sales_date,
                    invoice_due_date: item.invoice_due_date,
                    invoice_payment_status: item.invoice_payment_status,
                    product: item.product,
                    product_name: item.product_name,
                    quantity_pc: item.quantity_pc,
                    product_price_cs: item.product_price_cs,
                    product_pride_dz: item.product_pride_dz,
                    product_price_pc: item.product_price_pc,
                    overdue: item.overdue,
                    distributor_principle_code: item.distributor_principle_code,
                    invoice_toko: item.invoice_toko,
                    principle: item.principle,
                    denda_amount: item.denda_amount,
                    total_yg_harus_dibayar: item.total_yg_harus_dibayar,
                    credit_limit: item.credit_limit,
                    status_toko: item.status_toko
                  })
                 // Logs.create({action: 'Get All: Update Invoice', description: 'token:'+token+'le_code:'+le_code+'dt_code:'+dt_code+'invoice_id:'+item.cash_memo_doc_no});
                }      
            })     
         }
   });
  }
                 }
                else{
}})

    let date = new Date()
    let current_date = moment(date.setDate(date.getDate()+114)).format('YYYY-MM-DD');
    let next_date = moment(date.setDate(date.getDate() + 3)).format('YYYY-MM-DD');
    let previous_date = moment(date.setDate(date.getDate() - 14)).format('YYYY-MM-DD');
    console.log(next_date,current_date,previous_date)
   
    
    let data = await service_invoice_top.get_all_invoice({overdue:overdue,dist_dt_code:dist_dt_code,role_type:role_type,user_id:user_id, dt_code:dt_code,sorting:sorting,startPage:startPage,itemsPerPage:itemsPerPage,invoice_payment_status:invoice_payment_status,invoice_due_date:invoice_due_date,next_date:next_date,previous_date:previous_date});
    let count = await service_invoice_top.countData({overdue:overdue,dist_dt_code:dist_dt_code,role_type:role_type,user_id:user_id,dt_code:dt_code,sorting:sorting,startPage:startPage,itemsPerPage:itemsPerPage,invoice_payment_status:invoice_payment_status,invoice_due_date:invoice_due_date,next_date:next_date,previous_date:previous_date})
    console.log(count)
    let total_pages = Math.ceil(count[0].total / itemsPerPage);
    let out = {};
    out.page = startPage;
    out.total_pages = total_pages;
    out.size = data.length;
    out.items_per_page = itemsPerPage;
    out.invoices = data;
    

    res.status(200).json(out);
    
  } catch(err) {
    if(err.code === undefined) {
      err.code = 400;
      err.message = 'Gagal :'+err;
    }
    err.message = (err.message === undefined) ? err : err.message;
    res.status(400).json({code: 400, message: err.message});
    }
}

async function get_invoice_offline(req,res){
  const token = req.swagger.params.token.value;
  const param_user_id = req.swagger.params.user_id;
  const param_lecode = req.swagger.params.le_code;

  const param_dtcode = req.swagger.params.dt_code;
  const param_invoice_payment_status = req.swagger.params.invoice_payment_status;
  const param_invoice_due_date = req.swagger.params.invoice_due_date;
  const param_overdue = req.swagger.params.overdue;
  const param_dist_dt_code = req.swagger.params.dist_dt_code;
  const param_role_type = req.swagger.params.role_type;
  const param_page = req.swagger.params.page;
  const param_items_per_page = req.swagger.params.items_per_page;
  const param_sorting = req.swagger.params.sorting;

  let user_id = _.isUndefined(param_user_id.value) ? null : param_user_id.value;
  let role_type = _.isUndefined(param_role_type.value) ? null : param_role_type.value;
  let le_code = _.isUndefined(param_lecode.value)?0:param_lecode.value;
  let startPage = _.isUndefined(param_page.value)?0:param_page.value;
  let itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;
  let dt_code = _.isUndefined(param_dtcode.value) ? null : param_dtcode.value;
  let invoice_payment_status = _.isUndefined(param_invoice_payment_status.value) ? null : param_invoice_payment_status.value;
  let invoice_due_date = _.isUndefined(param_invoice_due_date.value) ? null : param_invoice_due_date.value;
  let overdue = _.isUndefined(param_overdue.value)?null:param_overdue.value;
  let dist_dt_code = _.isUndefined(param_dist_dt_code.value) ? null : param_dist_dt_code.value;
  let sorting = _.isUndefined(param_sorting.value) ? null : param_sorting.value;

  
  if(dt_code === ''){
    dt_code = null
  }
  if(invoice_payment_status === ''){
    invoice_payment_status = null
  }
  if(invoice_due_date === ''){
    invoice_due_date = null
  }
  if(overdue === 0){
    overdue = null
  }
  if(startPage === ''){
    startPage = 0
  }
  if(itemsPerPage === ''){
    itemsPerPage = 15
  }
  if(sorting === ''){
    sorting = null
  }
  
  startPage = (startPage < 1 ) ? 1 : startPage;

  try {
  let leCode = le_code.split(',')
  console.log(leCode,leCode.length)
  console.log(invoice_payment_status, 'paid/unpaid')
  console.log(invoice_due_date,'due date')
  
    let date = new Date()
    let current_date = moment(date.setDate(date.getDate()+114)).format('YYYY-MM-DD');
    let next_date = moment(date.setDate(date.getDate() + 3)).format('YYYY-MM-DD');
    let previous_date = moment(date.setDate(date.getDate() - 14)).format('YYYY-MM-DD');
    console.log(next_date,current_date,previous_date)
   
    
    let data = await service_invoice_top.get_all_invoice({overdue:overdue,dist_dt_code:dist_dt_code,role_type:role_type,user_id:user_id, dt_code:dt_code,sorting:sorting,startPage:startPage,itemsPerPage:itemsPerPage,invoice_payment_status:invoice_payment_status,invoice_due_date:invoice_due_date,next_date:next_date,previous_date:previous_date});
    let count = await service_invoice_top.countData({overdue:overdue,dist_dt_code:dist_dt_code,role_type:role_type,user_id:user_id,dt_code:dt_code,sorting:sorting,startPage:startPage,itemsPerPage:itemsPerPage,invoice_payment_status:invoice_payment_status,invoice_due_date:invoice_due_date,next_date:next_date,previous_date:previous_date})
    console.log(count)
    let total_pages = Math.ceil(count[0].total / itemsPerPage);
    let out = {};
    out.page = startPage;
    out.total_pages = total_pages;
    out.size = data.length;
    out.items_per_page = itemsPerPage;
    out.invoices = data;
    

    res.status(200).json(out);
    
  } catch(err) {
    if(err.code === undefined) {
      err.code = 400;
      err.message = 'Gagal :'+err;
    }
    err.message = (err.message === undefined) ? err : err.message;
    res.status(400).json({code: 400, message: err.message});
    }
}

async function get_one_invoice(req,res){
  const token = req.swagger.params.token.value;
  const dt_code = req.swagger.params.dt_code.value;
  const le_code = req.swagger.params.le_code.value;
  const invoice_id = req.swagger.params.invoice_id.value;
  const tenor = req.swagger.params.tenor.value;
  const user_id = req.swagger.params.user_id.value;

  try {
  //let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
    let args = {
      le_code : le_code,
      dt_code : dt_code,
      invoice_number : invoice_id,
      tenor : tenor
    }
    let data = await service_invoice_top.get_one_invoice({dt_code: dt_code,le_code: le_code,cash_memo_doc_no: invoice_id+'-'+tenor,invoice_toko : invoice_id})

    request({
            headers: {
            'Content-Type': 'application/json'
            },
             url: "https://"+top_dev_api,
             method: "GET",
             json: true
           }, function (error, response, body){
                console.log(response.statusCode)
                if(response.statusCode != 502 ){
   
    request({
      headers: {
      'Content-Type': 'application/json',
      'api_key' : top_api_key
      },
       url: "https://"+top_dev_api+"/loan/detail",
       method: "POST",
       json: true,
       body: args
     },async function (error, response, body){
     console.log(body)
       if (body["status"] ==="success") {
            if(body.data.installment === undefined){
              res.status(200).json({message: 'Data installment kosong'});
            }
            else{
            let data_checked = await service_invoice_top.find_invoice({dt_code: dt_code,le_code:le_code,cash_memo_doc_no:invoice_id+'-'+tenor,invoice_toko: invoice_id})
            console.log(data_checked)
            if(data_checked === 0){
              service_invoice_top.insert_invoice({
                user_id:user_id,
                dt_code: body.data.installment.dt_code,
                le_code:body.data.installment.le_code,
                outlet_code: body.data.installment.outlet_code,
                cash_memo_doc_no: body.data.installment.cash_memo_doc_no,
                cash_memo_doc_date: body.data.installment.cash_memo_doc_date,
                cash_memo_total_amount: body.data.installment.cash_memo_total_amount,
                cash_memo_balance_amount: body.data.installment.cash_memo_balance_amount,
                cashmemo_type: body.data.installment.cashmemo_type,
                invoice_id: body.data.installment.invoice_id,
                invoice_id_associated_with_retailer_id: body.data.installment.invoice_id_associated_with_retailer_id,
                invoice_sales_date: body.data.installment.invoice_sales_date,
                invoice_due_date: body.data.installment.invoice_due_date,
                invoice_payment_status: body.data.installment.invoice_payment_status,
                product: body.data.installment.product,
                product_name: body.data.installment.product_name,
                quantity_pc: body.data.installment.quantity_pc,
                product_price_cs: body.data.installment.product_price_cs,
                product_pride_dz: body.data.installment.product_pride_dz,
                product_price_pc: body.data.installment.product_price_pc,
                overdue: body.data.installment.overdue,
                distributor_principle_code: body.data.installment.distributor_principle_code,
                invoice_toko: body.data.installment.invoice_toko,
                principle: body.data.installment.principle,
                denda_amount: body.data.installment.denda_amount,
                total_yg_harus_dibayar:body.data.installment.total_yg_harus_dibayar,
                credit_limit: body.data.installment.credit_limit,
                status_toko: body.data.installment.status_toko
              })
            //  Logs.create({action: 'Insert New TOP Invoice', description: 'token:'+token+'le_code:'+le_code+'dt_code:'+dt_code+'invoice_id:'+body.data.installment.cash_memo_doc_no});
            }
            else if(data_checked === 1){
              service_invoice_top.update_invoice({
                le_code: body.data.installment.le_code,
                dt_code:body.data.installment.dt_code,
                cash_memo_doc_no: body.data.installment.cash_memo_doc_no,
                cash_memo_doc_date: body.data.installment.cash_memo_doc_date,
                cash_memo_total_amount: body.data.installment.cash_memo_total_amount,
                cash_memo_balance_amount: body.data.installment.cash_memo_balance_amount,
                cashmemo_type: body.data.installment.cashmemo_type,
                invoice_id:body.data.installment.invoice_id,
                invoice_id_associated_with_retailer_id: body.data.installment.invoice_id_associated_with_retailer_id,
                invoice_sales_date: body.data.installment.invoice_sales_date,
                invoice_due_date: body.data.installment.invoice_due_date,
                invoice_payment_status: body.data.installment.invoice_payment_status,
                product: body.data.installment.product,
                product_name: body.data.installment.product_name,
                quantity_pc: body.data.installment.quantity_pc,
                product_price_cs: body.data.installment.product_price_cs,
                product_pride_dz: body.data.installment.product_pride_dz,
                product_price_pc: body.data.installment.product_price_pc,
                overdue: body.data.installment.overdue,
                distributor_principle_code: body.data.installment.distributor_principle_code,
                invoice_toko: body.data.installment.invoice_toko,
                principle: body.data.installment.principle,
                denda_amount: body.data.installment.denda_amount,
                total_yg_harus_dibayar: body.data.installment.total_yg_harus_dibayar,
                credit_limit: body.data.installment.credit_limit,
                status_toko: body.data.installment.status_toko
              })
            //  Logs.create({action: 'Get One: Update Invoice', description: 'token:'+token+'le_code:'+le_code+'dt_code:'+dt_code+'invoice_id:'+body.data.installment.cash_memo_doc_no});
            }       
      //let data = await service_invoice_top.get_one_invoice({dt_code: dt_code,le_code: le_code,cash_memo_doc_no: invoice_id+'-'+tenor,invoice_toko : invoice_id})

    res.status(200).json(data[0]); 
  }
     }
     else{
      res.status(400).json({code:400, message:'data tidak ditemukan'});
     }
     })}
     else
     
    
     {
         res.status(200).json(data[0]); 
     }
       })
  } catch(err) {
    if(err.code === undefined) {
      err.code = 400;
      err.message = 'Gagal :'+err;
    }
    err.message = (err.message === undefined) ? err : err.message;
    res.status(400).json({code: 400, message: err.message});
    }
}


async function get_invoice_all_tenor(req,res){
  const token = req.swagger.params.token.value;
  const dt_code = req.swagger.params.dt_code.value;
  const le_code = req.swagger.params.le_code.value;
  const invoice_number = req.swagger.params.invoice_number.value;

  try{

    let args = {
      le_code : le_code,
      dt_code : dt_code,
      invoice_number : invoice_number
    }
    console.log(args)

    request({
      headers: {
      'Content-Type': 'application/json',
      'api_key' : top_api_key
      },
       url: "https://"+top_dev_api+"/loan/detail",
       method: "POST",
       json: true,
       body: args
     },async function (error, response, body){
       console.log(body)
       let invoices = body.data.installment
       console.log(invoices)
       let total_amount = 0
       let total_inv = 0
       for(let i=0;i < invoices.length; i++){
         total_amount = total_amount + invoices[i].total_yg_harus_dibayar
         total_inv = total_inv + invoices[i].principle
       }
       
       console.log(invoices.length)
       console.log(total_amount, total_inv)
       res.status(200).json({data: invoices, total_tenor: invoices.length, total_pembayaran:total_amount, total_inv:total_inv})
    })
  }catch(err) {
    if(err.code === undefined) {
      err.code = 400;
      err.message = 'Gagal :'+err;
    }
    err.message = (err.message === undefined) ? err : err.message;
    res.status(400).json({code: 400, message: err.message});
    }
}


async function calculator(req,res){
  const token = req.swagger.params.token.value;
  const param_amount = req.swagger.params.amount;
  const param_discount = req.swagger.params.discount;

  let amount = _.isUndefined(param_amount.value)?0:param_amount.value;
  let discount = _.isUndefined(param_discount.value)?0:param_discount.value;

  try {

  let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
  let args ={
    "amount" : amount,
    "discount": discount,
    }
    console.log(args);
    request({
      headers: {
      'Content-Type': 'application/json',
      'api_key' : top_api_key
      },
       url: "https://"+top_dev_api+"/loan/calculator-tokopandai",
       method: "POST",
       json: true,
       body: args
     }, function (error, response, body){
        res.json({body});
        console.log(body);
   });
  } catch(err) {
    if(err.code === undefined) {
      err.code = 400;
      err.message = 'Gagal :'+err;
    }
    err.message = (err.message === undefined) ? err : err.message;
    res.status(400).json({code: 400, message: err.message});
    }
}

async function apply_loan(req,res){
  const token = req.swagger.params.token.value;
  const param_dtcode = req.swagger.params.dt_code;
  const param_lecode = req.swagger.params.le_code;
  const param_invoicenumber = req.swagger.params.invoice_number;
  const param_tenor = req.swagger.params.tenor;
  const param_amount = req.swagger.params.amount;
  const param_principal_code = req.swagger.params.principal_code;
  const param_discount = req.swagger.params.total_potongan;

  let dt_code = _.isUndefined(param_dtcode.value)?0:param_dtcode.value;
  let le_code = _.isUndefined(param_lecode.value)?0:param_lecode.value;
  let tenor = _.isUndefined(param_tenor.value)?0:param_tenor.value;
  let invoice_number = _.isUndefined(param_invoicenumber.value)?0:param_invoicenumber.value;
  let principal_code = _.isUndefined(param_principal_code.value)?0:param_principal_code.value;
  let amount = _.isUndefined(param_amount.value)?0:param_amount.value
  let discount = _.isUndefined(param_discount.value)?0:param_discount.value;

  try {

  let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
  let args ={

       "dt_code": dt_code,
       "le_code": le_code,
       "invoice_number": invoice_number,
       "amount": amount,
       "tenor": tenor,
       "principle_code": principal_code,
       "discount": discount
     }
  console.log(args);
  request({
      headers: {
      'Content-Type': 'application/json',
      'api_key' : top_api_key
      },
       url: "https://"+top_dev_api+"/loan/apply-tokopandai",
       method: "POST",
       json: true,
       body: args
     }, function (error, response, body){
         res.status(200).json({code: 200, body});
   });
  } catch(err) {
    if(err.code === undefined) {
      err.code = 400;
      err.message = 'Gagal :'+err;
    }
    err.message = (err.message === undefined) ? err : err.message;
    res.status(400).json({code: 400, message: err.message});
    }
}

async function repayment(req,res){
  const token = req.swagger.params.token.value;
  const param_amount = req.swagger.params.amount;
  const param_invoice_number = req.swagger.params.invoice_number;
  const param_dt_code = req.swagger.params.dt_code;
  const param_le_code = req.swagger.params.le_code;

  let amount = _.isUndefined(param_amount.value)?0:param_amount.value;
  let invoice_number = _.isUndefined(param_invoice_number.value)?0:param_invoice_number.value;
  let dt_code = _.isUndefined(param_dt_code.value)?0:param_dt_code.value;
  let le_code = _.isUndefined(param_le_code.value)?0:param_le_code.value;

  try {

  let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
  let args ={
    "dt_code": dt_code,
    "le_code": le_code,
    "invoice_number": invoice_number,
    "amount": amount
  }
  request({
      headers: {
        'Content-Type': 'application/json',
        'api_key' : top_api_key
      },
       url: "https://"+top_dev_api+"/loan/repayment-tokopandai",
       method: "POST",
       json: true,
       body: args
     }, function (error, response, body){
         res.status(200).json({code: 200, body});
         console.log(response);
   });
  } catch(err) {
    if(err.code === undefined) {
      err.code = 400;
      err.message = 'Gagal :'+err;
    }
    err.message = (err.message === undefined) ? err : err.message;
    res.status(400).json({code: 400, message: err.message});
    }
}

async function cek_saldo(req,res){
  const le_code = req.swagger.params.le_code.value;
  try {
  let args ={
    le_code : le_code
  }
  
  request({
            headers: {
            'Content-Type': 'application/json'
            },
             url: "https://"+top_dev_api,
             method: "GET",
             json: true
           }, function (error, response, body){
                console.log(response.statusCode)
                if(response.statusCode === 502 ){
                    res.status(200).json({code:response.statusCode,saldo : 0 });
                    
                }else{
           
  
                request({
                    headers: {
                    'Content-Type': 'application/json',
                    'api_key' : top_api_key
                    },
                     url: "https://"+top_dev_api+"/loan/detail",
                     method: "POST",
                     json: true,
                     body: args
                   }, function (error, response, body){
                       res.status(200).json({code:200,saldo : body });

                 }); 
           }})
   
  } catch(err) {
    if(err.code === undefined) {
      err.code = 400;
      err.message = 'Gagal :'+err;
    }
    err.message = (err.message === undefined) ? err : err.message;
    res.status(400).json({code: 400, message: err.message});
    }
}


async function inputPaymentDistributor(req,res){
	const log_action = "FIT : inputPaymentDistributor";
  const token = req.swagger.params.token.value;
  const param_fmcg_id = req.swagger.params.fmcg_id;

  const param_le_code = req.swagger.params.le_code;
  const param_dt_code = req.swagger.params.dt_code;
  const param_amount_invoice = req.swagger.params.amount_invoice;
  const param_total_amount = req.swagger.params.total_amount;
  const param_invoice_no = req.swagger.params.invoice_no;
  const param_tenor = req.swagger.params.tenor;
  const param_principal_code = req.swagger.params.principal_code;

  let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
  let dt_code = _.isUndefined(param_dt_code.value) ? null : param_dt_code.value;
  let le_code = _.isUndefined(param_le_code.value) ? null : param_le_code.value;
  let amount_invoice = _.isUndefined(param_amount_invoice.value) ? null : param_amount_invoice.value;
  let total_amount = _.isUndefined(param_total_amount.value) ? null : param_total_amount.value;
  let invoice_no = _.isUndefined(param_invoice_no.value) ? null : param_invoice_no.value;
  let tenor = _.isUndefined(param_tenor.value) ? null : param_tenor.value;
  let principal_code = _.isUndefined(param_principal_code.value) ? null : param_principal_code.value;
 
	//Logs.create({action: log_action, description: "CustomerName: "+CustomerName+", Address: "+Address+", MobilePhone: "+MobilePhone,created_at: Date.now(),updated_at: Date.now()});
	try {
		//let auth_user = await middleware.verify({token: token, acl:'2b3e6865-bdae-4881-ba71-ea9c5f123e8d', is_allowed_access_token_static: true});
    let check = await service_top.findOne({invoice_no:invoice_no});
    console.log(check);
    if(check == 0){
      let args = {
          le_code:le_code,
          dt_code:dt_code,
          invoice_no:invoice_no,
          amount:amount_invoice,
          total_tagihan:total_amount,
          principal_code:principal_code,
          fmcg_id:fmcg_id,
          tenor:tenor
      };
      console.log(args)
      service_top.insert_payment_distributor(args);
      res.status(200).json({code: 200, body:'add data success'});
    }
    else {
      throw new CustomError(400, 'Data sudah ada');
      
    }
	} catch(err) {
		if(err.code === undefined) {
			 err.code = 400;
			 err.message = 'Insert Gagal :'+err;
       console.log(err);
		}
		err.message = (err.message === undefined) ? err : err.message;
		res.status(400).json({code: 400, message: err.message});
	}
}

async function updateStatusPaymentDistributor(req,res){
  const token = req.swagger.params.token.value;
  const param_invoice_no = req.swagger.params.invoice_no;
  const param_payment_status = req.swagger.params.payment_status;

  let invoice_no = _.isUndefined(param_invoice_no.value)?null:param_invoice_no.value;
  let payment_status = _.isUndefined(param_payment_status.value) ? null : param_payment_status.value;
  console.log(payment_status, invoice_no)

  try {
    let check = await service_top.findOne({invoice_no:invoice_no});
    console.log(check);
    let tanggal = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss')
    if(check == 1){
      let args = {
          invoice_no:invoice_no,
          payment_status:payment_status,
          transaction_date: tanggal
      };
      console.log(args)
      service_top.update_payment_status_distributor(args);
      res.status(200).json({code: 200, body:'payment success'});
    }
    else {
      throw new CustomError(400, 'Update status gagal');
    }
	} catch(err) {
		if(err.code === undefined) {
			 err.code = 400;
			 err.message = 'Insert Gagal :'+err;
       console.log(err);
		}
		err.message = (err.message === undefined) ? err : err.message;
		res.status(400).json({code: 400, message: err.message});
	}
}

async function cek_le_code(req,res){
  const token = req.swagger.params.token.value;
  const param_lecode = req.swagger.params.le_code;

  let le_code = _.isUndefined(param_lecode.value)?0:param_lecode.value;

  try {

  let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

  console.log(le_code)

     request({
       headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization' : "Basic djRMZDAtMW50TDpWQGxkMEpha2FydGFAMjAxOCNjb2xsZWN0aW9u"
       },
        url: "http://192.168.13.5/agentakuisisi/getAktivasi?lecode="+le_code,
        method: "get",
        json: true,
      }, function (error, response, body){
          res.json(body.is_active);
   })

  } catch(err) {
    if(err.code === undefined) {
      err.code = 400;
      err.message = 'Gagal :'+err;
    }
    err.message = (err.message === undefined) ? err : err.message;
    res.status(400).json({code: 400, message: err.message});
  }
}

async function insert_invoice_loan_history(req,res){
    const token = req.swagger.params.token.value;

    const param_le_code = req.swagger.params.le_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_user_id = req.swagger.params.user_id;
    const param_amount = req.swagger.params.amount;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_tenor = req.swagger.params.tenor;
    const param_principal_code = req.swagger.params.principal_code;
    const param_total_potongan = req.swagger.params.total_potongan;
    const param_total_amount_invoice =req.swagger.params.total_amount_invoice; 

    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let le_code = _.isUndefined(param_le_code.value)?null:param_le_code.value;
    let user_id = _.isUndefined(param_user_id.value)?null:param_user_id.value;
    let tenor = _.isUndefined(param_tenor.value)?0:param_tenor.value;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let principal_code = _.isUndefined(param_principal_code.value)?null:param_principal_code.value;
    let amount = _.isUndefined(param_amount.value)?0:param_amount.value
    let total_amount_invoice = _.isUndefined(param_total_amount_invoice.value)?0:param_total_amount_invoice.value
    let total_potongan = _.isUndefined(param_total_potongan.value)?0:param_total_potongan.value;

    try {
      let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
      let total_discount = total_potongan/100*20;
      let arg = {
        user_id:user_id,
        le_code:le_code,
        dt_code:dt_code,
        total_amount_invoice:total_amount_invoice,
        amount:amount,
        invoice_id:invoice_id,
        tenor:tenor,
        principal_code:principal_code,
        total_potongan:total_potongan,
        total_discount :total_discount
      }

      let loan_history = await service_invoice_top.insert_loan_history(arg)
      res.status(200).json(loan_history)
    } catch(err) {
      if(err.code === undefined) {
        err.code = 400;
        err.message = 'Gagal :'+err;
      }
      err.message = (err.message === undefined) ? err : err.message;
      res.status(400).json({code: 400, message: err.message});
      }
}

async function get_invoice_loan_history(req,res){
  const token = req.swagger.params.token.value;

  const param_le_code = req.swagger.params.le_code;
  const param_dt_code = req.swagger.params.dt_code;
  const param_invoice_id = req.swagger.params.invoice_id;

  let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
  let le_code = _.isUndefined(param_le_code.value)?null:param_le_code.value;
  let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;

  try {
    let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

    let arg = {
      le_code:le_code,
      dt_code:dt_code,
      invoice_id:invoice_id
    }
    console.log(arg)
    let loan_history = await service_invoice_top.get_loan_history(arg)
    res.status(200).json(loan_history)
  } catch(err) {
    if(err.code === undefined) {
      err.code = 400;
      err.message = 'Gagal :'+err;
    }
    err.message = (err.message === undefined) ? err : err.message;
    res.status(400).json({code: 400, message: err.message});
    }
}

async function get_unpaid_invoices(req,res){
  const token = req.swagger.params.token.value;

  try{

    let unpaid_invoices = await service_top.get_unpaid_invoice()
    console.log(unpaid_invoices)
    res.status(200).json(unpaid_invoices);

  }catch(err) {
		if(err.code === undefined) {
			 err.code = 400;
			 err.message = 'Insert Gagal :'+err;
       console.log(err);
		}
		err.message = (err.message === undefined) ? err : err.message;
		res.status(400).json({code: 400, message: err.message});
	}
}


export { apply_loan }
export { cek_saldo }
export { cek_le_code }
export { get_invoice }
export { get_invoice_offline }
export { calculator }
export { repayment }
export { get_one_invoice } 
export { inputPaymentDistributor }
export { updateStatusPaymentDistributor }
export { get_invoice_all_tenor }
export { insert_invoice_loan_history }
export { get_invoice_loan_history }
export { get_unpaid_invoices }

