import _ from 'lodash'

import helper from '~/utils/helper'

import UnauthorizedRequestError from '~/response/error/UnauthorizedRequestError'
import TokenExpiredError from '~/response/error/TokenExpiredError'

import { USERTYPES } from '~/properties'
import { access_token_static } from '~/properties'

async function verify({token=null, acl=null, is_allowed_access_token_static=null}={}) {
	let is_access_token_static = access_token_static.includes(token);

	if(is_access_token_static && is_allowed_access_token_static) {
		return;
	} else {
		throw new UnauthorizedRequestError();
	}
}

function getUSERTYPESKey(data, type) {
	return Object.keys(data).filter(k => {
        return data[k] == type;
    }).pop();
}

function checkTokenExpiry({expirySeconds=null}={}) {
    if (!expirySeconds) {
        throw new TokenExpiredError();
    }
    const nowSeconds = new Date().getTime();

    if (nowSeconds > expirySeconds) {
        throw new TokenExpiredError();
    }
}

let obj = {};
obj.verify = verify;
obj.getUSERTYPESKey = getUSERTYPESKey;

export default obj