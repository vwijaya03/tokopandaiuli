'use strict';
import GoogleAuth from 'google-auth-library'
import _ from 'lodash'
import util from 'util'
import { sprintf } from "sprintf-js"
import queryString from 'query-string'
import Promise from 'bluebird'

import middleware from '~/api/controllers/auth'

import service_user from '~/services/user'
import service_retailer from '~/services/retailer'

import helper from '~/utils/helper'
import fs from 'fs'
import path from 'path'
const excelToJson = require('convert-excel-to-json');

import { Distributor } from '~/orm/index'
import { Logs } from '~/orm/index'
import { ACCESS_TOKEN_INTERNAL_DEV } from '~/properties'
import UnauthorizedRequestError from '~/response/error/UnauthorizedRequestError'
import ExistDataError from '~/response/error/ExistDataError'
import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

const acl_retailer = ['super_admin'];

async function get_retailers(req,res){
  const log_action = "Get Retailer";
  const token = req.swagger.params.token.value;
  Logs.create({action: log_action, description: "Get Retailer"});

  try {
    let auth_user = await middleware.verify({token: token, acl: acl_retailer});
     let data = await  service_retailer.getAll();
     res.json(data);

  } catch(err) {
      if(err.code === undefined) {
          err.code = 400;
          err.message = 'Error'+err;
      }
      err.message = (err.message === undefined) ? err : err.message;
      res.status(400).json({code: 400, message: err.message});
  }
}
async function get_one_retailer(req,res){
  const log_action = "Get One Retailer";
  const retailer_id = req.swagger.params.retailer_id.value;
  const token = req.swagger.params.token.value;
  try {
    Logs.create({action: log_action, description: "Get Retailer : retailer_id "+retailer_id});
    let auth_user = await middleware.verify({token: token, acl: acl_retailer});
    let dist = await service_retailer.getOneByDtCode({retailer_id:retailer_id});
    if(dist == null){
      throw new error('data tidak ada');
    }
    res.json(dist);
     }
   catch(err) {
      if(err.code === undefined) {
          err.code = 400;
          err.message = 'Gagal : Data Kosong ';
      }
      err.message = (err.message === undefined) ? err : err.message;
      res.status(400).json({code: 400, message: err.message});
  }
}
async function upload_retailer(req, res) {
    const token = req.swagger.params.token.value;
    const param_file = req.swagger.params.file;
    let file = _.isUndefined(param_file.value)?'':param_file.value[0];
    let path_dir = __dirname+'/static_file/data-excel-retailer/'+file;

    let extension = path.extname(file.originalname);
    let original_filename = file.originalname;
    Promise.resolve()
    .then(() => {
        if(token == ACCESS_TOKEN_INTERNAL_DEV) {
            try {
                let stats = fs.statSync(path_dir);
                if (!stats.isDirectory()) {
                    fs.unlinkSync(path_dir);
                    fs.mkdirSync(path_dir);
                }
            } catch(err) {
                fs.mkdirSync(path_dir);
            } finally {
                path_dir = path_dir + '/' + original_filename.slice(0, -(extension.length)) + extension;
                fs.renameSync(file.path, path_dir);
                let result =  excelToJson({sourceFile: path_dir});
                let data =JSON.stringify(result);
                service_retailer.postUploadRetailer({records:result});
                res.json(new CustomSuccess(200, ' Insert berhasil'));
            }
        } else {
            throw new Error('Token Invalid');
        }
    })
}
export { get_retailers }
export { get_one_retailer }
export { upload_retailer }
