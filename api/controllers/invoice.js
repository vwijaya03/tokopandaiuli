'use strict';

import _ from 'lodash'
import util from 'util'

import middleware from '~/api/controllers/auth'

import helper from '~/utils/helper'

import service_distributor from '~/services/distributor'
import service_invoice from '~/services/invoice'
import service_formatter from '~/services/formatter'

import { Invoice } from '~/orm/index'
import { Logs } from '~/orm/index'
import { Task } from '~/orm/index'

import UnauthorizedRequestError from '~/response/error/UnauthorizedRequestError'
import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

const exec = require('child_process').execSync;

async function get_invoices(req, res) {
    const token = req.swagger.params.token.value;
    const platform = req.swagger.params.platform.value;
    const usertype = req.swagger.params.usertype.value;
    const param_unique_code = req.swagger.params.unique_code;
    const param_outlet_code = req.swagger.params.outlet_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_le_code = req.swagger.params.le_code;
    const param_paid = req.swagger.params.paid;
    const param_unpaid = req.swagger.params.unpaid;
    //const param_paid_by = req.swagger.params.paid_by;
    const param_filter1 = req.swagger.params.filter1;
    const param_filter2 = req.swagger.params.filter2;
    const param_join_date = req.swagger.params.join_date;
    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;
    const param_sorting = req.swagger.params.sorting;
    const param_sortingBy = req.swagger.params.sortingBy;
    const param_filter_by = req.swagger.params.filter_by;

    let isUnpaid = param_unpaid.value?param_unpaid.value:false;
    let isPaid = param_paid.value?param_paid.value:false;
    //let paid_by = _.isUndefined(param_paid_by.value)?null:param_paid_by.value
    let unique_code = _.isUndefined(param_unique_code.value)?null:param_unique_code.value;
    let outletCode = _.isUndefined(param_outlet_code.value)?null:param_outlet_code.value;
    let distributorCode = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let leCode = _.isUndefined(param_le_code.value)?null:param_le_code.value;
    let startPage = _.isUndefined(param_page.value)?0:param_page.value;
    let itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;

    let sorting = _.isUndefined(param_sorting.value)?null:param_sorting.value;
    let sortingBy = _.isUndefined(param_sortingBy.value)?null:param_sortingBy.value;
    let filter_by = _.isUndefined(param_filter_by.value)?null:param_filter_by.value;
    let filter1 = _.isUndefined(param_filter1.value)?null:param_filter1.value.trim();
    let filter2 = _.isUndefined(param_filter2.value)?null:param_filter2.value.trim();
    let join_date = _.isUndefined(param_join_date.value)?null:param_join_date.value.trim();
  
    try {
        await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let invoice_args = {};
        invoice_args.platform = platform;
        invoice_args.usertype = usertype;
        invoice_args.outletCode = outletCode;
        invoice_args.distributorCode = distributorCode;
        invoice_args.leCode = leCode;
        invoice_args.isPaid = isPaid;
	//invoice_args.paid_by = paid_by;
        invoice_args.isUnpaid = isUnpaid;
        invoice_args.startPage = startPage;
        invoice_args.itemsPerPage = itemsPerPage;
        invoice_args.sorting = sorting;
        invoice_args.sortingBy = sortingBy;
        invoice_args.filter_by = filter_by;
        invoice_args.filter1 = filter1;
        invoice_args.filter2 = filter2;
        invoice_args.join_date = join_date;

        if(usertype == 0) {
            if(!unique_code)
                invoice_args.leCode = ['null'];
            else
                invoice_args.leCode = unique_code;
        } else if(usertype == 1 || usertype == 3) {
            if(!unique_code)
                invoice_args.distributorCode = ['null'];
            else
                invoice_args.distributorCode = unique_code;
        } else if(usertype == 4) {
            if(!unique_code)
                invoice_args.distributorCode = ['null'];
            else
                invoice_args.distributorCode = unique_code;
        }

        let result_invoices = await service_invoice.get_invoices(invoice_args);

        let formatter_options = {};
        formatter_options.attributes = ['!product'];

        let formatted_invoices = await service_formatter.format({obj:result_invoices.list, typeId: 'invoice', options:formatter_options});
        let out = {};

        if(itemsPerPage != 0)
        {
            let total_pages = Math.ceil(result_invoices.total / itemsPerPage);

            out.page = startPage;
            out.total_pages = total_pages;
            out.size = formatted_invoices.length;
            out.total_items = result_invoices.total;
            out.total_unpaid_items = result_invoices.total_unpaid;
            out.items_per_page = itemsPerPage;
            out.invoices = formatted_invoices;
        }
        else
        {
            out.total_items = result_invoices.total;
            out.total_unpaid_items = result_invoices.total_unpaid;
            out.invoices = formatted_invoices;
        }

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_one_invoice(req, res) {
    const token = req.swagger.params.token.value;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;

    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;

    try {
        await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let invoice = await service_invoice.get_one_invoice({invoice_id: invoice_id, dt_code: dt_code});

        let formatter_options = {};
        formatter_options.attributes = ['!product'];

        let out = service_formatter.format({obj: invoice, typeId: 'invoice', options:formatter_options});

        res.json(out[0]);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_retur(req, res) {
    const token = req.swagger.params.token.value;
    const platform = req.swagger.params.platform.value;
    const usertype = req.swagger.params.usertype.value;
    const param_unique_code = req.swagger.params.unique_code;
    const param_outlet_code = req.swagger.params.outlet_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_le_code = req.swagger.params.le_code;

    const param_startDate = req.swagger.params.start_date;
    const param_endDate = req.swagger.params.end_date;
    const param_join_date = req.swagger.params.join_date;
    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;
    const param_sorting = req.swagger.params.sorting;
    const param_sortingBy = req.swagger.params.sortingBy;
    const param_filter_by = req.swagger.params.filter_by;

    let unique_code = _.isUndefined(param_unique_code.value)?null:param_unique_code.value;
    let outletCode = _.isUndefined(param_outlet_code.value)?null:param_outlet_code.value;
    let distributorCode = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let leCode = _.isUndefined(param_le_code.value)?null:param_le_code.value;
    let startPage = _.isUndefined(param_page.value)?0:param_page.value;
    let itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;

    let sorting = _.isUndefined(param_sorting.value)?null:param_sorting.value;
    let sortingBy = _.isUndefined(param_sortingBy.value)?null:param_sortingBy.value;
    let filterBy = _.isUndefined(param_filter_by.value)?null:param_filter_by.value;
    let startDate = _.isUndefined(param_startDate.value)?null:param_startDate.value.trim();
    let endDate = _.isUndefined(param_endDate.value)?null:param_endDate.value.trim();
    let join_date = _.isUndefined(param_join_date.value)?null:param_join_date.value.trim();

    try {
        await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let retur_args = {};
        retur_args.platform = platform;
        retur_args.usertype = usertype;
        retur_args.outletCode = outletCode;
        retur_args.distributorCode = distributorCode;
        retur_args.leCode = leCode;
        retur_args.startPage = startPage;
        retur_args.itemsPerPage = itemsPerPage;
        retur_args.sorting = sorting;
        retur_args.sortingBy = sortingBy;
        retur_args.filterBy = filterBy;
        retur_args.startDate = startDate;
        retur_args.endDate = endDate;
        retur_args.join_date = join_date;

        if(usertype == 0) {
            if(!unique_code)
                retur_args.leCode = ['null'];
            else
                retur_args.leCode = unique_code;
        } else if(usertype == 1 || usertype == 3) {
            if(!unique_code)
                retur_args.distributorCode = ['null'];
            else
                retur_args.distributorCode = unique_code;
        } else if(usertype == 4) {
            if(!unique_code)
                retur_args.distributorCode = ['null'];
            else
                retur_args.distributorCode = unique_code;
        }

        let result_retur = await service_invoice.get_retur(retur_args);

        let formatter_options = {};
        formatter_options.attributes = ['!product'];

        let formatted_invoices = await service_formatter.format({obj:result_retur.list, typeId: 'invoice', options:formatter_options});
        let out = {};

        if(itemsPerPage != 0)
        {
            let total_pages = Math.ceil(result_retur.total / itemsPerPage);

            out.page = startPage;
            out.total_pages = total_pages;
            out.size = formatted_invoices.length;
            out.total_items = result_retur.total;
            out.total_unpaid_items = result_retur.total_unpaid;
            out.items_per_page = itemsPerPage;
            out.invoices = formatted_invoices;
        }
        else
        {
            out.total_items = result_retur.total;
            out.total_unpaid_items = result_retur.total_unpaid;
            out.invoices = formatted_invoices;
        }

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_total_unpaid_amount(req, res) {
    const token = req.swagger.params.token.value;
    const param_le_code = req.swagger.params.le_code;
    const param_join_date = req.swagger.params.join_date;

    let le_code = _.isUndefined(param_le_code.value)?null:param_le_code.value;
    let join_date = _.isUndefined(param_join_date.value)?null:param_join_date.value.trim();

    try {
        await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        if(!le_code)
            le_code = ['null'];

        let result = await service_invoice.get_total_unpaid_amount({le_code: le_code, join_date: join_date});

        res.json(result);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function check_process_invoice(req, res) {
    const token = req.swagger.params.token.value;
    const param_dt_code = req.swagger.params.dt_code;

    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;

    try {
        await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        if(!dt_code || dt_code == null || dt_code.length == 0) {
            throw new CustomError(400, 'Kode distributor tidak boleh kosong');
        }

        let result = await service_invoice.check_process_invoice({dt_code: dt_code});

        res.json(new CustomSuccess(200, 'Tidak ada proses invoice yang sedang berjalan'));
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function task_list(req, res) {
    const token = req.swagger.params.token.value;
    const param_dt_code = req.swagger.params.dt_code;
    const param_email = req.swagger.params.email;
    const param_page = req.swagger.params.page;
    const param_item_per_page = req.swagger.params.item_per_page;
    const param_sorting = req.swagger.params.sorting;
    const param_sortingBy = req.swagger.params.sortingBy;
    const param_usertype = req.swagger.params.usertype;

    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let email = _.isUndefined(param_email.value)?null:param_email.value;
    let page = _.isUndefined(param_page.value)?0:param_page.value;
    let item_per_page = _.isUndefined(param_item_per_page.value)?15:param_item_per_page.value;
    let sorting = _.isUndefined(param_sorting.value)?null:param_sorting.value;
    let sortingBy = _.isUndefined(param_sortingBy.value)?null:param_sortingBy.value;
    let usertype = _.isUndefined(param_usertype.value)?null:param_usertype.value;

    try {
        await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let args = {};
        args.dt_code = dt_code;
        args.email = email;
        args.page = page;
        args.item_per_page = item_per_page;
        args.sorting = sorting;
        args.sortingBy = sortingBy;
        args.usertype = usertype;

        let tasks = await service_invoice.get_task_list(args);

        let {list:result_tasks, total:total_tasks} = tasks;

        let total_pages = Math.ceil(total_tasks / item_per_page);

        let out = {};
        out.page = page;
        out.total_pages = total_pages;
        out.size = result_tasks.length;
        out.total_tasks = total_tasks;
        out.items_per_page = item_per_page;
        out.tasks = result_tasks;

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function process_invoice(req, res) {
    const token = req.swagger.params.token.value;
    const param_dt_code = req.swagger.params.dt_code;
    const param_filename = req.swagger.params.filename;
    const param_fmcg_name = req.swagger.params.fmcg_name;
    const param_user_id = req.swagger.params.user_id;

    let fmcg_name = _.isUndefined(param_fmcg_name.value)?null:param_fmcg_name.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let filename = _.isUndefined(param_filename.value)?null:param_filename.value;
    let user_id = _.isUndefined(param_user_id.value)?null:param_user_id.value;

    let d = new Date();
    let current_datetime = d.getFullYear()+"-"+("0" + (d.getMonth() + 1)).slice(-2)+"-"+("0" + d.getDate()).slice(-2)+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();

    let path_dir = '/opt/data-invoice/'+filename;
    let dest_dir = '/opt/data-invoice/rl_'+fmcg_name+'_'+current_datetime+'_'+filename;
    let total_invoice = 0;
    let action_invoice = "";
    let status = 0;

    try {
        await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let distributor = await service_distributor.getOneByDtCode({dt_code: dt_code});

        if(distributor == null) {
            throw new CustomError(400, 'Data distributor tidak di temukan');
        }

        let remove_2_lines_from_bottom = "head --lines=-2 '" + path_dir + "' > '" +dest_dir+"'";

        await exec(remove_2_lines_from_bottom, (error, stdout, stderr) => {
            //console.log('rmv 2 line');
            //console.log(error);
        });

        await exec("rm -rf '"+path_dir+"'", (error, stdout, stderr) => {
            //console.log('exec rm');
            //console.log(error);
        });

        let sql_command = `mysql -u nodebackend -pSecretpass_@2018 --local-infile=1 unilever -e "LOAD DATA LOCAL INFILE '`+dest_dir+`' INTO TABLE excel_dump FIELDS TERMINATED BY ',' IGNORE 3 LINES (@col1, @col2, @col3, @col4, @col5, @col6, @col7, @col8, @col9, @col10, @col11, @col12, @col13, @col14, @col15, @col16, @col17, @col18, @col19)  SET session_id='123', file_name='`+'rl_'+fmcg_name+'_'+current_datetime+'_'+filename+`', dt_code=@col1, le_code=@col2, outlet_code=@col3, cash_memo_total_amount=@col6, cash_memo_balance_amount=@col7, cashmemo_type=@col8, invoice_id=@col9, invoice_sales_date=@col11, invoice_due_date=@col12, invoice_payment_status=@col13, product=@col14, product_name=@col15, quantity=@col16, product_price_cs=@col17, product_price_dz=@col18, product_price_pc=@col19"`;

        await exec(sql_command, (error, stdout, stderr) => {
            //console.log('exec sql command');
            //console.log(error);
        });

        res.json(new CustomSuccess(200, 'Upload invoice berhasil'));

        let result_task = await Task.create({dt_code: dt_code, original_name: filename, file_name: 'rl_'+fmcg_name+'_'+current_datetime+'_'+filename, status: status, total_row: total_invoice, processed_row: 0, updated_row: 0, action: action_invoice, created: current_datetime, user_id: user_id, total_upload: total_invoice
        });
        let task_id = result_task.id;

        let excel_dump = await service_invoice.enhanceGetDataFromExcelDump({filename: 'rl_'+fmcg_name+'_'+current_datetime+"_"+filename});

        let result_task_id = task_id;
        let total_row = excel_dump[0].total_rows;

        await Task.update({total_row: total_row, total_upload: total_row},{where: {id: result_task_id}});

        await service_invoice.insertInvoiceFromExcelDump({filename: 'rl_'+fmcg_name+'_'+current_datetime+"_"+filename});
        service_invoice.insertProductFromExcelDump({filename: 'rl_'+fmcg_name+'_'+current_datetime+"_"+filename});

        let invoice = await service_invoice.enhanceCheckExcelDumpToInvoice({filename: 'rl_'+fmcg_name+'_'+current_datetime+"_"+filename});

        let processed_invoice = 0;
        let updated_invoice = 1;

        await invoice.map( (data, idx) => {
            if(data.result == 2) {
                let invoice_payment_status = data.ed_invoice_payment_status.split("/");
                let invoice_payment_status_paid = invoice_payment_status[0];
                let invoice_payment_status_unpaid = invoice_payment_status[1];
                
                action_invoice += " invoice_id: "+data.ed_invoice_id+" dt_code: "+data.ed_dt_code+" dari cash memo balance amount "+data.cash_memo_balance_amount+" menjadi "+data.ed_balance_amount+" ,";
                Task.update({action: action_invoice, updated_row:updated_invoice}, {where: {id: task_id}});

                Invoice.update({cash_memo_balance_amount: data.ed_balance_amount, invoice_payment_status_paid: invoice_payment_status_paid, invoice_payment_status_unpaid: invoice_payment_status_unpaid}, {where: {invoice_id: data.ed_invoice_id, dt_code: data.ed_dt_code}});
                updated_invoice++;
            }

            processed_invoice++;

            // if(processed_invoice == Math.floor(invoice.length * 0.2)) {
            //     Task.update({processed_row: processed_invoice}, {where: {id: task_id }});
            // } else if(processed_invoice == Math.floor(invoice.length * 0.4)) {
            //     Task.update({processed_row: processed_invoice}, {where: {id: task_id }});
            // } else if(processed_invoice == Math.floor(invoice.length * 0.6)) {
            //     Task.update({processed_row: processed_invoice}, {where: {id: task_id }});
            // } else if(processed_invoice == Math.floor(invoice.length * 0.8)) {
            //     Task.update({processed_row: processed_invoice}, {where: {id: task_id }});
            // } else if(processed_invoice == invoice.length) {
            //     Task.update({processed_row: processed_invoice}, {where: {id: task_id }});
            // }

        });

        Task.update({processed_row: processed_invoice}, {where: {id: task_id }});
        Task.update({status: 1}, {where: {id: task_id}});

        service_invoice.clearUnregisteredUserInvoice();
    } catch(err) {
        Logs.create({action: `process_invoice`, description: util.inspect(err)});

        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function updatePaymentPembiayaan(req, res) {
    const token = req.swagger.params.token.value;
    const param_dt_code = req.swagger.params.dt_code;
    const param_le_code = req.swagger.params.le_code;
    const param_invoice_id = req.swagger.params.invoice_id;
    const amount = req.swagger.params.amount.value;
    const param_paid_by = req.swagger.params.paid_by;
 
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let le_code = _.isUndefined(param_le_code.value)?null:param_le_code.value;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let paid_by = _.isUndefined(param_paid_by.value)?null:param_paid_by.value;
  
    try {
        
        await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
        await service_invoice.updatePaymentPembiayaan({dt_code:dt_code,le_code:le_code,invoice_id:invoice_id,amount:amount,paid_by:paid_by});
        res.status(200).json({code: 200, body:'success'});
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

export { get_invoices }
export { get_one_invoice }
export { check_process_invoice }
export { get_retur }
export { get_total_unpaid_amount }
export { task_list }
export { process_invoice }
export { updatePaymentPembiayaan }
