'use strict';

import moment from 'moment'
import crypto from 'crypto'
import RandomString from 'randomstring'
import NodeMailer from 'nodemailer'

import { Op } from '~/utils/helper'

import { orm } from '~/services/mariadb'
import { Distributor } from '~/orm/index'
import { allowed_user_status } from '~/properties'

function input({dt_code=null, name=null}={}) {
    return Distributor.create(
        { dt_code: dt_code, name: name }
    );
}

function getOneByDtCode({dt_code=null}={}) {
	return Distributor.findOne({
		attributes: ['distributor_id', 'dt_code', 'name'],
		where: {
			dt_code: dt_code
		}
	});
}

function getAll({dt_code=null}={}) {
	return Distributor.findAll({
		attributes: ['distributor_id', 'dt_code', 'name']
	});
}

function deleteDtCode({ dt_code=null}={}) {
    return Distributor.destroy(
        { where: {  dt_code: dt_code }}
    )
}

let obj = {}
obj.input = input
obj.getOneByDtCode = getOneByDtCode
obj.deleteDtCode = deleteDtCode
obj.getAll = getAll
export default obj
