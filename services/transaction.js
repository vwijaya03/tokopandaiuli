'use strict';
import _ from 'lodash'
import util from 'util'

import middleware from '~/api/controllers/auth'

import helper from '~/utils/helper'

import service_invoice from '~/services/invoice'
import service_formatter from '~/services/formatter'

import { Invoice } from '~/orm/index'
import { Transaction } from '~/orm/index'
import { Logs } from '~/orm/index'
import { orm } from '~/services/mariadb'

import UnauthorizedRequestError from '~/response/error/UnauthorizedRequestError'
import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

async function make_payment(req, res) {
    const token = req.swagger.params.token.value;
    const param_dt_code = req.swagger.params.dt_code;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_amount = req.swagger.params.amount;
    const param_total_potongan = req.swagger.params.total_potongan;
    const param_fromUserId = req.swagger.params.fromUserId;
    const param_toUserId = req.swagger.params.toUserId;
    const param_transact_date = req.swagger.params.transact_date;
    const param_valdo_tx_id = req.swagger.params.valdo_tx_id;
    const param_currency_code = req.swagger.params.currency_code;

    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let amount = _.isUndefined(param_amount.value)?null:param_amount.value;
    let total_potongan = _.isUndefined(param_total_potongan.value)?0:param_total_potongan.value;
    let fromUserId = _.isUndefined(param_fromUserId.value)?null:param_fromUserId.value;
    let toUserId = _.isUndefined(param_toUserId.value)?null:param_toUserId.value;
    let transact_date = _.isUndefined(param_transact_date.value)?null:param_transact_date.value;
    let valdo_tx_id = _.isUndefined(param_valdo_tx_id.value)?null:param_valdo_tx_id.value;
    let currency_code = _.isUndefined(param_currency_code.value)?null:param_currency_code.value;

    let db_transaction;

    try {
        Logs.create({action: 'make_payment', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        db_transaction = await orm.transaction();

        let invoice = await service_invoice.get_one_invoice({invoice_id: invoice_id, dt_code: dt_code});

        let kekurangan = 0;

        if((Number(invoice[0].cash_memo_balance_amount) - Number(total_potongan)) < 0) {
            kekurangan = 0;
        } else {
            kekurangan = Number(invoice[0].cash_memo_balance_amount) - Number(total_potongan);
        }

        if( (Number(amount) + Number(total_potongan)) > Number(invoice[0].cash_memo_balance_amount)) {
            if( (invoice[0].cash_memo_balance_amount -  total_potongan) < 0) {
                Invoice.update({cash_memo_balance_amount: 0}, {where: {invoice_id: invoice_id, dt_code: dt_code}});
                Potongan.update({status: 1}, {where: {trans_invoice_id: invoice_id, dt_code: dt_code}});

                throw new CustomError(400, 'Invoice sudah lunas');
            } else {
                throw new CustomError(400, 'Jumlah yang di inputkan terlalu banyak, kekurangan yang belum di bayar: Rp. '+kekurangan);
            }
        }

        await Invoice.update({cash_memo_balance_amount: invoice[0].cash_memo_balance_amount - amount }, {where: {invoice_id: invoice_id, dt_code: dt_code}, transaction: db_transaction});
        
        await Transaction.create({
            fromUserId: fromUserId,
            toUserId: toUserId,
            dt_code: dt_code,
            invoice_id: invoice_id,
            invoice_code: dt_code+invoice_id,
            valdo_tx_id: valdo_tx_id,
            amount: amount,
            currency_code: currency_code,
            transact_date: transact_date
        }, {transaction: db_transaction});

        await db_transaction.commit();

        res.json(new CustomSuccess(200, 'Pembayaran invoice berhasil'));
    } catch(err) {
        await db_transaction.rollback();

        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }
        
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

export { make_payment }
