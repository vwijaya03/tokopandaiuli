'use strict';

import { data } from '~/properties';
import Sequelize from 'sequelize'
import log from '~/utils/console_log'

const mariadb_config = data.mariadb;

let host = mariadb_config.host;
let port = mariadb_config.port;
let username = mariadb_config.user;
let database = mariadb_config.db;
let password = mariadb_config.password;

let credentialGodrej = ["202.182.53.80", "STAGING_TOKOPANDAI", "tokopandai", "pandai#258"];

let orm = new Sequelize(database, username, password, {
	host: host,
	dialect: 'mysql',
	// logging: false,
	freezeTableName: true,
	timezone: '+07:00',
	pool: {
		max: 100,
		min: 0,
		idle: 200000,
		acquire: 1000000
	},
	operatorsAliases: false
});

let ormGodrej = new Sequelize(credentialGodrej[1], credentialGodrej[2], credentialGodrej[3], {
	dialect: 'mssql',
	host: credentialGodrej[0],
	// logging: false,
	freezeTableName: true,
	timezone: '+07:00',
	pool: {
		max: 100,
		min: 0,
		idle: 200000,
		acquire: 1000000
	},
	operatorsAliases: false
});

function init() {
    return checkConnection();
}

function checkConnection() {
    log.info('checking db connection');

    return orm.query("SELECT 1;").spread((results, metadata) => {
	  	// Results will be an empty array and metadata will contain the number of affected rows.
	  	log.info('DB ULI connection ok.');
	}).then( () => {
		return ormGodrej.query("SELECT 1;").spread((results, metadata) => {
		  	// Results will be an empty array and metadata will contain the number of affected rows.
		  	log.info('DB Godrej connection ok.');
		});
	});
}

let obj = {};
obj.init = init;
obj.checkConnection = checkConnection;

export default obj
export { orm }
export { ormGodrej }
