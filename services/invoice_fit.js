'use strict';

import moment from 'moment'

import { FitInvoice } from '~/orm/index'

import { orm } from '~/services/mariadb'


function find_invoice({dt_code=null,le_code=null,cash_memo_doc_no=null, invoice_toko=null}={}){
    return FitInvoice.count({
		attributes: ['dt_code', 'le_code', 'cash_memo_doc_no', 'invoice_toko'],
		where: {
			dt_code: dt_code,
            le_code: le_code,
            cash_memo_doc_no:cash_memo_doc_no,
            invoice_toko:invoice_toko
		}
	});
}

function get_one_invoice({dt_code=null,le_code=null,cash_memo_doc_no=null, invoice_toko=null}={}){
    try{
        return orm.query('SELECT invoice_toko as id,dt_code,le_code,outlet_code,cashmemo_type,cash_memo_total_amount,total_yg_harus_dibayar,invoice_payment_status,invoice_due_date FROM  invoice_fit where dt_code = "'+dt_code+'" and le_code = "'+le_code+'" and cash_memo_doc_no = "'+cash_memo_doc_no+'" and invoice_toko = "'+invoice_toko+'"', { type: orm.QueryTypes.SELECT})
        .then(function(tasks){
            tasks.forEach(result => {
                 result.id= result.id;
                 result.dt_code= result.dt_code;
                 result.le_code= result.le_code;
                 result.outlet_code= result.outlet_code;
                 result.cashmemo_type= result.cashmemo_type;
                 result.total_amount= result.cash_memo_total_amount;
                 result.balance_amount= result.total_yg_harus_dibayar;
                 result.invoice_payment_status_paid= 0;
                 result.payment_status_unpaid= 0;
                 //result.cicilan= 0;
                 result.sales_date= moment(result.invoice_due_date).format('YYYY-MM-DD HH:mm:ss');
                 result.due_date= moment(result.invoice_due_date).format('YYYY-MM-DD HH:mm:ss');
                 result.is_paid= false;
                 result.is_refunded= false;
                 result.has_refund= false;
                 result.distributor= {dt_code:result.dt_code,name:""};
                 result.transaction= [];
                 result.potongan = [];
                 result.total_potongan = 0;
                 result.product = null
                 result.outlet_name= "";
                //  delete result.invoice_due_date;
                //  delete result.invoice_toko;
                 //delete result.dt_code;
                //  delete result.le_code;
                //  delete result.outlet_code;
                //  delete result.cashmemo_type;
                //  delete result.cash_memo_total_amount;
                //  delete result.total_yg_harus_dibayar;
                //  delete result.invoice_payment_status;
                 
               });
               return tasks;
           });
    }
    catch(err) {
        console.log(err)
	}
}


function insert_invoice({user_id=null,dt_code=null,le_code=null,outlet_code=null,cash_memo_doc_no=null,cash_memo_doc_date=null,
    cash_memo_total_amount=null,cash_memo_balance_amount=null,cashmemo_type=null,invoice_id=null,
    invoice_id_associated_with_retailer_id=null,invoice_sales_date=null,invoice_due_date=null,invoice_payment_status=null,
    product=null,product_name=null,quantity_pc=null,product_price_cs=null,product_pride_dz=null,product_price_pc=null,
    overdue=null,distributor_principle_code=null,invoice_toko=null,principle=null,denda_amount=null,
    total_yg_harus_dibayar=null,credit_limit=null,status_toko=null}={}){
    return FitInvoice.create({
        user_id: user_id,
        dt_code: dt_code,
        le_code: le_code,
        outlet_code: outlet_code,
        cash_memo_doc_no: cash_memo_doc_no,
        cash_memo_doc_date: cash_memo_doc_date,
        cash_memo_total_amount: cash_memo_total_amount,
        cash_memo_balance_amount: cash_memo_balance_amount,
        cashmemo_type: cashmemo_type,
        invoice_id: invoice_id,
        invoice_id_associated_with_retailer_id: invoice_id_associated_with_retailer_id,
        invoice_sales_date: invoice_sales_date,
        invoice_due_date: invoice_due_date,
        invoice_payment_status: invoice_payment_status,
        product: product,
        product_name: product_name,
        quantity_pc: quantity_pc,
        product_price_cs: product_price_cs,
        product_pride_dz: product_pride_dz,
        product_price_pc: product_price_pc,
        overdue: overdue,
        distributor_principle_code: distributor_principle_code,
        invoice_toko: invoice_toko,
        principle: principle,
        denda_amount: denda_amount,
        total_yg_harus_dibayar: total_yg_harus_dibayar,
        credit_limit: credit_limit,
        status_toko: status_toko
    });
}

function update_invoice({le_code=null,dt_code=null,cash_memo_doc_no=null,cash_memo_doc_date=null,
    cash_memo_total_amount=null,cash_memo_balance_amount=null,cashmemo_type=null,invoice_id=null,
    invoice_id_associated_with_retailer_id=null,invoice_sales_date=null,invoice_due_date=null,invoice_payment_status=null,
    product=null,product_name=null,quantity_pc=null,product_price_cs=null,product_pride_dz=null,product_price_pc=null,
    overdue=null,distributor_principle_code=null,invoice_toko=null,principle=null,denda_amount=null,
    total_yg_harus_dibayar=null,credit_limit=null,status_toko=null}={}){
    return FitInvoice.update(
        {
            cash_memo_doc_no: cash_memo_doc_no,
            cash_memo_doc_date: cash_memo_doc_date,
            cash_memo_total_amount: cash_memo_total_amount,
            cash_memo_balance_amount: cash_memo_balance_amount,
            cashmemo_type: cashmemo_type,
            invoice_id: invoice_id,
            invoice_id_associated_with_retailer_id: invoice_id_associated_with_retailer_id,
            invoice_sales_date: invoice_sales_date,
            invoice_due_date: invoice_due_date,
            invoice_payment_status: invoice_payment_status,
            product: product,
            product_name: product_name,
            quantity_pc: quantity_pc,
            product_price_cs: product_price_cs,
            product_pride_dz: product_pride_dz,
            product_price_pc: product_price_pc,
            overdue: overdue,
            distributor_principle_code: distributor_principle_code,
            invoice_toko: invoice_toko,
            principle: principle,
            denda_amount: denda_amount,
            total_yg_harus_dibayar: total_yg_harus_dibayar,
            credit_limit: credit_limit,
            status_toko: status_toko
        }, {
                where:{
                    dt_code: dt_code,
                    le_code: le_code,
                    cash_memo_doc_no:cash_memo_doc_no,
                    invoice_toko:invoice_toko
                }
        }
    );
}

function get_all_invoice({user_id=null,dt_code=null, sorting=null,sortingBy=null, startPage=1, itemsPerPage=15}={}){
    startPage = (startPage < 1 ) ? 1 : startPage;
    const limit = itemsPerPage;
    const offset = ( startPage - 1 ) * itemsPerPage;

    var orderByColumnQuery = '';
    var orderByAscOrDescQuery = '';
    var filterByDtCode= '';

    if(dt_code !== null){
        filterByDtCode = ' and dt_code = '+dt_code
    }
    if(sorting == null)
    {
        orderByAscOrDescQuery = 'asc';
    }
    else
    {
        orderByAscOrDescQuery = sorting;
    }
    if(sortingBy == null)
    {
        orderByColumnQuery = 'order by invoice_due_date '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'le_code')
    {
        orderByColumnQuery = 'order by le_code '+orderByAscOrDescQuery;
    }
    
    return  orm.query('SELECT cash_memo_doc_no,invoice_toko,dt_code,le_code,outlet_code,cashmemo_type,cash_memo_total_amount,total_yg_harus_dibayar,invoice_payment_status,invoice_due_date FROM  invoice_fit where user_id = '+user_id+filterByDtCode+ '  ' + orderByColumnQuery +' limit '+ limit + ' offset '+offset, { type: orm.QueryTypes.SELECT})
          .then(function(tasks){
           tasks.forEach(result => {
                result.id= result.invoice_toko;
                result.dt_code= result.dt_code;
                result.le_code= result.le_code;
                result.outlet_code= result.outlet_code;
                result.cashmemo_type= result.cashmemo_type;
                result.total_amount= result.cash_memo_total_amount;
                result.balance_amount= result.total_yg_harus_dibayar;
                result.invoice_payment_status_paid= 0;
                result.payment_status_unpaid= 0;
                //result.cicilan= 0;
                result.sales_date= moment(result.invoice_due_date).format('YYYY-MM-DD HH:mm:ss');
                result.due_date= moment(result.invoice_due_date).format('YYYY-MM-DD HH:mm:ss');
                result.is_paid= false;
                result.is_refunded= false;
                result.has_refund= false;
                result.distributor= {dt_code:result.dt_code,name:""};
                result.outlet_name= "";
                let teno = result.cash_memo_doc_no.split("-");
                result.tenor = teno[1];
                delete result.invoice_due_date;
                delete result.invoice_toko;
                delete result.cash_memo_total_amount;
                delete result.total_yg_harus_dibayar;
                delete result.invoice_payment_status;
                
              });
              return tasks;

          });
}

function countData({user_id=null,dt_code=null,sorting=null,sortingBy=null, startPage=1, itemsPerPage=15}={}){
    startPage = (startPage < 1 ) ? 1 : startPage;
    var orderByColumnQuery = '';
    var orderByAscOrDescQuery = '';
    var filterByDtCode= '';

    if(dt_code !== null){
        filterByDtCode= ' and dt_code = '+dt_code
    }
    if(sorting == null)
    {
        orderByAscOrDescQuery = 'desc';
    }
    else
    {
        orderByAscOrDescQuery = sorting;
    }
    if(sortingBy == null)
    {
        orderByColumnQuery = 'order by id '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'le_code')
    {
        orderByColumnQuery = 'order by le_code '+orderByAscOrDescQuery;
    }

    try{
        return orm.query('SELECT count(*) as total from invoice_fit where user_id = '+user_id+filterByDtCode+ ' ' + orderByColumnQuery,{type: orm.QueryTypes.SELECT})
    }
    catch(err){
        console.log(err)
    }
}

let obj = {}
obj.find_invoice = find_invoice
obj.get_one_invoice = get_one_invoice
obj.insert_invoice = insert_invoice
obj.get_all_invoice = get_all_invoice
obj.countData = countData
obj.update_invoice = update_invoice

export default obj
