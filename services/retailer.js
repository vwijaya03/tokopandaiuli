'use strict';

import moment from 'moment'
import crypto from 'crypto'
import RandomString from 'randomstring'
import NodeMailer from 'nodemailer'
import { Op } from '~/utils/helper'
import { orm } from '~/services/mariadb'
import { Retailer } from '~/orm/index'
import { allowed_user_status } from '~/properties'

function getOneByDtCode({retailer_id=null}={}) {
	return Retailer.findOne({
    attributes: ['retailer_id','distributor_id', 'outlet_code', 'name','address','le_code_dry','le_code_ice','le_code_ufs'],
		where: {

      retailer_id:retailer_id
		}
	});
}

function input({name=null, address=null,le_code_dry=null}={}) {
    return Retailer.create(
        { name: name, address: address,le_code_dry:le_code_dry }
    );
}

function getAll({}={}) {
	return Retailer.findAll({
		attributes: ['retailer_id','distributor_id', 'outlet_code', 'name','address','le_code_dry','le_code_ice','le_code_ufs']
	});
}

function postUploadRetailer({records=null}={}) {
	for(let j = 1; j < records.Participant.length; j++){
		Retailer.findOne({
            where: {le_code_dry: records.Participant[j].C}
        })
        .then(result => {
            if(result == null) {
              return Retailer.create({le_code_dry: records.Participant[j].C, name: records.Participant[j].A, address: records.Participant[j].B});
						}
        });
		}
}

let obj = {}
obj.postUploadRetailer = postUploadRetailer
obj.getOneByDtCode = getOneByDtCode
obj.getAll = getAll
obj.input =input
export default obj
