import RandomString from 'randomstring'

import helper from '~/utils/helper'

import Invoice from '~/orm/invoice'
import Potongan from '~/orm/potongan'

import { orm } from '~/services/mariadb'
import { Op } from '~/utils/helper'

import CustomError from '~/response/error/CustomError'

async function get_invoices({outletCode=null, distributorCode=null, leCode=null, isPaid=false, isUnpaid=false, filter1=null, filter2=null, startPage=1, itemsPerPage=15, sorting=null, sortingBy=null, filter_by=null,  usertype=null, platform=null, join_date=null,paid_by=null}={}) {
    // normalize startPage to 1 if less than 1.
    startPage = (startPage < 1 ) ? 1 : startPage;

    const limit = itemsPerPage;
    const offset = ( startPage - 1 ) * itemsPerPage;

    let filter = {};
    let table = Invoice;
    let defaultQuery;
    let defaultQueryisPaid = 'where invoice.cash_memo_balance_amount = 0 and invoice.hide_status = 0';
    let defaultQueryisUnPaid = 'where invoice.cash_memo_balance_amount > 0 and invoice.hide_status = 0';
    let whereDateQuery;
    let whereOutletCodeQuery;
    let whereDistributorCodeQuery;
    let whereQuery;
    let orderByColumnQuery = '';
    let orderByAscOrDescQuery = '';
    let whereLeCode = '';
    let whereJoinDate = '';
    let wherePaidBy = '';
    let groupByInvoiceCode = '';

    if(usertype == 1 || usertype == 3) {
        if(isPaid == true)
        {
            defaultQuery = `where (select SUM(transactions.amount) from transactions where transactions.invoice_id = invoice.invoice_id and transactions.dt_code = invoice.dt_code and transactions.transact_date <= "${filter2} 23:59:59" limit 1) + invoice.invoice_payment_status_paid + IF((select sum(amount_potongan) from potongan where trans_invoice_id = invoice.invoice_id and dt_code = invoice.dt_code limit 1) IS NULL, 0, (select sum(amount_potongan) from potongan where trans_invoice_id = invoice.invoice_id and dt_code = invoice.dt_code limit 1)) = invoice.cash_memo_total_amount and invoice.hide_status = 0`
        }
        else if(isUnpaid == true)
        {
            defaultQuery = `where (select SUM(transactions.amount) from transactions where transactions.invoice_id = invoice.invoice_id and transactions.dt_code = invoice.dt_code and transactions.transact_date <= "${filter2} 23:59:59" limit 1) + invoice.invoice_payment_status_paid + IF((select sum(amount_potongan) from potongan where trans_invoice_id = invoice.invoice_id and dt_code = invoice.dt_code limit 1) IS NULL, 0, (select sum(amount_potongan) from potongan where trans_invoice_id = invoice.invoice_id and dt_code = invoice.dt_code limit 1)) != invoice.cash_memo_total_amount and invoice.hide_status = 0`
        }
        else
        {
            defaultQuery = 'where invoice.hide_status = 0';
        }
    } else {
        if(isPaid == true)
        {
            defaultQuery = 'where invoice.cash_memo_balance_amount = 0 and invoice.hide_status = 0'
        }
        else if(isUnpaid == true)
        {
            defaultQuery = 'where invoice.cash_memo_balance_amount > 0 and invoice.hide_status = 0'
        }
        else
        {
            defaultQuery = 'where invoice.cash_memo_balance_amount >= 0 and invoice.hide_status = 0';
        }
    }

    if(sorting == null)
    {
        orderByAscOrDescQuery = 'desc';
    }
    else
    {
        orderByAscOrDescQuery = sorting;
    }

    if(sortingBy == null)
    {
        orderByColumnQuery = 'order by invoice.invoice_sales_date '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'sales_date')
    {
        orderByColumnQuery = 'order by invoice.invoice_sales_date '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'paid_date')
    {
        orderByColumnQuery = 'order by transact_date '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'due_date')
    {
        orderByColumnQuery = 'order by invoice.invoice_due_date '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'invoice_id')
    {
        orderByColumnQuery = 'order by invoice.invoice_id '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'outlet_code')
    {
        orderByColumnQuery = 'order by invoice.outlet_code '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'is_paid')
    {
        orderByColumnQuery = 'order by is_paid '+orderByAscOrDescQuery;
    }

    if(paid_by !== null){
        wherePaidBy = 'and paid_by = "'+paid_by+'"'
    } else if(paid_by == null){
        wherePaidBy = 'and paid_by = ""'
    }
    
    if(filter_by != null)
    {
        if(filter_by == 'sales_date')
        {
            if(filter1 && filter2){
                whereDateQuery = 'and invoice.invoice_sales_date >= '+"'"+filter1+"'"+' and invoice.invoice_sales_date <= '+"'"+filter2+" 23:59:59'";
            }
            else{
                throw new Error('Masukkan tanggal awal dan tanggal akhir.');
            }
        }
        else if(filter_by == 'due_date')
        {
            if(filter1 && filter2){
                whereDateQuery = 'and invoice.invoice_due_date >= '+"'"+filter1+"'"+' and invoice.invoice_due_date <= '+"'"+filter2+" 23:59:59'";
            }
            else{
                throw new Error('Masukkan tanggal awal dan tanggal akhir.');
            }

        }
        else if(filter_by == 'paid_date')
        {
            if(platform == "web") {
                if(filter1 && filter2){
                    whereDateQuery = `and (select MAX(transactions.transact_date) from transactions where transactions.invoice_id = invoice.invoice_id and transactions.dt_code = invoice.dt_code and transactions.transact_date >= "${filter1}" and transactions.transact_date <= "${filter2} 23:59:59" limit 1) >= "${filter1}" and (select MAX(transactions.transact_date) from transactions where transactions.invoice_id = invoice.invoice_id and transactions.dt_code = invoice.dt_code and transactions.transact_date >= "${filter1}" and transactions.transact_date <= "${filter2} 23:59:59" limit 1) <= "${filter2} 23:59:59"`;
                }
                else{
                    throw new Error('Masukkan tanggal awal dan tanggal akhir.');
                }
            } else {
                if(filter1 && filter2){
                    whereDateQuery = 'and transactions.transact_date >= '+"'"+filter1+"'"+' and transactions.transact_date <= '+"'"+filter2+" 23:59:59'";
                }
                else{
                    throw new Error('Masukkan tanggal awal dan tanggal akhir.');
                }
            }
        }
        else if(filter_by == 'invoice_id')
        {
            if(filter1){
                whereDateQuery = `and invoice.invoice_id = "${filter1}"`;
            }
        }
        else if(filter_by == 'le_code')
        {
            if(filter1){
                whereDateQuery = `and invoice.le_code = "${filter1}"`;
            }
        }
        else if(filter_by == 'dt_code')
        {
            if(filter1){
                whereDateQuery = `and invoice.dt_code = "${filter1}"`;
            }
        }
        else 
        {
            if(platform == "web") {
                if(outletCode.length < 2) {
                    let today = helper.formatCurrentDateTime();
                    let last_7days = helper.formatCustomCurrentDateTime(0, 30, 0, 0, 0, "minus");

                    whereDateQuery = `and invoice.invoice_due_date BETWEEN "${last_7days}" AND "${today}" `;
                }
            }
        }
    } 

    if(usertype == 0) {
        if(join_date != null) {
            whereJoinDate = "and invoice_due_date >= "+"'"+join_date+"'";
        } else {
            whereJoinDate = "and invoice_due_date = NULL";
        }

        if(leCode)
            whereLeCode = `and invoice.le_code in (${leCode})`;
    }

    if (outletCode) {
        whereOutletCodeQuery = 'and invoice.outlet_code = '+outletCode;
    }
    if (distributorCode) {
        whereDistributorCodeQuery = 'and invoice.dt_code = '+distributorCode;
    }

    if(usertype == 1 || usertype == 3 || usertype == 4) {
        whereDistributorCodeQuery = `and invoice.dt_code in (${distributorCode})`;
    }

    if(whereDateQuery == null)
        whereDateQuery = '';
    if(whereOutletCodeQuery == null)
        whereOutletCodeQuery = '';
    if(whereDistributorCodeQuery == null)
        whereDistributorCodeQuery = '';

    whereQuery = whereDateQuery+' '+whereOutletCodeQuery+' '+whereDistributorCodeQuery+' '+whereLeCode+' '+whereJoinDate;

    if(whereQuery == null) {
        whereQuery = '';
    }

    if(platform == 'web')
    {
        // if(filter_by == 'paid_date')
        // {
        //     if(filter1 && filter2){
        //         whereDateQuery = 'and (select MAX(transactions.transact_date) from transactions where transactions.invoice_id = invoice.invoice_id and transactions.dt_code = invoice.dt_code limit 1) >= '+"'"+filter1+"'"+' and (select MAX(transactions.transact_date) from transactions where transactions.invoice_id = invoice.invoice_id and transactions.dt_code = invoice.dt_code limit 1) <= '+"'"+filter2+" 23:59:59'";
        //     }
        //     else{
        //         throw new Error('Masukkan tanggal awal dan tanggal akhir.');
        //     }
        // }

        // whereQuery = '';

        if(whereQuery == null) {
            whereQuery = '';
        }

        if(itemsPerPage == 0)
        {
            let invoices = await orm.query('select (select sum(amount_potongan) from potongan where trans_invoice_id = invoice.invoice_id and dt_code = invoice.dt_code limit 1) as total_potongan, dist.name, dist.dt_code, invoice.invoice_id,invoice.paid_by, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on transactions.dt_code = invoice.dt_code and transactions.invoice_id = invoice.invoice_id inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQuery+' '+whereQuery+' group by invoice.id, dist.name, dist.dt_code, transactions.invoice_code '+orderByColumnQuery, { type: orm.QueryTypes.SELECT});

            invoices.forEach(result => {
                let milisecond_transact_date = new Date(String(result.transact_date));
                let milisecond_sales_date = new Date(String(result.invoice_sales_date));
                let milisecond_due_date = new Date(String(result.invoice_due_date));

                result.transact_date = milisecond_transact_date.getTime();
                result.invoice_sales_date = milisecond_sales_date.getTime();
                result.invoice_due_date = milisecond_due_date.getTime();
                result.distributor = JSON.parse(`{"dt_code": "${result.dt_code}", "name": "${result.name}"}`);
                result.total_potongan = (result.total_potongan == null) ? 0 : result.total_potongan;

                result.is_paid = (result.is_paid == 1)
                result.is_refunded = (result.is_refunded == 1)
                result.has_refund = (result.has_refund == 1)
            });

            let response = await orm.query('select count(*) as total_items from (select count(invoice.invoice_id) as total_items from invoice left outer join transactions on transactions.dt_code = invoice.dt_code and transactions.invoice_id = invoice.invoice_id '+defaultQuery+' '+whereQuery+' group by invoice.id) as T', { type: orm.QueryTypes.SELECT});

            return {list:invoices, total:response[0].total_items};
        }

        let invoices = null;

        if(filter_by == 'paid_date') {
            invoices = await orm.query(`
            select 
                (select sum(amount_potongan) from potongan where trans_invoice_id = invoice.invoice_id and dt_code = invoice.dt_code limit 1) as total_potongan, 	
                (select distributors.name from distributors where distributors.dt_code = invoice.dt_code limit 1) as name,
                invoice.invoice_id,invoice.paid_by, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, 
                
                IF(invoice.cash_memo_balance_amount = 0 AND ( (select SUM(transactions.amount) from transactions where transactions.invoice_id = invoice.invoice_id and transactions.dt_code = invoice.dt_code and transactions.transact_date <= "${filter2} 23:59:59" limit 1) + invoice.invoice_payment_status_paid + IF((select sum(amount_potongan) from potongan where trans_invoice_id = invoice.invoice_id and dt_code = invoice.dt_code limit 1) IS NULL, 0, (select sum(amount_potongan) from potongan where trans_invoice_id = invoice.invoice_id and dt_code = invoice.dt_code limit 1)) = invoice.cash_memo_total_amount), true, false) as is_paid,
                
                false as has_refund, false as is_refunded,

                (select MAX(transactions.transact_date) from transactions where transactions.invoice_id = invoice.invoice_id and transactions.dt_code = invoice.dt_code and transactions.transact_date >= "${filter1}" and transactions.transact_date <= "${filter2} 23:59:59" limit 1) as transact_date,

                (select SUM(transactions.amount) from transactions where transactions.invoice_id = invoice.invoice_id and transactions.dt_code = invoice.dt_code and transactions.transact_date <= "${filter2} 23:59:59" limit 1) as paid_amount    
            from invoice ${defaultQuery} ${whereQuery} group by invoice.id ${orderByColumnQuery} limit ${limit} offset ${offset}
            `, { type: orm.QueryTypes.SELECT});
        } else {
            invoices = await orm.query(`
            select 
                (select sum(amount_potongan) from potongan where trans_invoice_id = invoice.invoice_id and dt_code = invoice.dt_code and remark_app !="retur_cash_uli" limit 1) as total_potongan,
(select sum(amount_potongan) from potongan where trans_invoice_id = invoice.invoice_id and dt_code = invoice.dt_code and remark_app ="retur_cash_uli" limit 1) as total_potongan_retur, 
(select remark_potongan from potongan where trans_invoice_id = invoice.invoice_id and dt_code = invoice.dt_code and remark_app ="retur_cash_uli" limit 1) as remark_potongan_retur, 	
                (select distributors.name from distributors where distributors.dt_code = invoice.dt_code limit 1) as name,
                invoice.invoice_id, invoice.le_code,invoice.paid_by, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded,
                (select MAX(transactions.transact_date) from transactions where transactions.invoice_id = invoice.invoice_id and transactions.dt_code = invoice.dt_code limit 1) as transact_date,
                (select SUM(transactions.amount) from transactions where transactions.invoice_id = invoice.invoice_id and transactions.dt_code = invoice.dt_code limit 1) as paid_amount    
            from invoice ${defaultQuery} ${whereQuery} group by invoice.id ${orderByColumnQuery} limit ${limit} offset ${offset}
            `, { type: orm.QueryTypes.SELECT});
        }

        // let invoices = await orm.query('select (select sum(amount_potongan) from potongan where trans_invoice_id = invoice.invoice_id and dt_code = invoice.dt_code limit 1) as total_potongan, dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on transactions.dt_code = invoice.dt_code and transactions.invoice_id = invoice.invoice_id inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQuery+' '+whereQuery+' group by invoice.id, dist.name, dist.dt_code, transactions.invoice_code '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT});

        invoices.forEach(result => {
            let milisecond_transact_date = new Date(String(result.transact_date));
            let milisecond_sales_date = new Date(String(result.invoice_sales_date));
            let milisecond_due_date = new Date(String(result.invoice_due_date));

            result.transact_date = milisecond_transact_date.getTime();
            result.invoice_sales_date = milisecond_sales_date.getTime();
            result.invoice_due_date = milisecond_due_date.getTime();
            result.distributor = JSON.parse(`{"dt_code": "${result.distributorCode}", "name": "${result.name}"}`);
            result.total_potongan = (result.total_potongan == null) ? 0 : result.total_potongan;
	        result.total_potongan_retur = (result.total_potongan_retur == null) ? 0 : result.total_potongan_retur;
	        result.remark_potongan_retur = (result.remark_potongan_retur == null) ? "-" : result.remark_potongan_retur;
            
            result.is_paid = (result.is_paid == 1)
            result.is_refunded = (result.is_refunded == 1)
            result.has_refund = (result.has_refund == 1)
        });

        let response = await orm.query('select count(*) as total_items from (select count(invoice.invoice_id) as total_items from invoice left outer join transactions on transactions.dt_code = invoice.dt_code and transactions.invoice_id = invoice.invoice_id '+defaultQuery+' '+whereQuery+' group by invoice.id) as T', { type: orm.QueryTypes.SELECT});

        let total_unpaid_items = await orm.query('select count(*) as total_unpaid_items from (select count(invoice.invoice_id) as total_unpaid_items from invoice left outer join transactions on transactions.dt_code = invoice.dt_code and transactions.invoice_id = invoice.invoice_id where invoice.cash_memo_balance_amount > 0 '+whereQuery+' group by invoice.id) as T', { type: orm.QueryTypes.SELECT});

        return {list:invoices, total:response[0].total_items, total_unpaid:total_unpaid_items[0].total_unpaid_items};
    }
    else if(platform == 'android')
    {
        if(usertype == 4)
        {
            if(leCode) {
                whereLeCode = `and invoice.le_code in (${leCode})`;
            } else {
                whereLeCode = ``;
            }

            let invoices = await orm.query('select * from ((select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code, invoice.paid_by from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQueryisUnPaid+' '+whereQuery+' '+whereLeCode+' group by invoice.id, dist.name, dist.dt_code, transactions.invoice_code '+'order by invoice.invoice_due_date asc'+' limit '+limit+' offset '+offset+') UNION (select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code, invoice.paid_by from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQueryisPaid+' '+whereQuery+' '+whereLeCode+' group by invoice.id, dist.name, dist.dt_code, transactions.invoice_code '+'order by invoice.invoice_due_date desc'+' limit '+limit+' offset '+offset+')) as DataCountPaidAndUnPaid', { replacements: { search_by_nama_toko_or_le_code: leCode}, type: orm.QueryTypes.SELECT});

            invoices.forEach(result => {
                let milisecond_transact_date = new Date(String(result.transact_date));
                let milisecond_sales_date = new Date(String(result.invoice_sales_date));
                let milisecond_due_date = new Date(String(result.invoice_due_date));

                result.transact_date = milisecond_transact_date.getTime();
                result.invoice_sales_date = milisecond_sales_date.getTime();
                result.invoice_due_date = milisecond_due_date.getTime();
                result.distributor = JSON.parse(`{"dt_code": "${result.dt_code}", "name": "${result.name}"}`);
		
                result.is_paid = (result.is_paid == 1)
                result.is_refunded = (result.is_refunded == 1)
                result.has_refund = (result.has_refund == 1)
            });

            let response = await orm.query('select count(le_code) as total_items from ((select dist.name, dist.dt_code , invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code, invoice.paid_by from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQueryisUnPaid+' '+whereQuery+' '+whereLeCode+' group by invoice.id, dist.name, dist.dt_code, transactions.invoice_code '+'order by invoice.invoice_due_date asc) UNION (select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code, invoice.paid_by from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQueryisPaid+' '+whereQuery+' '+whereLeCode+' group by invoice.id, dist.name, dist.dt_code, transactions.invoice_code '+'order by invoice.invoice_due_date desc)) as DataCountPaidAndUnPaid', { replacements: { search_by_nama_toko_or_le_code: leCode}, type: orm.QueryTypes.SELECT});

            let total_unpaid_items = await orm.query('select count(*) as total_unpaid_items from (select count(invoice.invoice_id) as total_unpaid_items from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code where invoice.cash_memo_balance_amount > 0 '+whereQuery+' '+whereLeCode+' group by invoice.id) as T', { type: orm.QueryTypes.SELECT});

            return {list:invoices, total:response[0].total_items, total_unpaid:total_unpaid_items[0].total_unpaid_items};
        }
        else
        {
            let invoices = await orm.query(`(select dist.name, dist.dt_code,invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code, invoice.paid_by from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code `+defaultQueryisUnPaid+` `+whereQuery+` group by invoice.id, dist.name, dist.dt_code, transactions.invoice_code order by invoice.invoice_due_date asc`+` limit `+limit+` offset `+offset+`) UNION (select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code, invoice.paid_by from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code `+defaultQueryisPaid+` `+whereQuery+` group by invoice.id, dist.name, dist.dt_code, transactions.invoice_code order by invoice.invoice_due_date desc`+` limit `+limit+` offset `+offset+`)`, { type: orm.QueryTypes.SELECT});

            invoices.forEach(result => {
                let milisecond_transact_date = new Date(String(result.transact_date));
                let milisecond_sales_date = new Date(String(result.invoice_sales_date));
                let milisecond_due_date = new Date(String(result.invoice_due_date));

                result.transact_date = milisecond_transact_date.getTime();
                result.invoice_sales_date = milisecond_sales_date.getTime();
                result.invoice_due_date = milisecond_due_date.getTime();
                result.distributor = JSON.parse(`{"dt_code": "${result.dt_code}", "name": "${result.name}"}`);

                result.is_paid = (result.is_paid == 1)
                result.is_refunded = (result.is_refunded == 1)
                result.has_refund = (result.has_refund == 1)
            });

            let response = await orm.query(`select count(le_code) as total_items from ((select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code `+defaultQueryisUnPaid+` `+whereQuery+` group by invoice.id, dist.name, dist.dt_code, transactions.invoice_code order by invoice.invoice_due_date asc`+`) UNION (select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code `+defaultQueryisPaid+` `+whereQuery+` group by invoice.id, dist.name, dist.dt_code, transactions.invoice_code order by invoice.invoice_due_date desc`+`)) as DataCountPaidAndUnPaid`, { type: orm.QueryTypes.SELECT});

            let total_unpaid_items = await orm.query(`select count(*) as total_unpaid_items from (select count(invoice.invoice_id) as total_unpaid_items from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code `+defaultQueryisUnPaid+` `+whereQuery+` group by invoice.id) as T`, { type: orm.QueryTypes.SELECT});

            return {list:invoices, total:response[0].total_items, total_unpaid:total_unpaid_items[0].total_unpaid_items};
        }

    } else if(platform == 'ios') {

    } else {

    }
}


async function get_one_invoice({invoice_id=null, dt_code=null}={}) {
    let data = await orm.query('select dist.name, dist.dt_code, invoice.invoice_id, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.le_code, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, invoice.invoice_details from invoice inner join distributors as dist ON dist.dt_code = invoice.dt_code where invoice.hide_status = 0 and invoice.invoice_id = "'+invoice_id+'" AND invoice.dt_code = '+dt_code, { type: orm.QueryTypes.SELECT});

    await data.forEach(result => {
        let milisecond_transact_date = new Date(String(result.transact_date));
        let milisecond_sales_date = new Date(String(result.invoice_sales_date));
        let milisecond_due_date = new Date(String(result.invoice_due_date));

        if(isNaN(milisecond_transact_date)) {
            delete result.transact_date;
        } else {
            result.transact_date = milisecond_transact_date.getTime();
        }

        result.distributor = JSON.parse(`{"dt_code": "${result.dt_code}", "name": "${result.name}"}`);
        result.invoice_sales_date = milisecond_sales_date.getTime();
        result.invoice_due_date = milisecond_due_date.getTime();

        result.is_paid = (result.is_paid == 1)
        result.is_refunded = (result.is_refunded == 1)
        result.has_refund = (result.has_refund == 1)
    });

    if(data.length == 0) {
        throw new CustomError(400, 'Data invoice tidak di temukan');
    }

    let products = await orm.query('select * from products where invoice_id = "'+invoice_id+'" and dt_code = '+dt_code, { type: orm.QueryTypes.SELECT});
    let transactions = await orm.query('select transaction_id as id, amount, DATE_FORMAT(convert_tz(transact_date, @@session.time_zone,"+07:00"),"%Y-%m-%d %H:%i:%s") AS date_time from transactions where invoice_code = "'+dt_code+invoice_id+'"', { type: orm.QueryTypes.SELECT});
    let check_last_number_invoice_id = invoice_id.substr(-1);
    let item = '';

    if(check_last_number_invoice_id == 0) {
        let check_invoice_id = invoice_id.substr(0, invoice_id.length - 1);
        let response = await Invoice.findAll({
            attributes: ['invoice_id', 'cash_memo_total_amount', 'cash_memo_balance_amount'],
            where: {
                invoice_id: {
                  [Op.like]: '%'+check_invoice_id+'1'+'%'
                },
                dt_code: dt_code,
                hide_status: 0
            },
        });

        item = data;
        item[0].products = products;
        item[0].retur = response;
        item[0].transactions = transactions;
    }
    else {
        item = data;
        item[0].products = products;
        item[0].transactions = transactions;
    }

    let potongan = await orm.query(`select remark_app,trim(trans_invoice_id) as invoice_id, trim(dt_code) as dt_code, trim(le_code) as le_code, amount_potongan as amount, trim(remark_potongan) as remark from potongan where trans_invoice_id = '${invoice_id}' and dt_code = '${dt_code}' and remark_app !="retur_cash_uli"`, { type: orm.QueryTypes.SELECT});

    let total_potongan = 0;
    potongan.map( (val, index) => {
        total_potongan =+ total_potongan + val.amount
    });

    let retur = await orm.query(`select remark_app,trim(trans_invoice_id) as invoice_id, trim(dt_code) as dt_code, trim(le_code) as le_code, amount_potongan as amount, trim(remark_potongan) as remark from potongan where trans_invoice_id = '${invoice_id}' and dt_code = '${dt_code}' and remark_app ="retur_cash_uli"`, { type: orm.QueryTypes.SELECT});
    let total_potongan_retur = 0;

    retur.map( (val, index) => {
        total_potongan_retur =+ total_potongan_retur + val.amount
    });

    item[0].potongan = potongan;
    item[0].total_potongan = Number(total_potongan);
    item[0].retur = retur;
    item[0].total_potongan_retur = Number(total_potongan_retur);

    if(Number(total_potongan) >= Number(item[0].cash_memo_balance_amount)) {
        await Invoice.update({ cash_memo_balance_amount: 0 }, {where: {invoice_id: invoice_id, dt_code: dt_code}});
        await Potongan.update({status: 1 }, {where: {trans_invoice_id: invoice_id, dt_code: dt_code}});
        item[0].cash_memo_balance_amount = 0;
        item[0].is_paid = true;

        return item;
    } else if(Number(total_potongan_retur) >= Number(item[0].cash_memo_balance_amount)) {
        await Invoice.update({ cash_memo_balance_amount: 0 }, {where: {invoice_id: invoice_id, dt_code: dt_code}});
        await Potongan.update({status: 1 }, {where: {trans_invoice_id: invoice_id, dt_code: dt_code}});
        item[0].cash_memo_balance_amount = 0;
        item[0].is_paid = true;
        return item;
    }else{
        return item;
    }
    
   
}

async function get_retur({outletCode=null, distributorCode=null, leCode=null, startDate=null, endDate=null, startPage=1, itemsPerPage=15, sorting=null, sortingBy=null, filterBy=null, usertype=null, platform=null, join_date=null}={}) {

    startPage = (startPage < 1 ) ? 1 : startPage;

    const limit = itemsPerPage;
    const offset = ( startPage - 1 ) * itemsPerPage;

    let filter = {};
    let table = Invoice;
    let defaultQuery;
    let defaultQueryisUnPaid = 'where invoice.cash_memo_balance_amount > 0';
    let whereDateQuery;
    let whereOutletCodeQuery;
    let whereDistributorCodeQuery;
    let whereQuery;
    let orderByColumnQuery = '';
    let orderByAscOrDescQuery = '';
    let whereLeCode = '';
    let whereJoinDate = '';
    let groupByInvoiceCode = '';

    defaultQuery = 'where invoice.cash_memo_balance_amount < 0';

    if(sorting == null)
    {
        orderByAscOrDescQuery = 'desc';
    }
    else
    {
        orderByAscOrDescQuery = sorting;
    }

    if(sortingBy == null)
    {
        orderByColumnQuery = 'order by invoice.invoice_sales_date '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'sales_date')
    {
        orderByColumnQuery = 'order by invoice.invoice_sales_date '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'paid_date')
    {
        orderByColumnQuery = 'order by transact_date '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'due_date')
    {
        orderByColumnQuery = 'order by invoice.invoice_due_date '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'invoice_id')
    {
        orderByColumnQuery = 'order by invoice.invoice_id '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'outlet_code')
    {
        orderByColumnQuery = 'order by invoice.outlet_code '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'is_paid')
    {
        orderByColumnQuery = 'order by is_paid '+orderByAscOrDescQuery;
    }

    if(filterBy != null)
    {
        if(filterBy == 'sales_date')
        {
            if(startDate && endDate){
                whereDateQuery = 'and invoice.invoice_sales_date >= '+"'"+startDate+"'"+' and invoice.invoice_sales_date <= '+"'"+endDate+" 23:59:59'";
            }
            else{
                throw new Error('Masukkan tanggal awal dan tanggal akhir.');
            }
        }
        else if(filterBy == 'due_date')
        {
            if(startDate && endDate){
                whereDateQuery = 'and invoice.invoice_due_date >= '+"'"+startDate+"'"+' and invoice.invoice_due_date <= '+"'"+endDate+" 23:59:59'";
            }
            else{
                throw new Error('Masukkan tanggal awal dan tanggal akhir.');
            }

        }
        else if(filterBy == 'paid_date')
        {
            if(startDate && endDate){
                whereDateQuery = 'and transactions.transact_date >= '+"'"+startDate+"'"+' and transactions.transact_date <= '+"'"+endDate+" 23:59:59'";
            }
            else{
                throw new Error('Masukkan tanggal awal dan tanggal akhir.');
            }

        }
    }

    if(usertype == 0) {
        if(join_date != null) {
            whereJoinDate = "and invoice_due_date >= "+"'"+join_date+"'";
        } else {
            whereJoinDate = "and invoice_due_date = NULL";
        }

        if(leCode)
            whereLeCode = `and invoice.le_code in (${leCode})`;
    }

    if (outletCode) {
        whereOutletCodeQuery = 'and invoice.outlet_code = '+outletCode;
    }
    if (distributorCode) {
        whereDistributorCodeQuery = 'and invoice.dt_code = '+distributorCode;
    }

    if(usertype == 1 || usertype == 3 || usertype == 4) {
        whereDistributorCodeQuery = `and invoice.dt_code in (${distributorCode})`;
    }

    if(whereDateQuery == null)
        whereDateQuery = '';
    if(whereOutletCodeQuery == null)
        whereOutletCodeQuery = '';
    if(whereDistributorCodeQuery == null)
        whereDistributorCodeQuery = '';

    whereQuery = whereDateQuery+' '+whereOutletCodeQuery+' '+whereDistributorCodeQuery+' '+whereLeCode+' '+whereJoinDate;

    if(whereQuery == null) {
        whereQuery = '';
    }

    if(platform == 'web')
    {
        if(itemsPerPage == 0)
        {
            let invoices = await orm.query('select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQuery+' '+whereQuery+' group by invoice.id '+orderByColumnQuery, { type: orm.QueryTypes.SELECT});

            invoices.forEach(result => {
                let milisecond_transact_date = new Date(String(result.transact_date));
                let milisecond_sales_date = new Date(String(result.invoice_sales_date));
                let milisecond_due_date = new Date(String(result.invoice_due_date));

                result.transact_date = milisecond_transact_date.getTime();
                result.invoice_sales_date = milisecond_sales_date.getTime();
                result.invoice_due_date = milisecond_due_date.getTime();
                result.distributor = JSON.parse(`{"dt_code": "${result.dt_code}", "name": "${result.name}"}`);

                result.is_paid = (result.is_paid == 1)
                result.is_refunded = (result.is_refunded == 1)
                result.has_refund = (result.has_refund == 1)
            });

            let response = await orm.query('select count(*) as total_items from (select count(invoice.invoice_id) as total_items from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code '+defaultQuery+' '+whereQuery+' group by invoice.id) as T', { type: orm.QueryTypes.SELECT});

            return {list:invoices, total:response[0].total_items};
        }

        let invoices = await orm.query('select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQuery+' '+whereQuery+' group by invoice.id '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT});

        invoices.forEach(result => {
            let milisecond_transact_date = new Date(String(result.transact_date));
            let milisecond_sales_date = new Date(String(result.invoice_sales_date));
            let milisecond_due_date = new Date(String(result.invoice_due_date));

            result.transact_date = milisecond_transact_date.getTime();
            result.invoice_sales_date = milisecond_sales_date.getTime();
            result.invoice_due_date = milisecond_due_date.getTime();
            result.distributor = JSON.parse(`{"dt_code": "${result.dt_code}", "name": "${result.name}"}`);

            result.is_paid = (result.is_paid == 1)
            result.is_refunded = (result.is_refunded == 1)
            result.has_refund = (result.has_refund == 1)
        });

        let response = await orm.query('select count(*) as total_items from (select count(invoice.invoice_id) as total_items from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code '+defaultQuery+' '+whereQuery+' group by invoice.id) as T', { type: orm.QueryTypes.SELECT});

        let total_unpaid_items = await orm.query('select count(*) as total_unpaid_items from (select count(invoice.invoice_id) as total_unpaid_items from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code '+defaultQuery+' '+whereQuery+' group by invoice.id) as T', { type: orm.QueryTypes.SELECT});

        return {list:invoices, total:response[0].total_items, total_unpaid:total_unpaid_items[0].total_unpaid_items};
    }
    else if(platform == 'android')
    {
        if(usertype == 4)
        {
            if(leCode) {
                whereLeCode = `and invoice.le_code in (${leCode})`;
            } else {
                whereLeCode = ``;
            }

            let invoices = await orm.query('select * from (select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQuery+' '+whereQuery+' '+whereLeCode+' group by invoice.id '+'order by invoice.invoice_due_date desc'+' limit '+limit+' offset '+offset+') as DataCountPaidAndUnPaid', { type: orm.QueryTypes.SELECT});

            invoices.forEach(result => {
                let milisecond_transact_date = new Date(String(result.transact_date));
                let milisecond_sales_date = new Date(String(result.invoice_sales_date));
                let milisecond_due_date = new Date(String(result.invoice_due_date));

                result.transact_date = milisecond_transact_date.getTime();
                result.invoice_sales_date = milisecond_sales_date.getTime();
                result.invoice_due_date = milisecond_due_date.getTime();
                result.distributor = JSON.parse(`{"dt_code": "${result.dt_code}", "name": "${result.name}"}`);

                result.is_paid = (result.is_paid == 1)
                result.is_refunded = (result.is_refunded == 1)
                result.has_refund = (result.has_refund == 1)
            });

            let response = await orm.query('select count(le_code) as total_items from (select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQuery+' '+whereQuery+' '+whereLeCode+' group by invoice.id '+'order by invoice.invoice_due_date desc) as DataCountPaidAndUnPaid', { type: orm.QueryTypes.SELECT});

            let total_unpaid_items = await orm.query('select count(*) as total_unpaid_items from (select count(invoice.invoice_id) as total_unpaid_items from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code '+defaultQueryisUnPaid+' '+whereQuery+' '+whereLeCode+' group by invoice.id) as T', { type: orm.QueryTypes.SELECT});

            return {list:invoices, total:response[0].total_items, total_unpaid:total_unpaid_items[0].total_unpaid_items};
        }
        else
        {
            let invoices = await orm.query(`select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code `+defaultQuery+` `+whereQuery+` group by invoice.id order by invoice.invoice_due_date desc`+` limit `+limit+` offset `+offset, { type: orm.QueryTypes.SELECT});

            invoices.forEach(result => {
                let milisecond_transact_date = new Date(String(result.transact_date));
                let milisecond_sales_date = new Date(String(result.invoice_sales_date));
                let milisecond_due_date = new Date(String(result.invoice_due_date));

                result.transact_date = milisecond_transact_date.getTime();
                result.invoice_sales_date = milisecond_sales_date.getTime();
                result.invoice_due_date = milisecond_due_date.getTime();
                result.distributor = JSON.parse(`{"dt_code": "${result.dt_code}", "name": "${result.name}"}`);

                result.is_paid = (result.is_paid == 1)
                result.is_refunded = (result.is_refunded == 1)
                result.has_refund = (result.has_refund == 1)
            });

            let response = await orm.query(`select count(le_code) as total_items from (select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code `+defaultQuery+` `+whereQuery+` group by invoice.id order by invoice.invoice_due_date desc) as DataCountPaidAndUnPaid`, { type: orm.QueryTypes.SELECT});

            let total_unpaid_items = await orm.query(`select count(*) as total_unpaid_items from (select count(invoice.invoice_id) as total_unpaid_items from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code `+defaultQueryisUnPaid+' '+whereQuery+` group by invoice.id) as T`, { type: orm.QueryTypes.SELECT});

            return {list:invoices, total:response[0].total_items, total_unpaid:total_unpaid_items[0].total_unpaid_items};
        }

    } else if(platform == 'ios') {

    } else {

    }
}

function get_total_unpaid_amount({le_code=null, join_date=null}={}) {
    return orm.query(`select SUM(cash_memo_balance_amount) as unpaid_total from invoice where invoice.hide_status = 0 and cash_memo_balance_amount >= 0 and invoice.le_code in (${le_code}) and invoice_due_date >= '${join_date}'`, { type: orm.QueryTypes.SELECT});
}

async function check_process_invoice({dt_code=null}={}) {
    let result = await orm.query(`select count(tasks.id) as total_tasks from tasks where tasks.dt_code in (${dt_code}) and tasks.status = 0`, { type: orm.QueryTypes.SELECT});

    if(result[0].total_tasks > 0) {
        throw new CustomError(400, 'Proses invoice sedang berjalan, silahkan tunggu hingga selesai');
    } else {
        return;
    }
}

async function get_task_list({dt_code=null, email=null, page=1, item_per_page=15, sorting=null, sortingBy=null, usertype=null}={}) {
    // normalize startPage to 1 if less than 1.
    page = (page < 1 ) ? 1 : page;

    const limit = item_per_page;
    const offset = ( page - 1 ) * item_per_page;

    let whereDTCode = '';
    let orderByColumnQuery = '';
    let orderByAscOrDescQuery = '';

    if(dt_code) {
        whereDTCode = `where tasks.dt_code in (${dt_code})`;
    }
    if(sorting == null || sorting == '')
    {
        orderByAscOrDescQuery = 'desc';
    }
    else
    {
        orderByAscOrDescQuery = sorting;
    }
    if(sortingBy == null || sortingBy == '')
    {
        orderByColumnQuery = 'order by tasks.createdAt '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'sales_date')
    {
        orderByColumnQuery = 'order by tasks.createdAt '+orderByAscOrDescQuery;
    }

    if(usertype == 1 || usertype == 3) {
        let tasks = await orm.query('select tasks.user_id, tasks.id, tasks.original_name, tasks.file_name, tasks.status, tasks.total_row, tasks.processed_row, tasks.updated_row, tasks.action, tasks.createdAt, tasks.updatedAt, tasks.total_upload from tasks '+whereDTCode+' '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT});

        tasks.forEach(result => {
            let milisecond_created_at = new Date(String(result.createdAt));
            let milisecond_updated_at = new Date(String(result.updatedAt));
            result.createdAt = milisecond_created_at.getFullYear()+"-"+("0" + (milisecond_created_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_created_at.getDate()).slice(-2)+" "+("0" + milisecond_created_at.getHours()).slice(-2)+":"+("0" + milisecond_created_at.getMinutes()).slice(-2)+":"+("0" + milisecond_created_at.getSeconds()).slice(-2);
            result.updatedAt = milisecond_updated_at.getFullYear()+"-"+("0" + (milisecond_updated_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_updated_at.getDate()).slice(-2)+" "+("0" + milisecond_updated_at.getHours()).slice(-2)+":"+("0" + milisecond_updated_at.getMinutes()).slice(-2)+":"+("0" + milisecond_updated_at.getSeconds()).slice(-2);
        });

        let total_tasks = await orm.query('select count(tasks.id) as total_tasks from tasks '+whereDTCode, { type: orm.QueryTypes.SELECT});

        return {list:tasks, total:total_tasks[0].total_tasks};
    } else {
        if(dt_code ===  '"null"') {
            dt_code = '';
        }

        if(dt_code) {
            let tasks = await orm.query('select tasks.user_id, tasks.id, tasks.original_name, tasks.file_name, tasks.status, tasks.total_row, tasks.processed_row, tasks.updated_row, tasks.action, tasks.createdAt, tasks.updatedAt, tasks.total_upload from tasks '+whereDTCode+' '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT});

            tasks.forEach(result => {
                let milisecond_created_at = new Date(String(result.createdAt));
                let milisecond_updated_at = new Date(String(result.updatedAt));
                result.createdAt = milisecond_created_at.getFullYear()+"-"+("0" + (milisecond_created_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_created_at.getDate()).slice(-2)+" "+("0" + milisecond_created_at.getHours()).slice(-2)+":"+("0" + milisecond_created_at.getMinutes()).slice(-2)+":"+("0" + milisecond_created_at.getSeconds()).slice(-2);
                result.updatedAt = milisecond_updated_at.getFullYear()+"-"+("0" + (milisecond_updated_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_updated_at.getDate()).slice(-2)+" "+("0" + milisecond_updated_at.getHours()).slice(-2)+":"+("0" + milisecond_updated_at.getMinutes()).slice(-2)+":"+("0" + milisecond_updated_at.getSeconds()).slice(-2);
            });

            let total_tasks = await orm.query('select count(tasks.id) as total_tasks from tasks '+whereDTCode, { type: orm.QueryTypes.SELECT});

            return {list:tasks, total:total_tasks[0].total_tasks};
        } else if(email) {
            let tasks = await orm.query('select tasks.user_id, tasks.id, tasks.original_name, tasks.file_name, tasks.status, tasks.total_row, tasks.processed_row, tasks.updated_row, tasks.action, tasks.createdAt, tasks.updatedAt, tasks.total_upload from tasks where new_users.email = "'+email+'" '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT});

            tasks.forEach(result => {
                let milisecond_created_at = new Date(String(result.createdAt));
                let milisecond_updated_at = new Date(String(result.updatedAt));

                result.createdAt = milisecond_created_at.getFullYear()+"-"+("0" + (milisecond_created_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_created_at.getDate()).slice(-2)+" "+("0" + milisecond_created_at.getHours()).slice(-2)+":"+("0" + milisecond_created_at.getMinutes()).slice(-2)+":"+("0" + milisecond_created_at.getSeconds()).slice(-2);

                result.updatedAt = milisecond_updated_at.getFullYear()+"-"+("0" + (milisecond_updated_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_updated_at.getDate()).slice(-2)+" "+("0" + milisecond_updated_at.getHours()).slice(-2)+":"+("0" + milisecond_updated_at.getMinutes()).slice(-2)+":"+("0" + milisecond_updated_at.getSeconds()).slice(-2);
            });

            let total_tasks = await orm.query('select count(tasks.id) as total_tasks from tasks where new_users.email = "'+email+'" ', { type: orm.QueryTypes.SELECT});

            return {list:tasks, total:total_tasks[0].total_tasks};
        } else {

            let tasks = await orm.query('select tasks.user_id, tasks.id, tasks.original_name, tasks.file_name, tasks.status, tasks.total_row, tasks.processed_row, tasks.updated_row, tasks.action, tasks.createdAt, tasks.updatedAt, tasks.total_upload from tasks '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT});

            tasks.forEach(result => {
                let milisecond_created_at = new Date(String(result.createdAt));
                let milisecond_updated_at = new Date(String(result.updatedAt));

                result.createdAt = milisecond_created_at.getFullYear()+"-"+("0" + (milisecond_created_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_created_at.getDate()).slice(-2)+" "+("0" + milisecond_created_at.getHours()).slice(-2)+":"+("0" + milisecond_created_at.getMinutes()).slice(-2)+":"+("0" + milisecond_created_at.getSeconds()).slice(-2);

                result.updatedAt = milisecond_updated_at.getFullYear()+"-"+("0" + (milisecond_updated_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_updated_at.getDate()).slice(-2)+" "+("0" + milisecond_updated_at.getHours()).slice(-2)+":"+("0" + milisecond_updated_at.getMinutes()).slice(-2)+":"+("0" + milisecond_updated_at.getSeconds()).slice(-2);
            });

            let total_tasks = await orm.query('select count(tasks.id) as total_tasks from tasks', { type: orm.QueryTypes.SELECT});

            return {list:tasks, total:total_tasks[0].total_tasks};
        }
    }
}

async function enhanceGetDataFromExcelDump({filename=null}={}) {
    //select count(*) as total_excel_dump from (select * from excel_dump group by dt_code, invoice_id) as invoice
    let result = await orm.query("select count(distinct id) as total_rows from excel_dump where file_name = '"+filename+"' ", { type: orm.QueryTypes.SELECT });

    return result;
}

async function insertInvoiceFromExcelDump({filename=null}={})
{
    let d = new Date();
    let rand = RandomString.generate({length: 12, charset: 'numeric'});
    let tmp_filename = '/opt/tmp/query_result/' + d.getTime() + rand + '.csv';

    await orm.query(`
        select a.file_name, a.dt_code, a.le_code, a.outlet_code, a.cash_memo_total_amount, a.cash_memo_balance_amount, a.cashmemo_type, a.invoice_id, a.invoice_sales_date, a.invoice_due_date, SUBSTRING_INDEX(a.invoice_payment_status, "/", 1) as invoice_payment_status_paid, SUBSTRING_INDEX(a.invoice_payment_status, "/", -1) as invoice_payment_status_unpaid, 0 as status
        from excel_dump as a
        where REPLACE(CONCAT(a.dt_code, a.invoice_id),' ', '') NOT IN (select REPLACE(CONCAT(b.dt_code, b.invoice_id),' ', '') from invoice as b) AND a.file_name = "${filename}"
        GROUP BY a.dt_code, a.invoice_id, a.file_name, a.dt_code, a.le_code, a.outlet_code, a.cash_memo_total_amount, a.cash_memo_balance_amount, a.cashmemo_type, a.invoice_id, a.invoice_sales_date, a.invoice_due_date, invoice_payment_status_paid, invoice_payment_status_unpaid, status
        INTO OUTFILE "${tmp_filename}"
        FIELDS TERMINATED BY ','
        ENCLOSED BY '"'
        LINES TERMINATED BY '\n'
    `, { type: orm.QueryTypes.RAW });

    await orm.query(`
        LOAD DATA INFILE "${tmp_filename}"
        INTO TABLE invoice
        FIELDS TERMINATED BY ','
        ENCLOSED BY '"'
        LINES TERMINATED BY '\n'
        (@col1, @col2, @col3, @col4, @col5, @col6, @col7, @col8, @col9, @col10, @col11, @col12, @col13)
        SET file_name=@col1, dt_code=@col2, le_code=@col3, outlet_code=@col4, cash_memo_total_amount=@col5, cash_memo_balance_amount=@col6, cashmemo_type=@col7, invoice_id=@col8, invoice_sales_date=@col9, invoice_due_date=@col10, invoice_payment_status_paid=@col11, invoice_payment_status_unpaid=@col12, status=@col13
    `, { type: orm.QueryTypes.RAW });

    return 'Invoice done';
}

async function insertProductFromExcelDump({filename=null}={})
{
    let d = new Date();
    let rand = RandomString.generate({length: 12, charset: 'numeric'});
    let tmp_filename = '/opt/tmp/query_result/' + d.getTime() + rand + '.csv';

    await orm.query(`
        select a.file_name, a.dt_code, a.invoice_id, a.product, a.product_name, a.quantity, a.product_price_cs, a.product_price_dz, a.product_price_pc, CURRENT_TIMESTAMP() as createdAt, CURRENT_TIMESTAMP() as updatedAt from excel_dump as a

        where REPLACE(CONCAT(a.dt_code, a.invoice_id),' ', '') NOT IN (select REPLACE(CONCAT(b.dt_code, b.invoice_id),' ', '') from products as b)
        AND a.file_name = '${filename}'
        INTO OUTFILE "${tmp_filename}"
        FIELDS TERMINATED BY ','
        ENCLOSED BY '"'
        LINES TERMINATED BY '\n'
    `, { type: orm.QueryTypes.RAW });

    await orm.query(`
        LOAD DATA INFILE "${tmp_filename}"
        INTO TABLE products
        FIELDS TERMINATED BY ','
        ENCLOSED BY '"'
        LINES TERMINATED BY '\n'
        (@col1, @col2, @col3, @col4, @col5, @col6, @col7, @col8, @col9, @col10, @col11)
        SET file_name=@col1, dt_code=@col2, invoice_id=@col3, product=@col4, product_name=@col5, quantity=@col6, product_price_cs=@col7, product_price_dz=@col8, product_price_pc=@col9, createdAt=@col10, updatedAt=@col11
    `, { type: orm.QueryTypes.RAW });

    return 'Product done';
}

async function enhanceCheckExcelDumpToInvoice({filename=null}={})
{
    // let result = await orm.query(`
    //     select *,

    //     case
    //         when ( Q.cash_memo_balance_amount > ed_balance_amount and Q.cash_memo_balance_amount is not null)
    //             then 2
    //         when ( Q.cash_memo_balance_amount <= ed_balance_amount and Q.cash_memo_balance_amount is not null) then 1
    //         else 0
    //     end as result

    //     from
    //     (
    //         select

    //         (select cash_memo_balance_amount from invoice as sq1 where sq1.dt_code = a.dt_code and sq1.invoice_id = a.invoice_id group by sq1.dt_code, sq1.invoice_id, cash_memo_balance_amount limit 1 ) as cash_memo_balance_amount,
    //         a.invoice_id as ed_invoice_id,
    //         a.dt_code as ed_dt_code,
    //         a.cash_memo_balance_amount as ed_balance_amount

    //         from excel_dump as a
    //         where REPLACE(CONCAT(a.dt_code, a.invoice_id),' ', '') IN (select REPLACE(CONCAT(b.dt_code, b.invoice_id),' ', '') from invoice as b)
    //         AND a.file_name = '`+filename+`'
    //         group by a.dt_code, a.invoice_id, cash_memo_balance_amount, a.cash_memo_balance_amount
    //     ) as Q
    // `, { type: orm.QueryTypes.SELECT });

    let result = await orm.query(`
        SELECT t1.invoice_id as ed_invoice_id, t1.dt_code as ed_dt_code, t1.cash_memo_balance_amount as ed_balance_amount, t2.cash_memo_balance_amount as cash_memo_balance_amount, t1.invoice_payment_status as ed_invoice_payment_status, case when ( t2.cash_memo_balance_amount > t1.cash_memo_balance_amount and t2.cash_memo_balance_amount is not null) then 2 when ( t2.cash_memo_balance_amount <= t1.cash_memo_balance_amount and t2.cash_memo_balance_amount is not null) then 1 else 0 end as result FROM excel_dump t1, invoice t2 WHERE t1.dt_code = t2.dt_code AND  t1.invoice_id = t2.invoice_id AND t1.file_name = "${filename}" GROUP BY t1.dt_code, t1.invoice_id, t1.cash_memo_balance_amount, t1.invoice_payment_status, t2.cash_memo_balance_amount
    `, { type: orm.QueryTypes.SELECT });

    return result;
}

async function clearUnregisteredUserInvoice()
{
    orm.query(`CALL clearUnregisteredUserInvoice()`, { type: orm.QueryTypes.RAW });

    return 'Product done';
}

function get_one_invoice_pembiayaan({le_code=null, dt_code=null,invoice_id=null}={}) {
    return Invoice.findOne({
       where: {dt_code: dt_code,le_code: le_code,invoice_id: invoice_id },
    });
}

function updatePaymentPembiayaan({le_code=null, dt_code=null,invoice_id=null,amount=null,paid_by=null}={}) {
    return Invoice.update(
    {
      cash_memo_balance_amount : amount,
      paid_by:paid_by
    }, {where: {dt_code: dt_code,le_code: le_code,invoice_id: invoice_id}});
}

function get_dtcode({le_code=null}={}){
    try{
        return orm.query(`SELECT DISTINCT le_code, dt_code FROM invoice WHERE le_code = '${le_code}'`, { type: orm.QueryTypes.SELECT })
    }catch(err){
        console.log(err)
    }
}

let obj = {}
obj.get_invoices = get_invoices
obj.get_one_invoice = get_one_invoice
obj.check_process_invoice = check_process_invoice
obj.get_retur = get_retur
obj.get_total_unpaid_amount = get_total_unpaid_amount
obj.get_task_list = get_task_list
obj.enhanceGetDataFromExcelDump = enhanceGetDataFromExcelDump
obj.insertInvoiceFromExcelDump = insertInvoiceFromExcelDump
obj.insertProductFromExcelDump = insertProductFromExcelDump
obj.enhanceCheckExcelDumpToInvoice = enhanceCheckExcelDumpToInvoice
obj.get_one_invoice_pembiayaan = get_one_invoice_pembiayaan
obj.updatePaymentPembiayaan = updatePaymentPembiayaan
obj.get_dtcode = get_dtcode
obj.clearUnregisteredUserInvoice = clearUnregisteredUserInvoice


export default obj

