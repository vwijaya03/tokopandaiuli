import RandomString from 'randomstring'

import Invoice from '~/orm/invoice'
import Potongan from '~/orm/potongan'
import TaskPotongan from '~/orm/taskpotongan'

import { orm } from '~/services/mariadb'
import { Op } from '~/utils/helper'

import CustomError from '~/response/error/CustomError'

async function uploadPotongan({records=null, filename=null, originalname=null, task_id=null}={}) {
	for (let j = 0; j < records.length; j++) {
		let result = await Potongan.findOne({where: {le_code: records[j]["le_code"], dt_code: records[j]['dt_code'], trans_invoice_id: records[j]['trans_invoice_id'], amount_potongan: records[j]['amount_potongan']} });

		if(result == null) {
          	await Potongan.create({
				dt_code: records[j]['dt_code'],
				file_name: originalname,
				le_code: records[j]['le_code'],
				trans_invoice_id: records[j]['trans_invoice_id'],
				amount_potongan: records[j]['amount_potongan'],
				remark_potongan: records[j]['remark_potongan'],
				task_id :task_id
			});
		}
	}
}

async function inputTaskPotongan({task_id=null}={}) {
	let data = task_id;
	let result_count_total_potongan = await Potongan.count({where: {task_id: task_id} });
	
	TaskPotongan.update({status: 1, total_upload: result_count_total_potongan }, {where: { id: data }} );
}

async function updatePembatalanPotongan({records=null,filename=null,originalname=null,task_id=null}={}) {
	for (let j = 0; j < records.length; j++) {
		await Potongan.destroy({where: {le_code: records[j]['le_code'], dt_code: records[j]['dt_code'], trans_invoice_id: records[j]['trans_invoice_id'], status: 0}});	
	}
}

async function getAllTaskPotongan( {dt_code = null, email = null, page = 1, item_per_page = 15, sorting = null, sortingBy = null, usertype = null} = {}) {
    // normalize page to 1 if less than 1.
    page = (page < 1) ? 1 : page;

    const limit = item_per_page;
    const offset = (page - 1) * item_per_page;

    let orderByColumnQuery = '';
    let orderByAscOrDescQuery = '';

    if (sorting == null) {
        orderByAscOrDescQuery = 'desc';
    } else {
        orderByAscOrDescQuery = sorting;
    }
    if (sortingBy == null) {
        orderByColumnQuery = 'order by potongan_tasks.createdAt ' + orderByAscOrDescQuery;
    } else if (sortingBy == 'sales_date') {
        orderByColumnQuery = 'order by potongan_tasks.createdAt ' + orderByAscOrDescQuery;
    }

    if (usertype == 1 || usertype == 3) {
        let tasks = await orm.query('select potongan_tasks.user_id, potongan_tasks.id, potongan_tasks.original_name, potongan_tasks.file_name, potongan_tasks.status, potongan_tasks.total_row, potongan_tasks.createdAt, potongan_tasks.updatedAt, potongan_tasks.total_upload from potongan_tasks where potongan_tasks.dt_code in (' + dt_code + ') ' + orderByColumnQuery + ' limit ' + limit + ' offset ' + offset, {type: orm.QueryTypes.SELECT});

        await tasks.forEach(result => {
            let milisecond_created_at = new Date(String(result.createdAt));
            let milisecond_updated_at = new Date(String(result.updatedAt));
            result.createdAt = milisecond_created_at.getFullYear() + "-" + ("0" + (milisecond_created_at.getMonth() + 1)).slice(-2) + "-" + ("0" + milisecond_created_at.getDate()).slice(-2) + " " + ("0" + milisecond_created_at.getHours()).slice(-2) + ":" + ("0" + milisecond_created_at.getMinutes()).slice(-2) + ":" + ("0" + milisecond_created_at.getSeconds()).slice(-2);
            result.updatedAt = milisecond_updated_at.getFullYear() + "-" + ("0" + (milisecond_updated_at.getMonth() + 1)).slice(-2) + "-" + ("0" + milisecond_updated_at.getDate()).slice(-2) + " " + ("0" + milisecond_updated_at.getHours()).slice(-2) + ":" + ("0" + milisecond_updated_at.getMinutes()).slice(-2) + ":" + ("0" + milisecond_updated_at.getSeconds()).slice(-2);
        });

        let total_tasks = await orm.query('select count(potongan_tasks.id) as total_tasks from potongan_tasks where potongan_tasks.dt_code in (' + dt_code + ') ', {type: orm.QueryTypes.SELECT});

        return {list: tasks, total: total_tasks[0].total_tasks};
    } else {
        if(dt_code ===  '"null"') {
            dt_code = '';
        }

        if (dt_code) {
            let tasks = await orm.query('select potongan_tasks.user_id, potongan_tasks.id, potongan_tasks.original_name, potongan_tasks.file_name, potongan_tasks.status, potongan_tasks.total_row,  potongan_tasks.createdAt, potongan_tasks.updatedAt, potongan_tasks.total_upload from potongan_tasks where potongan_tasks.dt_code in (' + dt_code + ') ' + orderByColumnQuery + ' limit ' + limit + ' offset ' + offset, {type: orm.QueryTypes.SELECT});

            await tasks.forEach(result => {
                let milisecond_created_at = new Date(String(result.createdAt));
                let milisecond_updated_at = new Date(String(result.updatedAt));
                result.createdAt = milisecond_created_at.getFullYear() + "-" + ("0" + (milisecond_created_at.getMonth() + 1)).slice(-2) + "-" + ("0" + milisecond_created_at.getDate()).slice(-2) + " " + ("0" + milisecond_created_at.getHours()).slice(-2) + ":" + ("0" + milisecond_created_at.getMinutes()).slice(-2) + ":" + ("0" + milisecond_created_at.getSeconds()).slice(-2);
                result.updatedAt = milisecond_updated_at.getFullYear() + "-" + ("0" + (milisecond_updated_at.getMonth() + 1)).slice(-2) + "-" + ("0" + milisecond_updated_at.getDate()).slice(-2) + " " + ("0" + milisecond_updated_at.getHours()).slice(-2) + ":" + ("0" + milisecond_updated_at.getMinutes()).slice(-2) + ":" + ("0" + milisecond_updated_at.getSeconds()).slice(-2);
            });

            let total_tasks = await orm.query('select count(potongan_tasks.id) as total_tasks from potongan_tasks where potongan_tasks.dt_code in (' + dt_code + ') ', {type: orm.QueryTypes.SELECT});

            return {list: tasks, total: total_tasks[0].total_tasks};
        } else if (email) {
            let tasks = await orm.query('select potongan_tasks.user_id, potongan_tasks.id, potongan_tasks.original_name, potongan_tasks.file_name, potongan_tasks.status, potongan_tasks.total_row,  potongan_tasks.createdAt, potongan_tasks.updatedAt, potongan_tasks.total_upload from potongan_tasks where users.email = "' + email + '" ' + orderByColumnQuery + ' limit ' + limit + ' offset ' + offset, {type: orm.QueryTypes.SELECT});

            await tasks.forEach(result => {
                let milisecond_created_at = new Date(String(result.createdAt));
                let milisecond_updated_at = new Date(String(result.updatedAt));

                result.createdAt = milisecond_created_at.getFullYear() + "-" + ("0" + (milisecond_created_at.getMonth() + 1)).slice(-2) + "-" + ("0" + milisecond_created_at.getDate()).slice(-2) + " " + ("0" + milisecond_created_at.getHours()).slice(-2) + ":" + ("0" + milisecond_created_at.getMinutes()).slice(-2) + ":" + ("0" + milisecond_created_at.getSeconds()).slice(-2);

                result.updatedAt = milisecond_updated_at.getFullYear() + "-" + ("0" + (milisecond_updated_at.getMonth() + 1)).slice(-2) + "-" + ("0" + milisecond_updated_at.getDate()).slice(-2) + " " + ("0" + milisecond_updated_at.getHours()).slice(-2) + ":" + ("0" + milisecond_updated_at.getMinutes()).slice(-2) + ":" + ("0" + milisecond_updated_at.getSeconds()).slice(-2);
            });

            let total_tasks = await orm.query('select count(potongan_tasks.id) as total_tasks from potongan_tasks where users.email = "' + email + '" ', {type: orm.QueryTypes.SELECT});

            return {list: tasks, total: total_tasks[0].total_tasks};
        } else {
            let tasks = await orm.query('select potongan_tasks.user_id, potongan_tasks.id, potongan_tasks.original_name, potongan_tasks.file_name, potongan_tasks.status, potongan_tasks.total_row,  potongan_tasks.createdAt, potongan_tasks.updatedAt, potongan_tasks.total_upload from potongan_tasks ' + orderByColumnQuery + ' limit ' + limit + ' offset ' + offset, {type: orm.QueryTypes.SELECT});

            await tasks.forEach(result => {
                let milisecond_created_at = new Date(String(result.createdAt));
                let milisecond_updated_at = new Date(String(result.updatedAt));

                result.createdAt = milisecond_created_at.getFullYear() + "-" + ("0" + (milisecond_created_at.getMonth() + 1)).slice(-2) + "-" + ("0" + milisecond_created_at.getDate()).slice(-2) + " " + ("0" + milisecond_created_at.getHours()).slice(-2) + ":" + ("0" + milisecond_created_at.getMinutes()).slice(-2) + ":" + ("0" + milisecond_created_at.getSeconds()).slice(-2);

                result.updatedAt = milisecond_updated_at.getFullYear() + "-" + ("0" + (milisecond_updated_at.getMonth() + 1)).slice(-2) + "-" + ("0" + milisecond_updated_at.getDate()).slice(-2) + " " + ("0" + milisecond_updated_at.getHours()).slice(-2) + ":" + ("0" + milisecond_updated_at.getMinutes()).slice(-2) + ":" + ("0" + milisecond_updated_at.getSeconds()).slice(-2);
            });

            let total_tasks = await orm.query('select count(potongan_tasks.id) as total_tasks from potongan_tasks', {type: orm.QueryTypes.SELECT});

            return {list: tasks, total: total_tasks[0].total_tasks};
        }
	}
}

async function getAllTaskPembatalanPotongan({dt_code=null, email=null, page=1, item_per_page=15, sorting=null, sortingBy=null, usertype=null}={})
{
    // normalize page to 1 if less than 1.
    page = (page < 1 ) ? 1 : page;

    const limit = item_per_page;
    const offset = ( page - 1 ) * item_per_page;

    let orderByColumnQuery = '';
    let orderByAscOrDescQuery = '';

    if(sorting == null)
    {
        orderByAscOrDescQuery = 'desc';
    }
    else
    {
        orderByAscOrDescQuery = sorting;
    }
    if(sortingBy == null)
    {
        orderByColumnQuery = 'order by potongan_batal_tasks.createdAt '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'sales_date')
    {
        orderByColumnQuery = 'order by potongan_batal_tasks.createdAt '+orderByAscOrDescQuery;
    }

    if(usertype == 1 || usertype == 3)
    {
        let tasks = await orm.query('select potongan_batal_tasks.user_id, potongan_batal_tasks.id, potongan_batal_tasks.original_name, potongan_batal_tasks.file_name, potongan_batal_tasks.status, potongan_batal_tasks.total_row, potongan_batal_tasks.createdAt, potongan_batal_tasks.updatedAt, potongan_batal_tasks.total_upload from potongan_batal_tasks where potongan_batal_tasks.dt_code in (' + dt_code + ') '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT});

        await tasks.forEach(result => {
            let milisecond_created_at = new Date(String(result.createdAt));
            let milisecond_updated_at = new Date(String(result.updatedAt));
            result.createdAt = milisecond_created_at.getFullYear()+"-"+("0" + (milisecond_created_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_created_at.getDate()).slice(-2)+" "+("0" + milisecond_created_at.getHours()).slice(-2)+":"+("0" + milisecond_created_at.getMinutes()).slice(-2)+":"+("0" + milisecond_created_at.getSeconds()).slice(-2);
            result.updatedAt = milisecond_updated_at.getFullYear()+"-"+("0" + (milisecond_updated_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_updated_at.getDate()).slice(-2)+" "+("0" + milisecond_updated_at.getHours()).slice(-2)+":"+("0" + milisecond_updated_at.getMinutes()).slice(-2)+":"+("0" + milisecond_updated_at.getSeconds()).slice(-2);
        });

        let total_tasks = await orm.query('select count(potongan_batal_tasks.id) as total_tasks from potongan_batal_tasks where potongan_batal_tasks.dt_code in (' + dt_code + ') ', { type: orm.QueryTypes.SELECT});

        return {list:tasks, total:total_tasks[0].total_tasks};
    } else {
        if(dt_code ===  '"null"') {
            dt_code = '';
        }

        if(dt_code)
        {
            let tasks = await orm.query('select potongan_batal_tasks.user_id, potongan_batal_tasks.id, potongan_batal_tasks.original_name, potongan_batal_tasks.file_name, potongan_batal_tasks.status, potongan_batal_tasks.total_row,  potongan_batal_tasks.createdAt, potongan_batal_tasks.updatedAt, potongan_batal_tasks.total_upload from potongan_batal_tasks where potongan_batal_tasks.dt_code in (' + dt_code + ') '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT});

            await tasks.forEach(result => {
                let milisecond_created_at = new Date(String(result.createdAt));
                let milisecond_updated_at = new Date(String(result.updatedAt));

                result.createdAt = milisecond_created_at.getFullYear()+"-"+("0" + (milisecond_created_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_created_at.getDate()).slice(-2)+" "+("0" + milisecond_created_at.getHours()).slice(-2)+":"+("0" + milisecond_created_at.getMinutes()).slice(-2)+":"+("0" + milisecond_created_at.getSeconds()).slice(-2);

                result.updatedAt = milisecond_updated_at.getFullYear()+"-"+("0" + (milisecond_updated_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_updated_at.getDate()).slice(-2)+" "+("0" + milisecond_updated_at.getHours()).slice(-2)+":"+("0" + milisecond_updated_at.getMinutes()).slice(-2)+":"+("0" + milisecond_updated_at.getSeconds()).slice(-2);
            });

            let total_tasks = await orm.query('select count(potongan_batal_tasks.id) as total_tasks from potongan_batal_tasks where potongan_batal_tasks.dt_code in (' + dt_code + ') ', { type: orm.QueryTypes.SELECT});

            return {list:tasks, total:total_tasks[0].total_tasks};
        }
        else if(email)
        {
            let tasks = await orm.query('select potongan_batal_tasks.user_id, potongan_batal_tasks.id, potongan_batal_tasks.original_name, potongan_batal_tasks.file_name, potongan_batal_tasks.status, potongan_batal_tasks.total_row,  potongan_batal_tasks.createdAt, potongan_batal_tasks.updatedAt, potongan_batal_tasks.total_upload from potongan_batal_tasks where users.email = "'+email+'" '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT});

            await tasks.forEach(result => {
                let milisecond_created_at = new Date(String(result.createdAt));
                let milisecond_updated_at = new Date(String(result.updatedAt));

                result.createdAt = milisecond_created_at.getFullYear()+"-"+("0" + (milisecond_created_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_created_at.getDate()).slice(-2)+" "+("0" + milisecond_created_at.getHours()).slice(-2)+":"+("0" + milisecond_created_at.getMinutes()).slice(-2)+":"+("0" + milisecond_created_at.getSeconds()).slice(-2);

                result.updatedAt = milisecond_updated_at.getFullYear()+"-"+("0" + (milisecond_updated_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_updated_at.getDate()).slice(-2)+" "+("0" + milisecond_updated_at.getHours()).slice(-2)+":"+("0" + milisecond_updated_at.getMinutes()).slice(-2)+":"+("0" + milisecond_updated_at.getSeconds()).slice(-2);
            });

            let total_tasks = await orm.query('select count(potongan_batal_tasks.id) as total_tasks from potongan_batal_tasks where users.email = "'+email+'" ', { type: orm.QueryTypes.SELECT});

            return {list:tasks, total:total_tasks[0].total_tasks};
        }
        else
        {
            let tasks = await orm.query('select potongan_batal_tasks.user_id, potongan_batal_tasks.id, potongan_batal_tasks.original_name, potongan_batal_tasks.file_name, potongan_batal_tasks.status, potongan_batal_tasks.total_row,  potongan_batal_tasks.createdAt, potongan_batal_tasks.updatedAt, potongan_batal_tasks.total_upload from potongan_batal_tasks '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT});

            await tasks.forEach(result => {
                let milisecond_created_at = new Date(String(result.createdAt));
                let milisecond_updated_at = new Date(String(result.updatedAt));

                result.createdAt = milisecond_created_at.getFullYear()+"-"+("0" + (milisecond_created_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_created_at.getDate()).slice(-2)+" "+("0" + milisecond_created_at.getHours()).slice(-2)+":"+("0" + milisecond_created_at.getMinutes()).slice(-2)+":"+("0" + milisecond_created_at.getSeconds()).slice(-2);

                result.updatedAt = milisecond_updated_at.getFullYear()+"-"+("0" + (milisecond_updated_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_updated_at.getDate()).slice(-2)+" "+("0" + milisecond_updated_at.getHours()).slice(-2)+":"+("0" + milisecond_updated_at.getMinutes()).slice(-2)+":"+("0" + milisecond_updated_at.getSeconds()).slice(-2);
            });

            let total_tasks = await orm.query('select count(potongan_batal_tasks.id) as total_tasks from potongan_batal_tasks', { type: orm.QueryTypes.SELECT});

            return {list:tasks, total:total_tasks[0].total_tasks};
        }
    }
}
function invoice_check({dt_code=null,le_code=null,trans_invoice_id=null}){
    return Potongan.count({
		where: {
            dt_code: dt_code,
            le_code:le_code,
            trans_invoice_id:trans_invoice_id
		}
	});
}

function create_retur({dt_code=null,le_code=null,trans_invoice_id=null,amount_potongan=null,date_potongan=null,remark_potongan=null,remark_app=null}){
    return Potongan.create({ 
        dt_code: dt_code,
        le_code: le_code,
        trans_invoice_id: trans_invoice_id,
        amount_potongan: amount_potongan,
        date_potongan: date_potongan,
        remark_potongan: remark_potongan,
	remark_app : remark_app
    });
}

function update_retur({dt_code=null,le_code=null,trans_invoice_id=null,amount_potongan=null,date_potongan=null}){
    return Potongan.update({ 
        amount_potongan: amount_potongan,
        date_potongan:date_potongan
    },{where:{
        dt_code:dt_code,
        le_code:le_code,
        trans_invoice_id:trans_invoice_id
    }});
}
function get_all_potongan({role_type=null,dt_code=null,remark_potongan=null,startPage=1,itemsPerPage=15}={}){
    startPage = (startPage < 1 ) ? 1 : startPage;
    const limit = itemsPerPage;
    const offset = ( startPage - 1 ) * itemsPerPage;
    console.log(role_type,'uu',limit,'aa',offset)

    try{
        if(role_type === 1){
        return orm.query(`SELECT dt_code,le_code,trans_invoice_id,amount_potongan,remark_potongan FROM unilever.potongan WHERE dt_code = '${dt_code}' AND remark_potongan = '${remark_potongan}' LIMIT ${limit} OFFSET ${offset}`, { type: orm.QueryTypes.SELECT})
        .then(function(tasks){
            tasks.forEach(result => {
                result.dt_code = result.dt_code;
                result.le_code = result.le_code;
                result.invoice_id = result.trans_invoice_id;
                result.amount_potongan = result.amount_potongan;
                result.remark = result.remark_potongan;
            })
            return orm.query(`select count(*) as total from unilever.potongan where dt_code = '${dt_code}' and remark_potongan = '${remark_potongan}'`, { type: orm.QueryTypes.SELECT})
              .then(function(total_tasks){
                  return {list:tasks, total:total_tasks[0].total};
              });
        })
    }
    else if(role_type === 10){
        return orm.query(`SELECT dt_code,le_code,trans_invoice_id,amount_potongan,remark_potongan FROM unilever.potongan WHERE remark_potongan = '${remark_potongan}' LIMIT ${limit} OFFSET ${offset}`, { type: orm.QueryTypes.SELECT})
        .then(function(tasks){
            tasks.forEach(result => {
                result.dt_code = result.dt_code;
                result.le_code = result.le_code;
                result.invoice_id = result.trans_invoice_id;
                result.amount_potongan = result.amount_potongan;
                result.remark = result.remark_potongan;
            })
            return orm.query(`select count(*) as total from unilever.potongan where remark_potongan = '${remark_potongan}'`, { type: orm.QueryTypes.SELECT})
              .then(function(total_tasks){
                  return {list:tasks, total:total_tasks[0].total};
              });
        })
    }
    }catch(err){
        console.log(err)
    }
}

let obj = {}
obj.uploadPotongan = uploadPotongan
obj.inputTaskPotongan = inputTaskPotongan
obj.updatePembatalanPotongan = updatePembatalanPotongan
obj.getAllTaskPotongan = getAllTaskPotongan
obj.getAllTaskPembatalanPotongan = getAllTaskPembatalanPotongan
obj.create_retur = create_retur
obj.invoice_check = invoice_check
obj.update_retur = update_retur
obj.get_all_potongan = get_all_potongan

export default obj
