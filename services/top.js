'use strict';

import moment from 'moment'
import crypto from 'crypto'
import RandomString from 'randomstring'
import NodeMailer from 'nodemailer'

import { Op } from '~/utils/helper'
import { orm } from '~/services/mariadb'
import { Top_payment_distributor } from '~/orm/index'
import { allowed_user_status } from '~/properties'
import { Transaction } from '~/orm/index'

import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'


function insert_payment_distributor({le_code=null,dt_code=null,invoice_no=null,amount=null,total_tagihan=null,fmcg_id=null,tenor=null,principal_code=null}={}) {
    return Top_payment_distributor.create(
    {
        le_code:le_code,
        dt_code:dt_code,
        invoice_no:invoice_no,
        amount:amount,
        total_tagihan:total_tagihan,
        fmcg_id:fmcg_id,
        tenor:tenor,
        principal_code:principal_code
    });
}

function update_payment_status_distributor({invoice_no=null, payment_status=null, transaction_date=null}={}){
       return Top_payment_distributor.update(
           {
               payment_status:payment_status,
               transaction_date:transaction_date
           }, {
               where:{invoice_no:invoice_no}
           }
       )
}

function findOne({invoice_no=null}={}) {
	return Top_payment_distributor.count({
		attributes: ['invoice_no'],
		where: {
			invoice_no: invoice_no
		}
	});
}


function get_paid_data({invoice_no=null}={}){
    try{
        return orm.query('select updatedAt as paid_date_uli from top_payment_distributors where invoice_no = '+invoice_no+ ' and payment_status = 1', { type: orm.QueryTypes.SELECT})
    }catch(err){
        console.log(err)
    }
}

function get_trans_date({invoice_no=null}={}){
    try{
        return orm.query('select transaction_date as paid_date_uli from top_payment_distributors where invoice_no = '+invoice_no+ ' and payment_status = 1', { type: orm.QueryTypes.SELECT})
    }catch(err){
        console.log(err)
    }
}

function get_unpaid_invoice(){
    try{
        return orm.query('select * from top_payment_distributors where payment_status = 2 OR payment_status = 0 ', { type: orm.QueryTypes.SELECT})
    }catch(err){
        console.log(err)
    }
}

function get_one_transaction({invoice_id=null,dt_code=null}={}){
    try{
        return orm.query(`SELECT * FROM transactions where invoice_id = '${invoice_id}' and dt_code = '${dt_code}';`, { type: orm.QueryTypes.SELECT})
    }catch(err){
        console.log(err)
    }
}
function get_one_distributor({dt_code=null}={}){
    try{
        return orm.query(`SELECT * FROM distributors where dt_code = '${dt_code}';`, { type: orm.QueryTypes.SELECT})
    }catch(err){
        console.log(err)
    }
}

function get_one_distributor({dt_code=null}={}){
    try{
        return orm.query(`SELECT * FROM distributors where dt_code = '${dt_code}';`, { type: orm.QueryTypes.SELECT})
    }catch(err){
        console.log(err)
    }
}




let obj = {}
obj.get_one_transaction = get_one_transaction
obj.insert_payment_distributor =insert_payment_distributor
obj.findOne = findOne
obj.update_payment_status_distributor = update_payment_status_distributor
obj.get_paid_data = get_paid_data
obj.get_trans_date = get_trans_date
obj.get_unpaid_invoice = get_unpaid_invoice
obj.get_one_distributor = get_one_distributor


export default obj

