'use strict';

import moment from 'moment'

import { TopInvoice } from '~/orm/index'
import { TopLoanHistory } from '~/orm/index'

import { orm } from '~/services/mariadb'

import service_top from '~/services/top'



function find_invoice({dt_code=null,le_code=null,cash_memo_doc_no=null, invoice_toko=null}={}){
    return TopInvoice.count({
		attributes: ['dt_code', 'le_code', 'cash_memo_doc_no', 'invoice_toko'],
		where: {
			dt_code: dt_code,
            le_code: le_code,
            cash_memo_doc_no:cash_memo_doc_no,
            invoice_toko:invoice_toko
		}
	});
}

function find_all_invoice({dt_code=null,le_code=null,invoice_toko=null}={}){
    return TopInvoice.count({
		attributes: ['dt_code', 'le_code', 'invoice_toko'],
		where: {
			dt_code: dt_code,
            le_code: le_code,
            invoice_toko:invoice_toko
		}
	});
}

function total_amount({dt_code=null,le_code=null,invoice_toko=null}={}){
    try{
        return orm.query(`select sum(principle) as total_amount from invoice_top where dt_code=${dt_code} and le_code=${le_code} and invoice_toko=${invoice_toko}`, { type: orm.QueryTypes.SELECT})
    }catch(err){
        console.log(total_amount)
    }
}

function get_one_invoice({dt_code=null,le_code=null,cash_memo_doc_no=null, invoice_toko=null}={}){
    try{
        return orm.query('SELECT invoice_toko as id,dt_code,le_code,outlet_code,cashmemo_type,cash_memo_total_amount,total_yg_harus_dibayar,invoice_payment_status,invoice_due_date,cash_memo_doc_no FROM  invoice_top where dt_code = "'+dt_code+'" and le_code = "'+le_code+'" and cash_memo_doc_no = "'+cash_memo_doc_no+'" and invoice_toko = "'+invoice_toko+'"', { type: orm.QueryTypes.SELECT})
        .then(function(tasks){
            tasks.forEach(result => {
                 result.id= result.id;
                 result.cash_memo_doc_no =cash_memo_doc_no;
                 result.dt_code= result.dt_code;
                 result.le_code= result.le_code;
                 result.outlet_code= result.outlet_code;
                 result.cashmemo_type= result.cashmemo_type;
                 result.total_amount= result.cash_memo_total_amount;
                 result.balance_amount= result.invoice_payment_status == "PAID"?0:result.cash_memo_total_amount;
                 result.invoice_payment_status_paid= 0;
                 result.payment_status_unpaid= 0;
                 result.sales_date= moment(result.invoice_due_date).format('YYYY-MM-DD HH:mm:ss');
                 result.due_date= moment(result.invoice_due_date).format('YYYY-MM-DD HH:mm:ss');
                 result.is_paid= result.invoice_payment_status == "PAID"?true:false;
                 result.is_refunded= false;
                 result.has_refund= false;
                 let teno = result.cash_memo_doc_no.split("-");
                 result.tenor = teno[1];
                 result.tenor_ke = teno[1];
                 result.distributor= {dt_code:result.dt_code,name:""};
                 result.transaction= [];
                 result.potongan = [];
                 result.total_potongan = 0;
                 result.product = null
                 result.outlet_name= "";
                 result.fmcg_id = 13;
               
           });
               return tasks;
           });
    }
    catch(err) {
        console.log(err)
	}
}
//
//
//function get_all_invoice({dist_dt_code=null,role_type=null,user_id=null,dt_code=null, sorting=null,
//    startPage=1,itemsPerPage=15,invoice_due_date=null,invoice_payment_status=null,next_date=null,previous_date=null}={}){
//    startPage = (startPage < 1 ) ? 1 : startPage;
//    const limit = itemsPerPage;
//    const offset = ( startPage - 1 ) * itemsPerPage;
//
//    
//    var orderByAscOrDescQuery = '';
//    var adminOrderByAscOrDescQuery = '';
//    var distOrderByAscOrDescQuery = '';
//    var filterByDtCode= '';
//    var filterByPaymentStatus = '';
//    var filterByDueDate = '';
//    var filterAdmin = '';
//
//    if(role_type === 0 && dt_code !== null){
//        filterByDtCode = ' and dt_code = '+dt_code
//    }
//    if(role_type === 0 && invoice_payment_status !== null){
//        filterByPaymentStatus = ' and invoice_payment_status = "'+invoice_payment_status+'"'
//    }
//    if(role_type === 0 && sorting === null)
//    {
//        orderByAscOrDescQuery = 'asc';
//    }
//    else if(role_type === 0 && sorting !== null)
//    {
//        orderByAscOrDescQuery = sorting;
//    }
//    let orderByColumnQuery = 'order by invoice_due_date '+orderByAscOrDescQuery;
//
//
//    if(role_type === 1 && dt_code !== null){
//        filterByDtCode = ' and dt_code = '+dt_code
//    }
//    if(role_type === 1 && invoice_payment_status !== null){
//        filterByPaymentStatus = ' and invoice_payment_status = "'+invoice_payment_status+'"'
//    }
//    if(role_type === 1 && sorting === null)
//    {
//        distOrderByAscOrDescQuery = 'desc';
//    }
//    else if(role_type === 1 && sorting !== null)
//    {
//        distOrderByAscOrDescQuery = sorting;
//    }
//    let distOrderByColumnQuery = 'order by overdue '+ distOrderByAscOrDescQuery;
//
//
//    if(role_type === 10 && invoice_payment_status !== null && invoice_due_date === null){
//        filterAdmin = ' where invoice_payment_status = "'+invoice_payment_status+'"'
//    }
//    else if(role_type === 10 && invoice_payment_status === null && invoice_due_date !== null){
//        filterAdmin = ' where invoice_due_date = '+invoice_due_date
//    }
//    else if(role_type === 10 && invoice_payment_status !== null && invoice_due_date !== null){
//        filterAdmin = ' where invoice_payment_status = "'+invoice_payment_status+ '" and invoice_due_date = '+invoice_due_date
//    }
//    if(role_type === 10 && sorting === null)
//    {
//        adminOrderByAscOrDescQuery = 'desc';
//    }
//    else if(role_type === 10 && sorting !== null)
//    {
//       adminOrderByAscOrDescQuery = sorting;
//    }
//    let adminOrderByColumnQuery = 'order by overdue '+ adminOrderByAscOrDescQuery;
//    
//    
//    if(role_type === 0){
//        return  orm.query('SELECT cash_memo_doc_date,overdue,cash_memo_doc_no,invoice_toko,dt_code,le_code,outlet_code,cashmemo_type,cash_memo_total_amount,total_yg_harus_dibayar,invoice_payment_status,invoice_due_date FROM invoice_top where (user_id = '+user_id+filterByDtCode+filterByPaymentStatus+filterByDueDate+' and invoice_due_date between "'+previous_date+ '" and "'+next_date+'") or (user_id = '+user_id+' and invoice_due_date < "'+next_date+'" and invoice_payment_status = "UNPAID") '+ orderByColumnQuery+' limit '+ limit + ' offset '+offset, { type: orm.QueryTypes.SELECT})
//          .then(function(tasks){
//           tasks.forEach(result => {
//                result.id= result.invoice_toko;
//                result.dt_code= result.dt_code;
//                result.le_code= result.le_code;
//                result.cash_memo_doc_no=result.cash_memo_doc_no;
//                result.outlet_code= result.outlet_code;
//                result.cashmemo_type= result.cashmemo_type;
//                result.amount= result.cash_memo_total_amount;
//                result.balance_amount= result.invoice_payment_status == "PAID"?0:result.cash_memo_total_amount;
//                result.status = result.invoice_payment_status;
//                result.invoice_payment_status_paid= 0;
//                result.payment_status_unpaid= 0;
//                
//                result.sales_date= moment(result.invoice_due_date).format('YYYY-MM-DD HH:mm:ss');
//                result.due_date= moment(result.invoice_due_date).format('YYYY-MM-DD HH:mm:ss');
//                
//                let date = result.cash_memo_doc_date ==null?"":moment(result.cash_memo_doc_date).format('YYYY-MM-DD HH:mm:ss');
//                result.paid_date = date
//                
//                result.is_paid= result.invoice_payment_status == "PAID"?true:false;
//                result.is_refunded= false;
//                result.has_refund= false;
//                result.overdue = result.overdue;
//                result.distributor= {dt_code:result.dt_code,name:""};
//                let teno = result.cash_memo_doc_no.split("-");
//                result.tenor_ke = teno[1];
//                result.paid_by = "Pinjam Modal"
//                let total_inv = find_all_invoice({dt_code:result.dt_code,le_code:result.le_code,cash_memo_doc_no:result.cash_memo_doc_no,invoice_toko:teno[0]})
//                result.total_tenor = total_inv
//                let amount_total = total_amount({dt_code:result.dt_code,le_code:result.le_code,invoice_toko:teno[0]})
//                result.total_amountt = amount_total
//                let trans_date = service_top.get_trans_date({invoice_no:teno[0]})
//                result.paid_data_uli = trans_date == " "?moment().format('YYYY-MM-DD HH:mm:ss'):trans_date;
//                delete result.invoice_due_date;
//                delete result.invoice_toko;
//                delete result.cash_memo_total_amount;
//                delete result.cash_memo_doc_date;
//                delete result.total_yg_harus_dibayar;
//                delete result.invoice_payment_status;
//                
//              });
//              return tasks;
//
//          });
//    }
//    else if(role_type === 1){
//        return  orm.query('SELECT overdue,cash_memo_doc_no,invoice_toko,dt_code,le_code,outlet_code,cashmemo_type,cash_memo_total_amount,total_yg_harus_dibayar,invoice_payment_status,invoice_due_date FROM invoice_top where dt_code = '+dist_dt_code+filterByPaymentStatus+filterByDueDate+' '+distOrderByColumnQuery +' limit '+ limit + ' offset '+offset, { type: orm.QueryTypes.SELECT})
//          .then(function(tasks){
//           tasks.forEach(result => {
//            result.id= result.invoice_toko;
//            result.dt_code= result.dt_code;
//            result.le_code= result.le_code;
//            result.outlet_code= result.outlet_code;
//            result.cashmemo_type= result.cashmemo_type;
//            result.amount= result.cash_memo_total_amount;
//            result.balance_amount= result.invoice_payment_status == "PAID"?0:result.cash_memo_total_amount;
//            result.status = result.invoice_payment_status;
//            result.invoice_payment_status_paid= 0;
//            result.payment_status_unpaid= 0;
//            result.sales_date= moment(result.invoice_due_date).format('YYYY-MM-DD HH:mm:ss');
//            result.due_date= moment(result.invoice_due_date).format('YYYY-MM-DD HH:mm:ss');
//            
//            let date = moment(result.cash_memo_doc_date).format('YYYY-MM-DD HH:mm:ss');
//            result.paid_date = date
//            result.is_paid= result.invoice_payment_status == "PAID"?true:false;
//            result.is_refunded= false;
//            result.has_refund= false;
//            result.overdue = result.overdue;
//            result.distributor= {dt_code:result.dt_code,name:""};
//            let teno = result.cash_memo_doc_no.split("-");
//            result.tenor_ke = teno[1];
//            result.paid_by = "Pinjam Modal"
//            let total_inv = find_all_invoice({dt_code:result.dt_code,le_code:result.le_code,cash_memo_doc_no:result.cash_memo_doc_no,invoice_toko:teno[0]})
//            result.total_tenor = total_inv
//            let amount_total = total_amount({dt_code:result.dt_code,le_code:result.le_code,invoice_toko:teno[0]})
//            result.total_amountt = amount_total
//            let trans_date = service_top.get_trans_date({invoice_no:teno[0]})
//            result.paid_data_uli = trans_date == " "?moment().format('YYYY-MM-DD HH:mm:ss'):trans_date;
//            delete result.invoice_due_date;
//            delete result.invoice_toko;
//            delete result.cash_memo_total_amount;
//            delete result.cash_memo_doc_date;
//            delete result.total_yg_harus_dibayar;
//            delete result.invoice_payment_status;});
//            return tasks;});
//    }
//    else if(role_type === 10){
//        return  orm.query('SELECT overdue,cash_memo_doc_no,invoice_toko,dt_code,le_code,outlet_code,cashmemo_type,cash_memo_total_amount,total_yg_harus_dibayar,invoice_payment_status,invoice_due_date FROM  invoice_top'+filterAdmin+' '+adminOrderByColumnQuery +' limit '+ limit + ' offset '+offset, { type: orm.QueryTypes.SELECT})
//          .then(function(tasks){
//           tasks.forEach(result => {
//            result.id= result.invoice_toko;
//            result.dt_code= result.dt_code;
//            result.le_code= result.le_code;
//            result.outlet_code= result.outlet_code;
//            result.cashmemo_type= result.cashmemo_type;
//            result.amount= result.cash_memo_total_amount;
//            result.balance_amount= result.invoice_payment_status == "PAID"?0:result.cash_memo_total_amount;
//            result.status = result.invoice_payment_status;
//            result.invoice_payment_status_paid= 0;
//            result.payment_status_unpaid= 0;
//            result.sales_date= moment(result.invoice_due_date).format('YYYY-MM-DD HH:mm:ss');
//            result.due_date= moment(result.invoice_due_date).format('YYYY-MM-DD HH:mm:ss');
//            let date = moment(result.cash_memo_doc_date).format('YYYY-MM-DD HH:mm:ss');
//            result.paid_date = date
//            result.is_paid= result.invoice_payment_status == "PAID"?true:false;
//            result.is_refunded= false;
//            result.has_refund= false;
//            result.overdue = result.overdue;
//            result.distributor= {dt_code:result.dt_code,name:""};
//            let teno = result.cash_memo_doc_no.split("-");
//            result.tenor_ke = teno[1];
//            result.paid_by = "Pinjam Modal"
//            let total_inv = find_all_invoice({dt_code:result.dt_code,le_code:result.le_code,cash_memo_doc_no:result.cash_memo_doc_no,invoice_toko:teno[0]})
//            result.total_tenor = total_inv
//            let amount_total = total_amount({dt_code:result.dt_code,le_code:result.le_code,invoice_toko:teno[0]})
//            result.total_amountt = amount_total
//            let trans_date = service_top.get_trans_date({invoice_no:teno[0]})
//            result.paid_data_uli = trans_date == " "?moment().format('YYYY-MM-DD HH:mm:ss'):trans_date;
//            delete result.invoice_due_date;
//            delete result.invoice_toko;
//            delete result.cash_memo_total_amount;
//            delete result.cash_memo_doc_date;
//            delete result.total_yg_harus_dibayar;
//            delete result.invoice_payment_status;});
//            return tasks;
//          });
//    }
//}
//
//function countData({dist_dt_code=null,role_type=null,user_id=null,dt_code=null,sorting=null,
//    invoice_payment_status=null,invoice_due_date=null,startPage=1,
//    itemsPerPage=15,next_date=null,previous_date=null}={}){
//    startPage = (startPage < 1 ) ? 1 : startPage;
//
//    var orderByAscOrDescQuery = '';
//    var adminOrderByAscOrDescQuery = '';
//    var distOrderByAscOrDescQuery = '';
//    var filterByDtCode= '';
//    var filterByPaymentStatus = '';
//    var filterByDueDate = '';
//    var filterAdmin = '';
//
//    if(role_type === 0 && dt_code !== null){
//        filterByDtCode = ' and dt_code = '+dt_code
//    }
//    if(role_type === 0 && invoice_payment_status !== null){
//        filterByPaymentStatus = ' and invoice_payment_status = "'+invoice_payment_status+'"'
//    }
//    if(role_type === 0 && sorting === null)
//    {
//        orderByAscOrDescQuery = 'asc';
//    }
//    else if(role_type === 0 && sorting !== null)
//    {
//        orderByAscOrDescQuery = sorting;
//    }
//    let orderByColumnQuery = 'order by invoice_due_date '+orderByAscOrDescQuery;
//
//
//    if(role_type === 1 && dt_code !== null){
//        filterByDtCode = ' and dt_code = '+dt_code
//    }
//    if(role_type === 1 && invoice_payment_status !== null){
//        filterByPaymentStatus = ' and invoice_payment_status = "'+invoice_payment_status+'"'
//    }
//    if(role_type === 1 && sorting === null)
//    {
//        distOrderByAscOrDescQuery = 'desc';
//    }
//    else if(role_type === 1 && sorting !== null)
//    {
//        distOrderByAscOrDescQuery = sorting;
//    }
//    let distOrderByColumnQuery = 'order by overdue '+ distOrderByAscOrDescQuery;
//
//
//    if(role_type === 10 && invoice_payment_status !== null && invoice_due_date === null){
//        filterAdmin = ' where invoice_payment_status = "'+invoice_payment_status+'"'
//    }
//    else if(role_type === 10 && invoice_payment_status === null && invoice_due_date !== null){
//        filterAdmin = ' where invoice_due_date = '+invoice_due_date
//    }
//    else if(role_type === 10 && invoice_payment_status !== null && invoice_due_date !== null){
//        filterAdmin = ' where invoice_payment_status = "'+invoice_payment_status+ '" and invoice_due_date = '+invoice_due_date
//    }
//    if(role_type === 10 && sorting === null)
//    {
//        adminOrderByAscOrDescQuery = 'desc';
//    }
//    else if(role_type === 10 && sorting !== null)
//    {
//       adminOrderByAscOrDescQuery = sorting;
//    }
//    let adminOrderByColumnQuery = 'order by overdue '+ adminOrderByAscOrDescQuery;
//    
//
//    try{
//        if(role_type === 0){
//            return orm.query('SELECT count(*) as total FROM invoice_top where (user_id = '+user_id+filterByDtCode+filterByPaymentStatus+filterByDueDate+' and invoice_due_date between "'+previous_date+ '" and "'+next_date+'") or (user_id = '+user_id+' and invoice_due_date < "'+next_date+'" and invoice_payment_status = "UNPAID") '+ orderByColumnQuery,{type: orm.QueryTypes.SELECT})
//        }
//        else if(role_type === 1){
//            return orm.query('SELECT count(*) as total from invoice_top where dt_code = '+dist_dt_code+filterByPaymentStatus+filterByDueDate+' '+ distOrderByColumnQuery,{type: orm.QueryTypes.SELECT})
//        }
//        else if(role_type === 10){
//            return orm.query('SELECT count(*) as total from invoice_top'+filterAdmin+' '+adminOrderByColumnQuery,{type: orm.QueryTypes.SELECT})
//        }
//        
//    }
//    catch(err){
//        console.log(err)
//    }
//}

function insert_invoice({user_id=null,dt_code=null,le_code=null,outlet_code=null,cash_memo_doc_no=null,cash_memo_doc_date=null,
    cash_memo_total_amount=null,cash_memo_balance_amount=null,cashmemo_type=null,invoice_id=null,
    invoice_id_associated_with_retailer_id=null,invoice_sales_date=null,invoice_due_date=null,invoice_payment_status=null,
    product=null,product_name=null,quantity_pc=null,product_price_cs=null,product_pride_dz=null,product_price_pc=null,
    overdue=null,distributor_principle_code=null,invoice_toko=null,principle=null,denda_amount=null,
    total_yg_harus_dibayar=null,credit_limit=null,status_toko=null}={}){
    return TopInvoice.create({
        user_id: user_id,
        dt_code: dt_code,
        le_code: le_code,
        outlet_code: outlet_code,
        cash_memo_doc_no: cash_memo_doc_no,
        cash_memo_doc_date: cash_memo_doc_date,
        cash_memo_total_amount: cash_memo_total_amount,
        cash_memo_balance_amount: cash_memo_balance_amount,
        cashmemo_type: cashmemo_type,
        invoice_id: invoice_id,
        invoice_id_associated_with_retailer_id: invoice_id_associated_with_retailer_id,
        invoice_sales_date: invoice_sales_date,
        invoice_due_date: invoice_due_date,
        invoice_payment_status: invoice_payment_status,
        product: product,
        product_name: product_name,
        quantity_pc: quantity_pc,
        product_price_cs: product_price_cs,
        product_pride_dz: product_pride_dz,
        product_price_pc: product_price_pc,
        overdue: overdue,
        distributor_principle_code: distributor_principle_code,
        invoice_toko: invoice_toko,
        principle: principle,
        denda_amount: denda_amount,
        total_yg_harus_dibayar: total_yg_harus_dibayar,
        credit_limit: credit_limit,
        status_toko: status_toko
    });
}

function update_invoice({le_code=null,dt_code=null,cash_memo_doc_no=null,cash_memo_doc_date=null,
    cash_memo_total_amount=null,cash_memo_balance_amount=null,cashmemo_type=null,invoice_id=null,
    invoice_id_associated_with_retailer_id=null,invoice_sales_date=null,invoice_due_date=null,invoice_payment_status=null,
    product=null,product_name=null,quantity_pc=null,product_price_cs=null,product_pride_dz=null,product_price_pc=null,
    overdue=null,distributor_principle_code=null,invoice_toko=null,principle=null,denda_amount=null,
    total_yg_harus_dibayar=null,credit_limit=null,status_toko=null}={}){
    return TopInvoice.update(
        {
            cash_memo_doc_no: cash_memo_doc_no,
            cash_memo_doc_date: cash_memo_doc_date,
            cash_memo_total_amount: cash_memo_total_amount,
            cash_memo_balance_amount: cash_memo_balance_amount,
            cashmemo_type: cashmemo_type,
            invoice_id: invoice_id,
            invoice_id_associated_with_retailer_id: invoice_id_associated_with_retailer_id,
            invoice_sales_date: invoice_sales_date,
            invoice_due_date: invoice_due_date,
            invoice_payment_status: invoice_payment_status,
            product: product,
            product_name: product_name,
            quantity_pc: quantity_pc,
            product_price_cs: product_price_cs,
            product_pride_dz: product_pride_dz,
            product_price_pc: product_price_pc,
            overdue: overdue,
            distributor_principle_code: distributor_principle_code,
            invoice_toko: invoice_toko,
            principle: principle,
            denda_amount: denda_amount,
            total_yg_harus_dibayar: total_yg_harus_dibayar,
            credit_limit: credit_limit,
            status_toko: status_toko
        }, {
                where:{
                    dt_code: dt_code,
                    le_code: le_code,
                    cash_memo_doc_no:cash_memo_doc_no,
                    invoice_toko:invoice_toko
                }
        }
    );
}

function get_all_invoice({overdue=null,dist_dt_code=null,role_type=null,user_id=null,dt_code=null, sorting=null,
    startPage=1,itemsPerPage=15,invoice_due_date=null,invoice_payment_status=null,next_date=null,previous_date=null}={}){
    startPage = (startPage < 1 ) ? 1 : startPage;
    const limit = itemsPerPage;
    const offset = ( startPage - 1 ) * itemsPerPage;

    
    var orderByAscOrDescQuery = '';
    var adminOrderByAscOrDescQuery = '';
    var distOrderByAscOrDescQuery = '';
    var filterByDtCode= '';
    var filterByPaymentStatus = '';
    var filterByDueDate = '';
    var filterAdmin = '';
    var filterByOverdue  = '';

    if(role_type === 0 && dt_code !== null){
        filterByDtCode = ' and dt_code = '+dt_code
    }
    if(role_type === 0 && invoice_payment_status !== null){
        filterByPaymentStatus = ' and invoice_payment_status = "'+invoice_payment_status+'"'
    }
    if(role_type === 0 && overdue !== null){
        filterByOverdue = ' and overdue = '+overdue
    }
    if(role_type === 0 && sorting === null)
    {
        orderByAscOrDescQuery = 'asc';
    }
    else if(role_type === 0 && sorting !== null)
    {
        orderByAscOrDescQuery = sorting;
    }
    let orderByColumnQuery = 'order by invoice_due_date '+orderByAscOrDescQuery;


    if(role_type === 1 && dt_code !== null){
        filterByDtCode = ' and dt_code = '+dt_code
    }
    if(role_type === 1 && invoice_payment_status !== null){
        filterByPaymentStatus = ' and invoice_payment_status = "'+invoice_payment_status+'"'
    }
    if(role_type === 1 && overdue !== null){
        filterByOverdue = ' and overdue = '+overdue
    }
    if(role_type === 1 && sorting === null)
    {
        distOrderByAscOrDescQuery = 'desc';
    }
    else if(role_type === 1 && sorting !== null)
    {
        distOrderByAscOrDescQuery = sorting;
    }
    let distOrderByColumnQuery = 'order by overdue '+ distOrderByAscOrDescQuery;


    if(role_type === 10 && invoice_payment_status !== null && invoice_due_date === null && overdue === null){
        filterAdmin = ' where invoice_payment_status = "'+invoice_payment_status+'"'
    }
    else if(role_type === 10 && invoice_payment_status === null && invoice_due_date !== null && overdue === null){
        filterAdmin = ' where invoice_due_date = "'+invoice_due_date+'"'
    }
    else if(role_type === 10 && invoice_payment_status !== null && invoice_due_date !== null && overdue === null){
        filterAdmin = ' where invoice_payment_status = "'+invoice_payment_status+ '" and invoice_due_date = "'+invoice_due_date+'"'
    }
    else if(role_type === 10 && invoice_payment_status !== null && invoice_due_date === null && overdue !== null){
        filterAdmin = ' where invoice_payment_status = "'+invoice_payment_status+'" and overdue = '+ overdue
    }
    else if(role_type === 10 && invoice_payment_status === null && invoice_due_date !== null && overdue !== null){
        filterAdmin = ' where invoice_due_date = "'+invoice_due_date+'" and overdue = '+overdue
    }
    else if(role_type === 10 && invoice_payment_status !== null && invoice_due_date !== null && overdue !== null){
        filterAdmin = ' where invoice_payment_status = "'+invoice_payment_status+ '" and invoice_due_date = "'+invoice_due_date+'" and overdue ='+overdue
    }
    else if(role_type === 10 && invoice_payment_status === null && invoice_due_date === null && overdue !== null){
        filterAdmin = ' where overdue ='+overdue
    }

    if(role_type === 10 && sorting === null)
    {
        adminOrderByAscOrDescQuery = 'desc';
    }
    else if(role_type === 10 && sorting !== null)
    {
       adminOrderByAscOrDescQuery = sorting;
    }
    let adminOrderByColumnQuery = 'order by overdue '+ adminOrderByAscOrDescQuery;
    
    
    if(role_type === 0){
        return  orm.query('SELECT cash_memo_doc_date,overdue,cash_memo_doc_no,invoice_toko,dt_code,le_code,outlet_code,cashmemo_type,cash_memo_total_amount,total_yg_harus_dibayar,invoice_payment_status,invoice_due_date FROM invoice_top where (user_id = '+user_id+filterByDtCode+filterByPaymentStatus+filterByDueDate+filterByOverdue+' and invoice_due_date between "'+previous_date+ '" and "'+next_date+'") or (user_id = '+user_id+' and invoice_due_date < "'+next_date+'" and invoice_payment_status = "UNPAID") '+ orderByColumnQuery+' limit '+ limit + ' offset '+offset, { type: orm.QueryTypes.SELECT})
          .then(function(tasks){
           tasks.forEach(result => {
                result.id= result.invoice_toko;
                result.dt_code= result.dt_code;
                result.le_code= result.le_code;
                result.cash_memo_doc_no=result.cash_memo_doc_no;
                result.outlet_code= result.outlet_code;
                result.cashmemo_type= result.cashmemo_type;
                result.amount= result.cash_memo_total_amount;
                result.balance_amount= result.invoice_payment_status == "PAID"?0:result.cash_memo_total_amount;
                result.status = result.invoice_payment_status;
                result.invoice_payment_status_paid= 0;
                result.payment_status_unpaid= 0;
                
                result.sales_date= moment(result.invoice_due_date).format('YYYY-MM-DD HH:mm:ss');
                result.due_date= moment(result.invoice_due_date).format('YYYY-MM-DD HH:mm:ss');
                
                let date = result.cash_memo_doc_date ==null?"":moment(result.cash_memo_doc_date).format('YYYY-MM-DD HH:mm:ss');
                result.paid_date = date
                
                result.is_paid= result.invoice_payment_status == "PAID"?true:false;
                result.is_refunded= false;
                result.has_refund= false;
                result.overdue = result.overdue;
                result.distributor= {dt_code:result.dt_code,name:""};
                let teno = result.cash_memo_doc_no.split("-");
                result.tenor_ke = teno[1];
                result.paid_by = "Pinjam Modal"
                let total_inv = find_all_invoice({dt_code:result.dt_code,le_code:result.le_code,cash_memo_doc_no:result.cash_memo_doc_no,invoice_toko:teno[0]})
                result.total_tenor = total_inv
                let amount_total = total_amount({dt_code:result.dt_code,le_code:result.le_code,invoice_toko:teno[0]})
                result.total_amountt = amount_total
                let trans_date = service_top.get_trans_date({invoice_no:teno[0]})
                result.paid_data_uli = trans_date == " "?moment().format('YYYY-MM-DD HH:mm:ss'):trans_date;
                delete result.invoice_due_date;
                delete result.invoice_toko;
                delete result.cash_memo_total_amount;
                delete result.cash_memo_doc_date;
                delete result.total_yg_harus_dibayar;
                delete result.invoice_payment_status;
                
              });
              return tasks;

          });
    }
    else if(role_type === 1){
        return  orm.query('SELECT overdue,cash_memo_doc_no,invoice_toko,dt_code,le_code,outlet_code,cashmemo_type,cash_memo_total_amount,total_yg_harus_dibayar,invoice_payment_status,invoice_due_date FROM invoice_top where dt_code = '+dist_dt_code+filterByPaymentStatus+filterByDueDate+filterByOverdue+' and invoice_due_date < "'+next_date+'" '+' '+distOrderByColumnQuery +' limit '+ limit + ' offset '+offset, { type: orm.QueryTypes.SELECT})
          .then(function(tasks){
           tasks.forEach(result => {
            result.id= result.invoice_toko;
            result.dt_code= result.dt_code;
            result.le_code= result.le_code;
            result.outlet_code= result.outlet_code;
            result.cashmemo_type= result.cashmemo_type;
            result.amount= result.cash_memo_total_amount;
            result.balance_amount= result.invoice_payment_status == "PAID"?0:result.cash_memo_total_amount;
            result.status = result.invoice_payment_status;
            result.invoice_payment_status_paid= 0;
            result.payment_status_unpaid= 0;
            result.sales_date= moment(result.invoice_due_date).format('YYYY-MM-DD HH:mm:ss');
            result.due_date= moment(result.invoice_due_date).format('YYYY-MM-DD HH:mm:ss');
            
            let date = moment(result.cash_memo_doc_date).format('YYYY-MM-DD HH:mm:ss');
            result.paid_date = date
            result.is_paid= result.invoice_payment_status == "PAID"?true:false;
            result.is_refunded= false;
            result.has_refund= false;
            result.overdue = result.overdue;
            result.distributor= {dt_code:result.dt_code,name:""};
            let teno = result.cash_memo_doc_no.split("-");
            result.tenor_ke = teno[1];
            result.paid_by = "Pinjam Modal"
            let total_inv = find_all_invoice({dt_code:result.dt_code,le_code:result.le_code,cash_memo_doc_no:result.cash_memo_doc_no,invoice_toko:teno[0]})
            result.total_tenor = total_inv
            let amount_total = total_amount({dt_code:result.dt_code,le_code:result.le_code,invoice_toko:teno[0]})
            result.total_amountt = amount_total
            let trans_date = service_top.get_trans_date({invoice_no:teno[0]})
            result.paid_data_uli = trans_date == " "?moment().format('YYYY-MM-DD HH:mm:ss'):trans_date;
            delete result.invoice_due_date;
            delete result.invoice_toko;
            delete result.cash_memo_total_amount;
            delete result.cash_memo_doc_date;
            delete result.total_yg_harus_dibayar;
            delete result.invoice_payment_status;});
            return tasks;});
    }
    else if(role_type === 10){
        return  orm.query('SELECT overdue,cash_memo_doc_no,invoice_toko,dt_code,le_code,outlet_code,cashmemo_type,cash_memo_total_amount,total_yg_harus_dibayar,invoice_payment_status,invoice_due_date FROM  invoice_top'+filterAdmin+' '+adminOrderByColumnQuery +' limit '+ limit + ' offset '+offset, { type: orm.QueryTypes.SELECT})
          .then(function(tasks){
           tasks.forEach(result => {
            result.id= result.invoice_toko;
            result.dt_code= result.dt_code;
            result.le_code= result.le_code;
            result.outlet_code= result.outlet_code;
            result.cashmemo_type= result.cashmemo_type;
            result.amount= result.cash_memo_total_amount;
            result.balance_amount= result.invoice_payment_status == "PAID"?0:result.cash_memo_total_amount;
            result.status = result.invoice_payment_status;
            result.invoice_payment_status_paid= 0;
            result.payment_status_unpaid= 0;
            result.sales_date= moment(result.invoice_due_date).format('YYYY-MM-DD HH:mm:ss');
            result.due_date= moment(result.invoice_due_date).format('YYYY-MM-DD HH:mm:ss');
            let date = moment(result.cash_memo_doc_date).format('YYYY-MM-DD HH:mm:ss');
            result.paid_date = date
            result.is_paid= result.invoice_payment_status == "PAID"?true:false;
            result.is_refunded= false;
            result.has_refund= false;
            result.overdue = result.overdue;
            result.distributor= {dt_code:result.dt_code,name:""};
            let teno = result.cash_memo_doc_no.split("-");
            result.tenor_ke = teno[1];
            result.paid_by = "Pinjam Modal"
            let total_inv = find_all_invoice({dt_code:result.dt_code,le_code:result.le_code,cash_memo_doc_no:result.cash_memo_doc_no,invoice_toko:teno[0]})
            result.total_tenor = total_inv
            let amount_total = total_amount({dt_code:result.dt_code,le_code:result.le_code,invoice_toko:teno[0]})
            result.total_amountt = amount_total
            let trans_date = service_top.get_trans_date({invoice_no:teno[0]})
            result.paid_data_uli = trans_date == " "?moment().format('YYYY-MM-DD HH:mm:ss'):trans_date;
            delete result.invoice_due_date;
            delete result.invoice_toko;
            delete result.cash_memo_total_amount;
            delete result.cash_memo_doc_date;
            delete result.total_yg_harus_dibayar;
            delete result.invoice_payment_status;});
            return tasks;
          });
    }
}

function countData({overdue=null,dist_dt_code=null,role_type=null,user_id=null,dt_code=null,sorting=null,
    invoice_payment_status=null,invoice_due_date=null,startPage=1,
    itemsPerPage=15,next_date=null,previous_date=null}={}){
    startPage = (startPage < 1 ) ? 1 : startPage;

    var orderByAscOrDescQuery = '';
    var adminOrderByAscOrDescQuery = '';
    var distOrderByAscOrDescQuery = '';
    var filterByDtCode= '';
    var filterByPaymentStatus = '';
    var filterByDueDate = '';
    var filterAdmin = '';
    var filterByOverdue = '';

    if(role_type === 0 && dt_code !== null){
        filterByDtCode = ' and dt_code = '+dt_code
    }
    if(role_type === 0 && invoice_payment_status !== null){
        filterByPaymentStatus = ' and invoice_payment_status = "'+invoice_payment_status+'"'
    }
    if(role_type === 0 && overdue !== null){
        filterByOverdue = ' and overdue = '+overdue
    }
    if(role_type === 0 && sorting === null)
    {
        orderByAscOrDescQuery = 'asc';
    }
    else if(role_type === 0 && sorting !== null)
    {
        orderByAscOrDescQuery = sorting;
    }
    let orderByColumnQuery = 'order by invoice_due_date '+orderByAscOrDescQuery;


    if(role_type === 1 && dt_code !== null){
        filterByDtCode = ' and dt_code = '+dt_code
    }
    if(role_type === 1 && invoice_payment_status !== null){
        filterByPaymentStatus = ' and invoice_payment_status = "'+invoice_payment_status+'"'
    }
    if(role_type === 1 && overdue !== null){
        filterByOverdue = ' and overdue = '+overdue
    }
    if(role_type === 1 && sorting === null)
    {
        distOrderByAscOrDescQuery = 'desc';
    }
    else if(role_type === 1 && sorting !== null)
    {
        distOrderByAscOrDescQuery = sorting;
    }
    let distOrderByColumnQuery = 'order by overdue '+ distOrderByAscOrDescQuery;


    if(role_type === 10 && invoice_payment_status !== null && invoice_due_date === null && overdue === null){
        filterAdmin = ' where invoice_payment_status = "'+invoice_payment_status+'"'
    }
    else if(role_type === 10 && invoice_payment_status === null && invoice_due_date !== null && overdue === null){
        filterAdmin = ' where invoice_due_date = "'+invoice_due_date+'"'
    }
    else if(role_type === 10 && invoice_payment_status !== null && invoice_due_date !== null && overdue === null){
        filterAdmin = ' where invoice_payment_status = "'+invoice_payment_status+ '" and invoice_due_date = "'+invoice_due_date+'"'
    }
    else if(role_type === 10 && invoice_payment_status !== null && invoice_due_date === null && overdue !== null){
        filterAdmin = ' where invoice_payment_status = "'+invoice_payment_status+'" and overdue = '+ overdue
    }
    else if(role_type === 10 && invoice_payment_status === null && invoice_due_date !== null && overdue !== null){
        filterAdmin = ' where invoice_due_date = "'+invoice_due_date+'" and overdue = '+overdue
    }
    else if(role_type === 10 && invoice_payment_status !== null && invoice_due_date !== null && overdue !== null){
        filterAdmin = ' where invoice_payment_status = "'+invoice_payment_status+ '" and invoice_due_date = "'+invoice_due_date+'" and overdue ='+overdue
    }
    else if(role_type === 10 && invoice_payment_status === null && invoice_due_date === null && overdue !== null){
        filterAdmin = ' where overdue ='+overdue
    }

    if(role_type === 10 && sorting === null)
    {
        adminOrderByAscOrDescQuery = 'desc';
    }
    else if(role_type === 10 && sorting !== null)
    {
       adminOrderByAscOrDescQuery = sorting;
    }
    let adminOrderByColumnQuery = 'order by overdue '+ adminOrderByAscOrDescQuery;
    

    try{
        if(role_type === 0){
            return orm.query('SELECT count(*) as total FROM invoice_top where (user_id = '+user_id+filterByDtCode+filterByPaymentStatus+filterByDueDate+filterByOverdue+' and invoice_due_date between "'+previous_date+ '" and "'+next_date+'") or (user_id = '+user_id+' and invoice_due_date < "'+next_date+'" and invoice_payment_status = "UNPAID") '+ orderByColumnQuery,{type: orm.QueryTypes.SELECT})
        }
        else if(role_type === 1){
            return orm.query('SELECT count(*) as total from invoice_top where dt_code = '+dist_dt_code+filterByPaymentStatus+filterByDueDate+filterByOverdue+' and invoice_due_date < "'+next_date+'" '+distOrderByColumnQuery, { type: orm.QueryTypes.SELECT})
        }
        else if(role_type === 10){
            return orm.query('SELECT count(*) as total from invoice_top'+filterAdmin+' '+adminOrderByColumnQuery,{type: orm.QueryTypes.SELECT})
        }
        
    }
    catch(err){
        console.log(err)
    }
}

function get_updated_invoices({next_date=null,previous_date=null}={}){
    try{
        return orm.query(`SELECT id, le_code, dt_code, invoice_toko AS invoice_number, cash_memo_doc_no as invoice_tenor, total_yg_harus_dibayar AS amount, invoice_payment_status FROM invoice_top WHERE invoice_payment_status = 'PAID' and updateAt BETWEEN '${previous_date}' AND '${next_date}'`)
    }catch(err){
        console.log(err)
    }
}

function insert_loan_history({user_id=null,dt_code=null,le_code=null,invoice_id=null,tenor=null,
amount=null,total_potongan=null,principal_code=null,total_amount_invoice=null,total_discount=null}={}){
    return TopLoanHistory.create({
        user_id: user_id,
        dt_code: dt_code,
        total_amount_invoice: total_amount_invoice,
        le_code: le_code,
        invoice_id: invoice_id,
        tenor:tenor,
        amount:amount,
        total_potongan:total_potongan,
        total_discount:total_discount,
        principal_code:principal_code
    });
}
function get_loan_history({dt_code=null,le_code=null,invoice_id=null}={}){
        return TopLoanHistory.findOne({
            where:{
                dt_code: dt_code,
                le_code: le_code,
                invoice_id: invoice_id,
            }
        });
}



let obj = {}
obj.find_invoice = find_invoice
obj.find_all_invoice = find_all_invoice
obj.total_amount = total_amount
obj.get_one_invoice = get_one_invoice
obj.insert_invoice = insert_invoice
obj.get_all_invoice = get_all_invoice
obj.countData = countData
obj.update_invoice = update_invoice
obj.get_updated_invoices = get_updated_invoices
obj.insert_loan_history =insert_loan_history
obj.get_loan_history = get_loan_history

export default obj
