import _ from 'lodash'
import Sequelize from 'sequelize'
import util from 'util'

const Op = Sequelize.Op;

// moment().format('YYYY-MM-DD HH:mm:ss')
// 2018-10-10 21:10:10

function modifiedSequelizeTimestamp(data) {
	
	let d = new Date(data);
	d.setHours(d.getHours() + 7);

	return d;
}

function formatCurrentDateTime() {

	let d = new Date();
	return d.getFullYear()+"-"+("0" + (d.getMonth() + 1)).slice(-2)+"-"+("0" + d.getDate()).slice(-2)+" "+("0" + d.getHours()).slice(-2)+":"+("0" + d.getMinutes()).slice(-2)+":"+("0" + d.getSeconds()).slice(-2);
}

function formatCurrentDateTimeAdd7Hours() {

	let d = new Date();
	d.setHours(d.getHours());
	return d.getFullYear()+"-"+("0" + (d.getMonth() + 1)).slice(-2)+"-"+("0" + d.getDate()).slice(-2)+" "+("0" + d.getHours()).slice(-2)+":"+("0" + d.getMinutes()).slice(-2)+":"+("0" + d.getSeconds()).slice(-2);
}

function formatDateTime(data) {

	let d = new Date(data);
	return d.getFullYear()+"-"+("0" + (d.getMonth() + 1)).slice(-2)+"-"+("0" + d.getDate()).slice(-2)+" "+("0" + d.getHours()).slice(-2)+":"+("0" + d.getMinutes()).slice(-2)+":"+("0" + d.getSeconds()).slice(-2);
}

function formatDateTimeAdd7Hours(data) {

	let d = new Date(data);
	d.setHours(d.getHours() + 7);
	return d.getFullYear()+"-"+("0" + (d.getMonth() + 1)).slice(-2)+"-"+("0" + d.getDate()).slice(-2)+" "+("0" + d.getHours()).slice(-2)+":"+("0" + d.getMinutes()).slice(-2)+":"+("0" + d.getSeconds()).slice(-2);
}

function formatCustomCurrentDateTime(month, date, hour, minute, second, type) {
	let d = new Date();

	if(type == "plus") {
		if(month) {
			d.setMonth(d.getMonth() + month);
		}
	
		if(date) {
			d.setDate(d.getDate() + date);
		}
	
		if(hour) {
			d.setHours(d.getHours() + hour);
		}
	
		if(minute) {
			d.setMinutes(d.getMinutes() + minute);
		}
	
		if(second) {
			d.setSeconds(d.getSeconds() + second);
		}
	} else if(type == "minus") {
		if(month) {
			d.setMonth(d.getMonth() - month);
		}
	
		if(date) {
			d.setDate(d.getDate() - date);
		}
	
		if(hour) {
			d.setHours(d.getHours() - hour);
		}
	
		if(minute) {
			d.setMinutes(d.getMinutes() - minute);
		}
	
		if(second) {
			d.setSeconds(d.getSeconds() - second);
		}
	}

	return d.getFullYear()+"-"+("0" + (d.getMonth() + 1)).slice(-2)+"-"+("0" + d.getDate()).slice(-2)+" "+("0" + d.getHours()).slice(-2)+":"+("0" + d.getMinutes()).slice(-2)+":"+("0" + d.getSeconds()).slice(-2);
}

function createVANumber(user_id, usertype,) {
	let kode_perusahaan = '01';
	let code_role = '';

    if(usertype == 1 || usertype == 3)
        code_role = 2;
    else if(usertype == 0)
        code_role = 1;
    else if(usertype == 2)
        code_role = 301;
    else if(usertype == 11)
        code_role = 11;

	let str = "" + user_id;
    let pad = "0000000";

    if(usertype == 2){
        pad = "00000";
    } else if(usertype == 11) {
        pad = "00000";
    }

    let unique_number = pad.substring(0, pad.length - str.length) + str;

    return code_role+kode_perusahaan+unique_number;
}

function isPhoneNumberOnly(str) {
	let regex = /^[0-9]{1,14}$/;

	if(str.match(regex)) {
		return true;
	} else {
		return false;
	}
}

function currentMilisecondsTime() {
	let d = new Date();
	return d.getTime();
}

function removeNullFromObject(data) {
	let keys = Object.keys(data);

	for (let i = 0; i < keys.length; i++) {
		let key = keys[i];
		if(data[key] == null) {
			data[key] = "";
		}
	}

	return data;
}

function splitString(str) {
	if(str == null || str.length == 0) {
		return "";
	} else {
		return str.split(" ");
	}
}

function getSimpleSwaggerParams(params) {
    return util.inspect(_.map(_.keys(params), (key) => {
        let param = params[key];
        return {
            key: key,
            value: param.value,
            valid: param.valid,
            error: param.error,
            definition: param.parameterObject.definition
        }
    }));
}

let obj = {};
obj.modifiedSequelizeTimestamp = modifiedSequelizeTimestamp
obj.formatCurrentDateTime = formatCurrentDateTime
obj.formatCurrentDateTimeAdd7Hours = formatCurrentDateTimeAdd7Hours
obj.formatDateTime = formatDateTime
obj.formatDateTimeAdd7Hours = formatDateTimeAdd7Hours
obj.formatCustomCurrentDateTime = formatCustomCurrentDateTime
obj.createVANumber = createVANumber
obj.isPhoneNumberOnly = isPhoneNumberOnly
obj.currentMilisecondsTime = currentMilisecondsTime
obj.removeNullFromObject = removeNullFromObject
obj.splitString = splitString
obj.getSimpleSwaggerParams = getSimpleSwaggerParams;

export default obj
export { Op }