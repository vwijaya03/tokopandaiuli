import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const TaskPotongan = orm.define('potongan_tasks', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    dt_code: {
        type: Sequelize.STRING, allowNull:true
    },
    original_name: {
        type: Sequelize.STRING, allowNull:true
    },
    file_name: {
        type: Sequelize.STRING, allowNull:true
    },
    status: {
        type: Sequelize.INTEGER, allowNull:true
    },
    total_row: {
        type: Sequelize.INTEGER, allowNull:true
    },
    total_upload: {
        type: Sequelize.INTEGER, allowNull:true
    },
    createdAt: {
        type: 'TIMESTAMP', allowNull:true
    },
    updatedAt: {
        type: 'TIMESTAMP', allowNull:true
    },
    user_id: {
        type: Sequelize.STRING, allowNull:true
    }
});

export default TaskPotongan
