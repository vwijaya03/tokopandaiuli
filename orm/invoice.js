import Sequelize from 'sequelize'
import { sprintf } from 'sprintf-js'
import { orm } from '~/services/mariadb'
import { DataTypes } from 'sequelize'

const Invoice = orm.define('invoice', {
    id: {
        type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true
    },
    distributorCode: {
        type: Sequelize.STRING, allowNull: false, field:'dt_code'
    },
    le_code: {
        type: Sequelize.STRING, allowNull: false
    },
    outletCode: {
        type: Sequelize.STRING, allowNull: false, field:'outlet_code'
    },
    cash_memo_total_amount: {
        type: Sequelize.DECIMAL(12, 4), allowNull: true
    },
    cash_memo_balance_amount: {
        type: Sequelize.DECIMAL(12, 4), allowNull: true
    },
    cashmemo_type: {
        type: Sequelize.STRING, allowNull: false
    },
    invoice_id: {
        type: Sequelize.STRING, allowNull: false
    },
    invoice_sales_date: {
        type: 'TIMESTAMP', allowNull:false,
        set(value) {
            this.setDataValue('invoice_sales_date', value);
        },
        get(key) {
            let value = this.getDataValue(key);
            if (value instanceof Date) {
                value = value.getTime();
            }
            return value;
        }
    },
    invoice_due_date: {
        type: 'TIMESTAMP', allowNull:false,
        set(value) {
            this.setDataValue('invoice_due_date', value);
        },
        get(key) {
            let value = this.getDataValue(key);
            if (value instanceof Date) {
                value = value.getTime();
            }
            return value;
        }
    },
    invoice_payment_status_paid: {
        type: Sequelize.DECIMAL(15, 4), allowNull: true
    },
    invoice_payment_status_unpaid: {
        type: Sequelize.DECIMAL(15, 4), allowNull: true
    },
    invoice_details: {
        type: Sequelize.STRING, allowNull: true
    },
    paid_by: {
        type: Sequelize.STRING, allowNull: true
    },
    approval_date: {
        type: 'TIMESTAMP', allowNull: true
    },
    status: {
        type: Sequelize.INTEGER, allowNull: true
    },
    hide_status: {
        type: Sequelize.INTEGER, allowNull: true
    },
    has_refund: {
        type: new DataTypes.VIRTUAL(DataTypes.BOOLEAN,
            ['cash_memo_balance_amount']),
        get: function () {
            let balance_amount = this.getDataValue('cash_memo_balance_amount');
            let balance =  (balance_amount < 0);

            if(balance)
            {
                return "1";
            }
            else
            {
                return "0";
            }
        }
    },
    is_refunded: {
        type: new DataTypes.VIRTUAL(DataTypes.BOOLEAN,
            ['cash_memo_balance_amount', 'invoice_payment_status_unpaid']),
        get: function () {
            let balance_amount = this.getDataValue('cash_memo_balance_amount');
            let status_unpaid_amount = this.getDataValue('invoice_payment_status_unpaid');
            let is_refunded = (balance_amount < 0) && (balance_amount == status_unpaid_amount);

            if(is_refunded)
            {
                return "1";
            }
            else
            {
                return "0";
            }
        }
    },
    is_paid: {
        type: new DataTypes.VIRTUAL(DataTypes.BOOLEAN,
            ['cash_memo_total_amount', 'invoice_payment_status_paid']),
        get: function () {
            let total_balance_amount = this.getDataValue('cash_memo_balance_amount');
            let is_paid = (total_balance_amount == 0);

            if(is_paid)
            {
                return "1";
            }
            else
            {
                return "0";
            }
        }
    }
},
    {
        freezeTableName: true, // Model tableName will be the same as the model name
        timestamps: false
    }
);

/**
setups query filter for outlet code
@param {Object} filter
**/
function queryOutletCode({filter=null, outletCode=null}={}) {
    filter['outletCode'] = outletCode;
}

/**
setups query filter for paid invoices
@param {Object} filter
*/
function queryIsPaid({filter=null}={}) {
    filter['cash_memo_total_amount'] = { $eq: Sequelize.col('invoice_payment_status_paid') }
}

/**
setups query filter for unpaid invoices
@param {Object} filter
*/
function queryIsUnpaid({filter=null}={}) {
    // filter['cash_memo_total_amount'] = { $ne: Sequelize.col('invoice_payment_status_paid') }
    filter['cash_memo_balance_amount'] = { $gte:0 }
}

/**
setups query filter to match specific distributor
*/
function queryDistributorCode({filter=null, distributorCode=null}={}) {
    filter['distributorCode'] = distributorCode;
}

/**
setups query filter for due date
@param {Object} filter
@param {Number} start - the starting due date in unix seconds
@param {Number} end - the ending due date in unix seconds
*/
function queryDueDate({filter=null, start=null, end=null}={}) {
    filter['invoice_due_date'] = { $gte:start, $lte:end };
}

function queryRetur({filter=null, cashmemo_type=null}={}) {
    filter['cash_memo_balance_amount'] = { $lt: 0};
}

function queryInvoiceTagihan({filter=null, cashmemo_type=null}={}) {
    filter['cash_memo_total_amount'] = { $gte: 0};
}

function queryAllReturRequest({filter=null, cashmemo_type=null}={}) {
    filter['cashmemo_type'] = cashmemo_type;
    filter['status'] = { $gte:0, $lte:2 };
    filter['approval_date'] = { $ne:null };
}

function querySortReturRequest({filter=null, cashmemo_type=null, status=null}={}) {
    filter['cashmemo_type'] = cashmemo_type;
    filter['status'] = status;
}

function queryOrderBy({filter=null,orderByType=null, }) {
    filter['order'] = [['invoice_sales_date', orderByType]];
}

let query = {};
query.outletCode = queryOutletCode;
query.isPaid = queryIsPaid;
query.isUnpaid = queryIsUnpaid;
query.duedate = queryDueDate;
query.distributorCode = queryDistributorCode;
query.queryRetur = queryRetur;
query.queryAllReturRequest = queryAllReturRequest;
query.querySortReturRequest = querySortReturRequest;
query.queryOrderBy = queryOrderBy;
query.queryInvoiceTagihan = queryInvoiceTagihan;

export default Invoice
export { query as InvoiceQuery }
