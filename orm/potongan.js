import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'


const Potongan = orm.define('potongan', {
    id: {
        type: Sequelize.INTEGER, field: 'id',
        primaryKey: true,
        autoIncrement: true
    },
    dt_code: {
        type: Sequelize.INTEGER, allowNull:true
    },
    le_code: {
        type: Sequelize.STRING, allowNull:true
    },
    trans_invoice_id: {
        type: Sequelize.STRING, allowNull:true
    },
    amount_potongan: {
        type: Sequelize.STRING, allowNull:true
    },
    date_potongan: {
        type: Sequelize.STRING, allowNull:true
    },
    remark_potongan: {
        type: Sequelize.STRING, allowNull:true
    },
    file_name: {
        type: Sequelize.TEXT, allowNull:true
    },
    task_id: {
        type: Sequelize.STRING, allowNull:true
    },
    status: {
        type: Sequelize.STRING, allowNull:true
    },
    remark_app: {
        type: Sequelize.STRING, allowNull:true
    },
}, {
    freezeTableName: true,
    timestamps: false
});

export default Potongan
