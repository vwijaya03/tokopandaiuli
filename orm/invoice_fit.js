import Sequelize from 'sequelize'
import { orm } from '~/services/mariadb'

const FitInvoice = orm.define('invoice_top', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_id:{
        type: Sequelize.INTEGER, allowNull: true
    },
    dt_code: {
        type: Sequelize.STRING, allowNull: true
    },
    le_code: {
        type: Sequelize.STRING, allowNull: true
    },
    outlet_code: {
        type: Sequelize.INTEGER, allowNull: true
    },
    cash_memo_doc_no:{
        type: Sequelize.STRING, allowNull: true
    },
    cash_memo_doc_date:{
        type: Sequelize.DATE, allowNull: true
    },
    cash_memo_total_amount: {
        type: Sequelize.DECIMAL(15, 4), allowNull: true
    },
    cash_memo_balance_amount: {
        type: Sequelize.DECIMAL(15, 4), allowNull: true
    },
    cashmemo_type: {
        type: Sequelize.STRING, allowNull: true
    },
    invoice_id: {
        type: Sequelize.STRING, allowNull: true
    },
    invoice_id_associated_with_retailer_id:{
        type: Sequelize.STRING, allowNull: true
    },
    invoice_sales_date: {
        type: Sequelize.STRING, allowNull:true,
    },
    invoice_due_date: {
        type: Sequelize.STRING, allowNull:true,
    },
    invoice_payment_status: {
        type: Sequelize.DECIMAL(15, 4), allowNull: true
    },
    product:{
        type: Sequelize.STRING, allowNull: true
    },
    product_name:{
        type: Sequelize.STRING, allowNull: true
    },
    quantity_pc:{
        type: Sequelize.STRING, allowNull: true
    },
    product_price_cs:{
        type: Sequelize.STRING, allowNull: true
    },
    product_pride_dz:{
        type: Sequelize.STRING, allowNull: true
    },
    product_price_pc:{
        type: Sequelize.STRING, allowNull: true
    },
    overdue:{
        type: Sequelize.INTEGER, allowNull: true
    },
    distributor_principle_code:{
        type: Sequelize.STRING, allowNull: true
    },
    invoice_toko:{
        type: Sequelize.STRING, allowNull: true
    },
    principle:{
        type: Sequelize.INTEGER, allowNull: true
    },
    denda_amount:{
        type: Sequelize.DECIMAL(15,4), allowNull: true
    },
    total_yg_harus_dibayar:{
        type: Sequelize.DECIMAL(15,4), allowNull: true
    },
    credit_limit:{
        type: Sequelize.DECIMAL(15,4), allowNull: true
    },
    status_toko:{
        type: Sequelize.STRING, allowNull: true
    }
},
    {
        freezeTableName: true, 
        timestamps: false
    }
);

export default FitInvoice


