import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const Top_invoice = orm.define('invoice', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    dt_code: {
        type: Sequelize.STRING, allowNull:true
    },
    le_code: {
        type: Sequelize.STRING, allowNull:true
    },
    outlet_code: {
        type: Sequelize.STRING, allowNull:true
    },
    cash_memo_doc_no: {
        type: Sequelize.INTEGER, allowNull:true
    },
    tenor: {
        type: Sequelize.INTEGER, allowNull:true
    },
    principal_code: {
        type: Sequelize.INTEGER, allowNull:true
    },
    createdAt: {
        type: 'TIMESTAMP', allowNull:true
    },
    updatedAt: {
        type: 'TIMESTAMP', allowNull:true
    }
});

export default Top_payment_distributor
