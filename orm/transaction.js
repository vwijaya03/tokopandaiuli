import Sequelize from 'sequelize'
import { orm } from '~/services/mariadb'

const Transaction = orm.define('transactions', {
    id: {
        type: Sequelize.BIGINT, field: 'transaction_id', primaryKey: true,
        autoIncrement: true
    },
    fromUserId: {
        type: Sequelize.INTEGER, allowNull:false, field:'from_user_id'
    },
    toUserId: {
        type: Sequelize.INTEGER, allowNull:true, field:'to_user_id'
    },
    dt_code: {
        type: Sequelize.STRING, allowNull:true, field:'dt_code'
    },
    invoice_id: {
        type: Sequelize.STRING, allowNull:true, field:'invoice_id'
    },
    invoice_code: {
        type: Sequelize.STRING, allowNull:true, field:'invoice_code'
    },
    retailerId: {
        type: Sequelize.INTEGER, allowNull:true, field:'retailer_id'
    },
    distributorId: {
        type: Sequelize.INTEGER, allowNull:true, field:'distributor_id'
    },
    valdo_tx_id: {
        type: Sequelize.STRING, allowNull:true, field:'valdo_tx_id'
    },
    type: {
        type: Sequelize.INTEGER, allowNull:true
    },
    amount: {
        type: Sequelize.DECIMAL(12, 4), allowNull:true
    },
    currency_code: {
        type: Sequelize.STRING, allowNull:true, field:'currency_code'
    },
    transact_date: {
        type: 'TIMESTAMP', allowNull:false, field: 'transact_date'
    }
}, {
    freezeTableName: true, // Model tableName will be the same as the model name
    timestamps: false
});

export default Transaction