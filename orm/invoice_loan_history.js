import Sequelize from 'sequelize'
import { orm } from '~/services/mariadb'

const TopLoanHistory = orm.define('invoice_loan_history', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_id:{
        type: Sequelize.INTEGER, allowNull: true
    },
    le_code: {
        type: Sequelize.STRING, allowNull: true
    },
    dt_code: {
        type: Sequelize.STRING, allowNull: true
    },
    amount:{
        type: Sequelize.DECIMAL(15,4), allowNull: true
    },
    total_amount_invoice:{
        type: Sequelize.DECIMAL(15,4), allowNull: true
    },
    invoice_id: {
        type: Sequelize.STRING, allowNull: true
    },
    tenor: {
        type: Sequelize.INTEGER, allowNull: true
    },
    principal_code:{
        type: Sequelize.INTEGER, allowNull: true
    },
    total_potongan:{
        type: Sequelize.DECIMAL(15,4), allowNull: true
    },
    total_discount:{
        type: Sequelize.DECIMAL(15,4), allowNull: true
    },
    createdAt: {
        type: 'TIMESTAMP', allowNull:true
    },
    updatedAt: {
        type: 'TIMESTAMP', allowNull:true
    }
},
    {
        freezeTableName: true, // Model tableName will be the same as the model name
        timestamps: false
    }
);

export default TopLoanHistory

