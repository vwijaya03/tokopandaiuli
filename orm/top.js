import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const Top_payment_distributor = orm.define('top_payment_distributors', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    dt_code: {
        type: Sequelize.STRING, allowNull:true
    },
    le_code: {
        type: Sequelize.STRING, allowNull:true
    },
    invoice_no: {
        type: Sequelize.STRING, allowNull:true
    },
    amount: {
        type: Sequelize.DECIMAL(15,4), allowNull: true
    },
    total_tagihan: {
        type: Sequelize.DECIMAL(15,4), allowNull: true
    },
    tenor: {
        type: Sequelize.STRING, allowNull:true
    },
    principal_code: {
        type: Sequelize.STRING, allowNull:true
    },
    fmcg_id: {
        type: Sequelize.STRING, allowNull:true
    },
    status:{
        type: Sequelize.INTEGER, allowNull:true
    },
    payment_status:{
        type: Sequelize.INTEGER, allowNull:true
    },
    transaction_date:{
        type: 'TIMESTAMP', allowNull:true
    },
    createdAt: {
        type: 'TIMESTAMP', allowNull:true
    },
    updatedAt: {
        type: 'TIMESTAMP', allowNull:true
    }
});

export default Top_payment_distributor
