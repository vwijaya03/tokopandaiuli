import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const Distributor = orm.define('distributors', {
    distributor_id: {
        type: Sequelize.INTEGER, field: 'distributor_id',
        primaryKey: true,
        autoIncrement: true
    },
    dt_code: {
        type: Sequelize.STRING, allowNull:true
    },
    name: {
        type: Sequelize.STRING, allowNull:true
    }
}, {
    freezeTableName: true, // Model tableName will be the same as the model name
    timestamps: false
});

export default Distributor
