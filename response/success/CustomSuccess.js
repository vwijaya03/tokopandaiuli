class CustomSuccess {
	constructor(code, message) {
        this.code = code;
        this.message = message;
    }
}

export default CustomSuccess 