class TokenExpiredError extends Error {
	constructor(code, message) {
        super(message);
        this.code = 401;
        this.message = 'Sesi login telah berakhir';

        // this.code = code;
        // this.message = message;
    }
}

export default TokenExpiredError