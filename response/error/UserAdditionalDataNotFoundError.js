class UserAdditionalDataNotFoundError extends Error {
	constructor(code, message) {
        super(message);
        this.code = 400;
        this.message = 'Data tambahan user tidak di temukan';
    }
}

export default UserAdditionalDataNotFoundError