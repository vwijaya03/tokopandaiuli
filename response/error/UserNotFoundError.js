class UserNotFoundError extends Error {
	constructor(code, message) {
        super(message);
        this.code = 400;
        this.message = 'Data tidak di temukan';
    }
}

export default UserNotFoundError