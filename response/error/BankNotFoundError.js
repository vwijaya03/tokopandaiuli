class BankNotFoundError extends Error {
	constructor(code, message) {
        super(message);
        this.code = 400;
        this.message = 'Data bank tidak di temukan';
    }
}

export default BankNotFoundError