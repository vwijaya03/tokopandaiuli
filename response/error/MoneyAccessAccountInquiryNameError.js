class MoneyAccessAccountInquiryNameError extends Error {
	constructor(message) {
        super(message);
        this.code = 400;
        this.message = 'Gagal mendapatkan nama akun, '+message;
    }
}

export default MoneyAccessAccountInquiryNameError