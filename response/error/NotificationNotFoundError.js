class NotificationNotFoundError extends Error {
	constructor(code, message) {
        super(message);
        this.code = 400;
        this.message = 'Notifikasi tidak di temukan';
    }
}

export default NotificationNotFoundError