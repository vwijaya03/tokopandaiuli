class MoneyAccessRegisterError extends Error {
	constructor(message) {
        super(message);
        this.code = 400;
        this.message = 'Register money access gagal, '+message;
    }
}

export default MoneyAccessRegisterError